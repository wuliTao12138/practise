# 行者系统
`环境说明:`

 mysql 5.7
 jdk 1.8
 maven 3.3.9
 tomcat 8
 
`部署说明：`
 
 打包为ROOT.war
 文件上传路径默认为项目内upload目录
 
 `不配置tomcat虚拟目录` ：
   
    1. clean -Dmaven.test.skip=true package
 `配置tomcat虚拟目录`：
    例如：<Context path="/upload" docBase="E:\Program Files\Apache Software Foundation\upload" debug="0" reloadable="true"/>
    
        1. clean -Dmaven.test.skip=true package -Pproduction
        2. 需修改conf->production 下 application.properties
        uploader.path 属性值，该属性值需在生产环境建立相应路径，并将项目内的upload文件夹移至该路径
## 简介
基于springmvc、spring、mybatis-plus、shiro、easyui、Log4j2简单实用的权限系统。

界面基于EasyUI，图标采用较为开放的`Foundation Icon`(MIT协议)。

`参考资料`：http://git.oschina.net/wangzhixuan/spring-shiro-training/wikis/Home

## 技术和功能
> Spring-cache

> Spring-data-redis

> Spring-Task

> Shiro

> Spring-cache-shiro

> hibernate-validator

> maven profile多环境配置

> 权限管理

> 角色管理

> 用户管理

> 部门管理

> 登陆日志

> 图标管理
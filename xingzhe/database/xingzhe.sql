/*
 Navicat Premium Data Transfer

 Source Server         : 172.16.35.242_3306
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : 172.16.35.242:3306
 Source Schema         : xingzhe

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : 65001

 Date: 18/08/2017 14:58:46
*/

-- ----------------------------
-- Table structure for organization
-- ----------------------------
DROP TABLE IF EXISTS `organization`;
CREATE TABLE `organization` (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(64) NOT NULL COMMENT '组织名',
  `address` varchar(100) DEFAULT NULL COMMENT '地址',
  `code` varchar(64) NOT NULL COMMENT '编号',
  `icon` varchar(32) DEFAULT NULL COMMENT '图标',
  `pid` bigint(19) DEFAULT NULL COMMENT '父级主键',
  `seq` tinyint(2) NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='组织机构';

-- ----------------------------
-- Records of organization
-- ----------------------------
BEGIN;
INSERT INTO `organization` VALUES (1, '总经办', '', '01', 'fi-social-windows', NULL, 0, '2014-02-19 01:00:00');
INSERT INTO `organization` VALUES (3, '技术部', '', '02', 'fi-social-github', NULL, 1, '2015-10-01 13:10:42');
INSERT INTO `organization` VALUES (5, '产品部', '', '03', 'fi-social-apple', NULL, 2, '2015-12-06 12:15:30');
INSERT INTO `organization` VALUES (6, '测试组', '', '04', 'fi-social-snapchat', 3, 0, '2015-12-06 13:12:18');
COMMIT;

-- ----------------------------
-- Table structure for resource
-- ----------------------------
DROP TABLE IF EXISTS `resource`;
CREATE TABLE `resource` (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(64) NOT NULL COMMENT '资源名称',
  `url` varchar(100) DEFAULT NULL COMMENT '资源路径',
  `open_mode` varchar(32) DEFAULT NULL COMMENT '打开方式 ajax,iframe',
  `description` varchar(255) DEFAULT NULL COMMENT '资源介绍',
  `icon` varchar(32) DEFAULT NULL COMMENT '资源图标',
  `pid` bigint(19) DEFAULT NULL COMMENT '父级资源id',
  `seq` tinyint(2) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `opened` tinyint(2) NOT NULL DEFAULT '1' COMMENT '打开状态',
  `resource_type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '资源类别',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=271 DEFAULT CHARSET=utf8 COMMENT='资源';

-- ----------------------------
-- Records of resource
-- ----------------------------
BEGIN;
INSERT INTO `resource` VALUES (1, '权限管理', '', '', '系统管理', 'fi-folder', NULL, 0, 0, 0, 0, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (11, '资源管理', '/resource/manager', 'ajax', '资源管理', 'fi-database', 1, 1, 0, 1, 0, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (12, '角色管理', '/role/manager', 'ajax', '角色管理', 'fi-torso-business', 1, 2, 0, 1, 0, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (13, '用户管理', '/user/manager', 'ajax', '用户管理', 'fi-torsos-all', 1, 3, 0, 1, 0, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (14, '部门管理', '/organization/manager', 'ajax', '部门管理', 'fi-results-demographics', 1, 4, 0, 1, 0, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (111, '列表', '/resource/treeGrid', 'ajax', '资源列表', 'fi-list', 11, 0, 0, 1, 1, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (112, '添加', '/resource/add', 'ajax', '资源添加', 'fi-page-add', 11, 0, 0, 1, 1, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (113, '编辑', '/resource/edit', 'ajax', '资源编辑', 'fi-page-edit', 11, 0, 0, 1, 1, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (114, '删除', '/resource/delete', 'ajax', '资源删除', 'fi-page-delete', 11, 0, 0, 1, 1, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (121, '列表', '/role/dataGrid', 'ajax', '角色列表', 'fi-list', 12, 0, 0, 1, 1, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (122, '添加', '/role/add', 'ajax', '角色添加', 'fi-page-add', 12, 0, 0, 1, 1, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (123, '编辑', '/role/edit', 'ajax', '角色编辑', 'fi-page-edit', 12, 0, 0, 1, 1, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (124, '删除', '/role/delete', 'ajax', '角色删除', 'fi-page-delete', 12, 0, 0, 1, 1, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (125, '授权', '/role/grant', 'ajax', '角色授权', 'fi-check', 12, 0, 0, 1, 1, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (131, '列表', '/user/dataGrid', 'ajax', '用户列表', 'fi-list', 13, 0, 0, 1, 1, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (132, '添加', '/user/add', 'ajax', '用户添加', 'fi-page-add', 13, 0, 0, 1, 1, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (133, '编辑', '/user/edit', 'ajax', '用户编辑', 'fi-page-edit', 13, 0, 0, 1, 1, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (134, '删除', '/user/delete', 'ajax', '用户删除', 'fi-page-delete', 13, 0, 0, 1, 1, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (141, '列表', '/organization/treeGrid', 'ajax', '用户列表', 'fi-list', 14, 0, 0, 1, 1, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (142, '添加', '/organization/add', 'ajax', '部门添加', 'fi-page-add', 14, 0, 0, 1, 1, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (143, '编辑', '/organization/edit', 'ajax', '部门编辑', 'fi-page-edit', 14, 0, 0, 1, 1, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (144, '删除', '/organization/delete', 'ajax', '部门删除', 'fi-page-delete', 14, 0, 0, 1, 1, '2014-02-19 01:00:00');
INSERT INTO `resource` VALUES (221, '日志监控', '', NULL, NULL, 'fi-folder', NULL, 3, 0, 0, 0, '2015-12-01 11:44:20');
INSERT INTO `resource` VALUES (226, '修改密码', '/user/editPwdPage', 'ajax', NULL, 'fi-unlock', NULL, 4, 0, 1, 1, '2015-12-07 20:23:06');
INSERT INTO `resource` VALUES (227, '登录日志', '/sysLog/manager', 'ajax', NULL, 'fi-info', 221, 0, 0, 1, 0, '2016-09-30 22:10:53');
INSERT INTO `resource` VALUES (228, 'Druid监控', '/druid', 'iframe', NULL, 'fi-monitor', 221, 0, 0, 1, 0, '2016-09-30 22:12:50');
INSERT INTO `resource` VALUES (229, '系统图标', '/icons.html', 'ajax', NULL, 'fi-photo', 221, 0, 0, 1, 0, '2016-12-24 15:53:47');
INSERT INTO `resource` VALUES (230, '测试管理', '', 'ajax', NULL, 'fi-page-multiple', NULL, 1, 0, 0, 0, '2016-12-24 15:53:47');
INSERT INTO `resource` VALUES (231, '新建文章', '/article/create', 'ajax', NULL, 'fi-page-edit', 230, 0, 0, 1, 0, '2016-12-24 15:53:47');
INSERT INTO `resource` VALUES (232, '后台管理', '', '', NULL, 'fi-folder', NULL, 0, 0, 1, 0, '2017-06-27 16:32:49');
INSERT INTO `resource` VALUES (233, '导师管理', '/tbTutor/manager', 'ajax', NULL, 'fi-torsos-all', 232, 0, 0, 1, 0, '2017-06-27 16:42:06');
INSERT INTO `resource` VALUES (234, '轮播图管理', '/tbSlideshow/manager', 'ajax', NULL, 'fi-photo', 232, -1, 0, 1, 0, '2017-06-27 16:55:26');
INSERT INTO `resource` VALUES (235, '邀请函管理', '/tbInvitation/manager', 'ajax', NULL, 'fi-mail', 232, 0, 0, 1, 0, '2017-06-27 16:57:12');
INSERT INTO `resource` VALUES (236, '类型管理', '', '', NULL, 'fi-widget', 232, 1, 0, 0, 0, '2017-06-27 17:00:30');
INSERT INTO `resource` VALUES (237, '领域类型', '/tbTypeDomain/manager', 'ajax', NULL, 'fi-book', 236, 0, 0, 1, 0, '2017-06-27 17:04:32');
INSERT INTO `resource` VALUES (238, '文章类型', '/tbTypeArticle/manager', 'ajax', NULL, 'fi-book', 236, 0, 0, 1, 0, '2017-06-27 17:05:07');
INSERT INTO `resource` VALUES (239, '来源类型', '/tbTypeSource/manager', 'ajax', NULL, 'fi-book', 236, 0, 0, 1, 0, '2017-06-27 17:05:37');
INSERT INTO `resource` VALUES (240, '文章管理', '/tbArticle/manager', 'ajax', NULL, 'fi-page-multiple', 232, 0, 0, 1, 0, '2017-06-27 17:12:21');
INSERT INTO `resource` VALUES (241, '列表', '/tbTypeDomain/dataGrid', 'ajax', NULL, 'fi-list', 237, 0, 0, 1, 1, '2017-06-28 09:47:15');
INSERT INTO `resource` VALUES (242, '添加', '/tbTypeDomain/add', 'ajax', NULL, 'fi-page-add', 237, 0, 0, 1, 1, '2017-06-28 09:51:28');
INSERT INTO `resource` VALUES (243, '编辑', '/tbTypeDomain/edit', 'ajax', NULL, 'fi-page-edit', 237, 0, 0, 1, 1, '2017-06-28 10:38:24');
INSERT INTO `resource` VALUES (244, '列表', '/tbTypeArticle/dataGrid', 'ajax', NULL, 'fi-list', 238, 0, 0, 1, 1, '2017-06-28 14:11:19');
INSERT INTO `resource` VALUES (245, '添加', '/tbTypeArticle/add', 'ajax', NULL, 'fi-page-add', 238, 0, 0, 1, 1, '2017-06-28 14:23:40');
INSERT INTO `resource` VALUES (246, '编辑', '/tbTypeArticle/edit', 'ajax', NULL, 'fi-page-edit', 238, 0, 0, 1, 1, '2017-06-28 14:24:20');
INSERT INTO `resource` VALUES (247, '列表', '/tbTypeSource/dataGraid', 'ajax', NULL, 'fi-list', 239, 0, 0, 1, 1, '2017-06-28 14:53:01');
INSERT INTO `resource` VALUES (248, '添加', '/tbTypeSource/add', 'ajax', NULL, 'fi-page-add', 239, 0, 0, 1, 1, '2017-06-28 14:55:03');
INSERT INTO `resource` VALUES (249, '编辑', '/tbTypeSource/edit', 'ajax', NULL, 'fi-page-edit', 239, 0, 0, 1, 1, '2017-06-28 14:55:36');
INSERT INTO `resource` VALUES (250, '列表', '/tbArticle/dataGrid', 'ajax', NULL, 'fi-list', 240, 0, 0, 1, 1, '2017-06-28 15:06:47');
INSERT INTO `resource` VALUES (251, '添加', '/tbArticle/add', 'ajax', NULL, 'fi-page-add', 240, 0, 0, 1, 1, '2017-06-28 15:07:24');
INSERT INTO `resource` VALUES (252, '编辑', '/tbArticle/edit', 'ajax', NULL, 'fi-page-edit', 240, 0, 0, 1, 1, '2017-06-28 15:07:53');
INSERT INTO `resource` VALUES (253, '列表', '/tbSlideshow/dataGrid', 'ajax', NULL, 'fi-list', 234, 0, 0, 1, 1, '2017-06-28 23:09:37');
INSERT INTO `resource` VALUES (254, '添加', '/tbSlideshow/add', 'ajax', NULL, 'fi-page-add', 234, 0, 0, 1, 1, '2017-06-28 23:11:27');
INSERT INTO `resource` VALUES (255, '编辑', '/tbSlideshow/edit', 'ajax', NULL, 'fi-page-edit', 234, 0, 0, 1, 1, '2017-06-28 23:12:04');
INSERT INTO `resource` VALUES (256, '列表', '/tbTutor/dataGrid', 'ajax', NULL, 'fi-list', 233, 0, 0, 1, 1, '2017-06-28 23:14:21');
INSERT INTO `resource` VALUES (257, '添加', '/tbTutor/add', 'ajax', NULL, 'fi-page-add', 233, 0, 0, 1, 1, '2017-06-28 23:15:19');
INSERT INTO `resource` VALUES (258, '编辑', '/tbTutor/edit', 'ajax', NULL, 'fi-page-edit', 233, 0, 0, 1, 1, '2017-06-28 23:15:52');
INSERT INTO `resource` VALUES (259, '列表', '/tbInvitation/dataGrid', 'ajax', NULL, 'fi-list', 235, 0, 0, 1, 1, '2017-06-28 23:17:43');
INSERT INTO `resource` VALUES (260, '添加', '/tbInvitation/add', 'ajax', NULL, 'fi-page-add', 235, 0, 0, 1, 1, '2017-06-28 23:18:14');
INSERT INTO `resource` VALUES (261, '编辑', '/tbInvitation/edit', 'ajax', NULL, 'fi-page-edit', 235, 0, 0, 1, 1, '2017-06-28 23:18:42');
INSERT INTO `resource` VALUES (262, '公开课管理', '/tbOpenClass/manager', 'ajax', NULL, 'fi-database', 232, 0, 0, 1, 0, '2017-07-06 01:52:40');
INSERT INTO `resource` VALUES (263, '列表', '/tbOpenClass/dataGrid', 'ajax', NULL, 'fi-list', 262, 0, 0, 1, 1, '2017-07-06 01:54:13');
INSERT INTO `resource` VALUES (264, '添加', '/tbOpenClass/add', 'ajax', NULL, 'fi-page-add', 262, 0, 0, 1, 1, '2017-07-06 01:55:01');
INSERT INTO `resource` VALUES (265, '编辑', '/tbOpenClass/edit', 'ajax', NULL, 'fi-page-edit', 262, 0, 0, 1, 1, '2017-07-06 01:56:59');
INSERT INTO `resource` VALUES (266, '删除', '/tbOpenClass/delete', 'ajax', NULL, 'fi-page-delete', 262, 0, 0, 1, 1, '2017-07-06 01:59:37');
INSERT INTO `resource` VALUES (267, '删除', '/tbInvitation/delete', 'ajax', NULL, 'fi-page-delete', 235, 0, 0, 1, 1, '2017-07-06 03:05:03');
INSERT INTO `resource` VALUES (268, '删除', '/tbSlideshow/delete', 'ajax', NULL, 'fi-page-delete', 234, 0, 0, 1, 1, '2017-07-07 07:29:46');
INSERT INTO `resource` VALUES (269, '删除', '/tbArticle/delete', 'ajax', NULL, 'fi-page-delete', 240, 0, 0, 1, 1, '2017-07-07 07:42:38');
INSERT INTO `resource` VALUES (270, '删除', '/tbTutor/delete', 'ajax', NULL, 'fi-page-delete', 233, 0, 0, 1, 1, '2017-07-07 07:45:55');
COMMIT;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(64) NOT NULL COMMENT '角色名',
  `seq` tinyint(2) NOT NULL DEFAULT '0' COMMENT '排序号',
  `description` varchar(255) DEFAULT NULL COMMENT '简介',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='角色';

-- ----------------------------
-- Records of role
-- ----------------------------
BEGIN;
INSERT INTO `role` VALUES (1, 'admin', 0, '超级管理员', 0);
INSERT INTO `role` VALUES (2, 'de', 0, '技术部经理', 0);
INSERT INTO `role` VALUES (7, 'pm', 0, '产品部经理', 0);
INSERT INTO `role` VALUES (8, 'test', 0, '测试账户', 0);
COMMIT;

-- ----------------------------
-- Table structure for role_resource
-- ----------------------------
DROP TABLE IF EXISTS `role_resource`;
CREATE TABLE `role_resource` (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `role_id` bigint(19) NOT NULL COMMENT '角色id',
  `resource_id` bigint(19) NOT NULL COMMENT '资源id',
  PRIMARY KEY (`id`),
  KEY `idx_role_resource_ids` (`role_id`,`resource_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1544 DEFAULT CHARSET=utf8 COMMENT='角色资源';

-- ----------------------------
-- Records of role_resource
-- ----------------------------
BEGIN;
INSERT INTO `role_resource` VALUES (1496, 1, 1);
INSERT INTO `role_resource` VALUES (1497, 1, 11);
INSERT INTO `role_resource` VALUES (1502, 1, 12);
INSERT INTO `role_resource` VALUES (1508, 1, 13);
INSERT INTO `role_resource` VALUES (1513, 1, 14);
INSERT INTO `role_resource` VALUES (1498, 1, 111);
INSERT INTO `role_resource` VALUES (1499, 1, 112);
INSERT INTO `role_resource` VALUES (1500, 1, 113);
INSERT INTO `role_resource` VALUES (1501, 1, 114);
INSERT INTO `role_resource` VALUES (1503, 1, 121);
INSERT INTO `role_resource` VALUES (1504, 1, 122);
INSERT INTO `role_resource` VALUES (1505, 1, 123);
INSERT INTO `role_resource` VALUES (1506, 1, 124);
INSERT INTO `role_resource` VALUES (1507, 1, 125);
INSERT INTO `role_resource` VALUES (1509, 1, 131);
INSERT INTO `role_resource` VALUES (1510, 1, 132);
INSERT INTO `role_resource` VALUES (1511, 1, 133);
INSERT INTO `role_resource` VALUES (1512, 1, 134);
INSERT INTO `role_resource` VALUES (1514, 1, 141);
INSERT INTO `role_resource` VALUES (1515, 1, 142);
INSERT INTO `role_resource` VALUES (1516, 1, 143);
INSERT INTO `role_resource` VALUES (1517, 1, 144);
INSERT INTO `role_resource` VALUES (1531, 1, 221);
INSERT INTO `role_resource` VALUES (1535, 1, 226);
INSERT INTO `role_resource` VALUES (1532, 1, 227);
INSERT INTO `role_resource` VALUES (1533, 1, 228);
INSERT INTO `role_resource` VALUES (1534, 1, 229);
INSERT INTO `role_resource` VALUES (1518, 1, 236);
INSERT INTO `role_resource` VALUES (1519, 1, 237);
INSERT INTO `role_resource` VALUES (1523, 1, 238);
INSERT INTO `role_resource` VALUES (1527, 1, 239);
INSERT INTO `role_resource` VALUES (1520, 1, 241);
INSERT INTO `role_resource` VALUES (1521, 1, 242);
INSERT INTO `role_resource` VALUES (1522, 1, 243);
INSERT INTO `role_resource` VALUES (1524, 1, 244);
INSERT INTO `role_resource` VALUES (1525, 1, 245);
INSERT INTO `role_resource` VALUES (1526, 1, 246);
INSERT INTO `role_resource` VALUES (1528, 1, 247);
INSERT INTO `role_resource` VALUES (1529, 1, 248);
INSERT INTO `role_resource` VALUES (1530, 1, 249);
INSERT INTO `role_resource` VALUES (1536, 2, 1);
INSERT INTO `role_resource` VALUES (1537, 2, 13);
INSERT INTO `role_resource` VALUES (1538, 2, 131);
INSERT INTO `role_resource` VALUES (1539, 2, 132);
INSERT INTO `role_resource` VALUES (1540, 2, 133);
INSERT INTO `role_resource` VALUES (1541, 2, 221);
INSERT INTO `role_resource` VALUES (1542, 2, 227);
INSERT INTO `role_resource` VALUES (1543, 2, 228);
INSERT INTO `role_resource` VALUES (158, 3, 1);
INSERT INTO `role_resource` VALUES (159, 3, 11);
INSERT INTO `role_resource` VALUES (164, 3, 12);
INSERT INTO `role_resource` VALUES (170, 3, 13);
INSERT INTO `role_resource` VALUES (175, 3, 14);
INSERT INTO `role_resource` VALUES (160, 3, 111);
INSERT INTO `role_resource` VALUES (161, 3, 112);
INSERT INTO `role_resource` VALUES (162, 3, 113);
INSERT INTO `role_resource` VALUES (163, 3, 114);
INSERT INTO `role_resource` VALUES (165, 3, 121);
INSERT INTO `role_resource` VALUES (166, 3, 122);
INSERT INTO `role_resource` VALUES (167, 3, 123);
INSERT INTO `role_resource` VALUES (168, 3, 124);
INSERT INTO `role_resource` VALUES (169, 3, 125);
INSERT INTO `role_resource` VALUES (171, 3, 131);
INSERT INTO `role_resource` VALUES (172, 3, 132);
INSERT INTO `role_resource` VALUES (173, 3, 133);
INSERT INTO `role_resource` VALUES (174, 3, 134);
INSERT INTO `role_resource` VALUES (176, 3, 141);
INSERT INTO `role_resource` VALUES (177, 3, 142);
INSERT INTO `role_resource` VALUES (178, 3, 143);
INSERT INTO `role_resource` VALUES (179, 3, 144);
INSERT INTO `role_resource` VALUES (1493, 7, 226);
INSERT INTO `role_resource` VALUES (1467, 7, 232);
INSERT INTO `role_resource` VALUES (1478, 7, 233);
INSERT INTO `role_resource` VALUES (1468, 7, 234);
INSERT INTO `role_resource` VALUES (1483, 7, 235);
INSERT INTO `role_resource` VALUES (1488, 7, 240);
INSERT INTO `role_resource` VALUES (1490, 7, 250);
INSERT INTO `role_resource` VALUES (1491, 7, 251);
INSERT INTO `role_resource` VALUES (1492, 7, 252);
INSERT INTO `role_resource` VALUES (1470, 7, 253);
INSERT INTO `role_resource` VALUES (1471, 7, 254);
INSERT INTO `role_resource` VALUES (1472, 7, 255);
INSERT INTO `role_resource` VALUES (1479, 7, 256);
INSERT INTO `role_resource` VALUES (1480, 7, 257);
INSERT INTO `role_resource` VALUES (1481, 7, 258);
INSERT INTO `role_resource` VALUES (1484, 7, 259);
INSERT INTO `role_resource` VALUES (1485, 7, 260);
INSERT INTO `role_resource` VALUES (1486, 7, 261);
INSERT INTO `role_resource` VALUES (1473, 7, 262);
INSERT INTO `role_resource` VALUES (1474, 7, 263);
INSERT INTO `role_resource` VALUES (1475, 7, 264);
INSERT INTO `role_resource` VALUES (1476, 7, 265);
INSERT INTO `role_resource` VALUES (1477, 7, 266);
INSERT INTO `role_resource` VALUES (1487, 7, 267);
INSERT INTO `role_resource` VALUES (1469, 7, 268);
INSERT INTO `role_resource` VALUES (1489, 7, 269);
INSERT INTO `role_resource` VALUES (1482, 7, 270);
INSERT INTO `role_resource` VALUES (1494, 8, 230);
INSERT INTO `role_resource` VALUES (1495, 8, 231);
COMMIT;

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `login_name` varchar(255) DEFAULT NULL COMMENT '登陆名',
  `role_name` varchar(255) DEFAULT NULL COMMENT '角色名',
  `opt_content` varchar(1024) DEFAULT NULL COMMENT '内容',
  `client_ip` varchar(255) DEFAULT NULL COMMENT '客户端ip',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=217 DEFAULT CHARSET=utf8 COMMENT='系统日志';

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for tb_article
-- ----------------------------
DROP TABLE IF EXISTS `tb_article`;
CREATE TABLE `tb_article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `show_time` datetime DEFAULT NULL COMMENT '显示时间',
  `title_img` varchar(255) DEFAULT NULL COMMENT '标题图片',
  `source_type` int(11) DEFAULT NULL COMMENT '来源类型id',
  `intro` text COMMENT '简介',
  `content` longtext COMMENT '信息内容',
  `article_type` int(11) DEFAULT NULL COMMENT '信息类型id',
  `sort_value` int(11) DEFAULT NULL COMMENT '排序值，值越大排越前',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL ,
  `delete_flag` tinyint(2) DEFAULT '0' COMMENT '0-正常，1-删除',
  `status` tinyint(2) DEFAULT '0' COMMENT '0-正常，1-停用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_article
-- ----------------------------
BEGIN;
INSERT INTO `tb_article` VALUES (1, '【MCTalk】在线教育是一场马拉松，来聊聊 2017 在线教育行业的疑问', '2017-06-30 10:33:00', '/upload/image/20170817/1502968397052024653.jpg', 2, 'MCTalk 是网易发起，网易云通信与视频（Netease Multi-media Communication）主导的科技活动品牌，包含线下沙龙、城市论坛、视频访谈等形式。MCTalk 力邀行业顶尖的重量级大咖和新锐精英，共享那些令人惊叹的大脑，与身体力行的创意 。 MCTalk 沙龙系列聚焦风口上的电商、教育、医疗、社交、媒体、智能设备等行业的新场景、新模式、新技术，共享有分量的行业趋势，有价值的前瞻观点，和有温度的经验感悟。MCTalk 沙龙以 Ted 式演说为主、穿插高端圆桌对话与颠覆式产品 Show，阵容涵盖上市公司负责人、顶级 CTO、活跃创业家等。', '[ MCTalk ]&lt;img src=&quot;http://www.gonghoo.com/article/images/1502279990633_17944680.png&quot; title=&quot;&quot; alt=&quot;&quot; width=&quot;290&quot; height=&quot;213&quot; _src=&quot;http://www.gonghoo.com/article/images/1502279990633_17944680.png&quot;&gt;MCTalk 是网易发起，网易云通信与视频（Netease Multi-media Communication）主导的科技活动品牌，包含线下沙龙、城市论坛、视频访谈等形式。MCTalk 力邀行业顶尖的重量级大咖和新锐精英，共享那些令人惊叹的大脑，与身体力行的创意 。 MCTalk 沙龙系列聚焦风口上的电商、教育、医疗、社交、媒体、智能设备等行业的新场景、新模式、新技术，共享有分量的行业趋势，有价值的前瞻观点，和有温度的经验感悟。MCTalk 沙龙以 Ted 式演说为主、穿插高端圆桌对话与颠覆式产品 Show，阵容涵盖上市公司负责人、顶级 CTO、活跃创业家等。[ 活动介绍 ]本期 MCTalk 沙龙以《在线教育是一场马拉松，来聊聊 2017 在线教育行业的疑问》为主题，网易云邀请了站上风口的行业领先者与创新家，同你分享教育行业前沿趋势与独家观点，以及如何在各类商业形态上，重构技术与商业的连接。&lt;img src=&quot;http://www.gonghoo.com/article/images/1502279990911_17944680.png&quot; title=&quot;&quot; alt=&quot;&quot; _src=&quot;http://www.gonghoo.com/article/images/1502279990911_17944680.png&quot;&gt;2017.08.26   13：30地址：浙江杭州滨江区网商路 599 号，网易大厦咨询：0571-89852407    [ 分享嘉宾 ]&lt;img src=&quot;http://www.gonghoo.com/image/20170817/1502962882401038294.jpg&quot; title=&quot;1502962882401038294.jpg&quot; alt=&quot;44.jpg&quot; width=&quot;814&quot; height=&quot;201&quot; _src=&quot;http://www.gonghoo.com/image/20170817/1502962882401038294.jpg&quot;&gt;[ 活动议程 ] 13:30-14:00   签到14:00-14:05   开场14:05-14:35 《网易K12产品解密，如何四维进攻3万亿基础教育市场？ 》14:35-15:05 《少儿英语外教竞争日益激烈，如何做出差异化优势？ 》15:05-15:35 《以双师课堂为例，详解互联网技术如何切中传统教学痛点 》15:35-16:05 《产品视角：在线教育十问》16:05-16:35 《直播能够打破在线教育亏损的魔咒吗？》16:35-17:05   互动探讨17:05-17:10   合影，散场[ 微信群 ]欢迎加入网易云&middot;教育交流群抢先 Get 嘉宾材料、视频链接、回顾文章，与来自政府、高校、互联网的教育专家们实时交流。&lt;img src=&quot;http://www.gonghoo.com/article/images/1502279991539_17944680.png&quot; title=&quot;&quot; alt=&quot;&quot; width=&quot;180&quot; height=&quot;180&quot; _src=&quot;http://www.gonghoo.com/article/images/1502279991539_17944680.png&quot;&gt;先扫码加群主微信：zxremma，由群主确认后加入交流群', 3, 1, '2017-06-28 15:49:17', '2017-08-18 11:01:22', 0, 0);
INSERT INTO `tb_article` VALUES (2, '2017新零售与人工智能论坛', '2017-06-30 10:33:00', '/upload/image/20170817/1502969575306086045.jpg', 2, '2017新零售与人工智能论坛', '&lt;img src=&quot;http://www.gonghoo.com/cms/banner/08041143054963/1501818230359.png&quot; _src=&quot;http://www.gonghoo.com/cms/banner/08041143054963/1501818230359.png&quot;&gt;&lt;img src=&quot;http://www.gonghoo.com/image/20170804/1501818407027038575.png&quot; title=&quot;1501818407027038575.png&quot; alt=&quot;微信图片_20170804101010.png&quot; _src=&quot;http://www.gonghoo.com/image/20170804/1501818407027038575.png&quot;&gt;&lt;img src=&quot;http://www.gonghoo.com/image/20170804/1501818545207009731.jpg&quot; title=&quot;1501818545207009731.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170804/1501818545207009731.jpg&quot;&gt;&lt;img src=&quot;http://www.gonghoo.com/image/20170804/1501818548788089316.jpg&quot; title=&quot;1501818548788089316.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170804/1501818548788089316.jpg&quot;&gt;&lt;img src=&quot;http://www.gonghoo.com/image/20170804/1501818552582024575.jpg&quot; title=&quot;1501818552582024575.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170804/1501818552582024575.jpg&quot;&gt;&lt;img src=&quot;http://www.gonghoo.com/image/20170804/1501818556067033722.jpg&quot; title=&quot;1501818556067033722.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170804/1501818556067033722.jpg&quot;&gt;&lt;img src=&quot;http://www.gonghoo.com/image/20170804/1501818584197014779.jpg&quot; title=&quot;1501818584197014779.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170804/1501818584197014779.jpg&quot;&gt;&lt;img src=&quot;http://www.gonghoo.com/image/20170804/1501818588260025193.jpg&quot; title=&quot;1501818588260025193.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170804/1501818588260025193.jpg&quot;&gt;', 3, 1, '2017-06-28 16:22:01', '2017-08-18 11:01:32', 0, 0);
INSERT INTO `tb_article` VALUES (3, '2017中国首席技术官大会暨第四届中国（杭州）移动互联网大会', '2017-07-04 13:34:00', '/upload/image/20170817/1502969945401057122.jpg', 2, '由中国首席技术官联盟、科技部现代服务产业联盟主办、杭州市移动互联网技术学会联合承办的 &ldquo;2017中国首席技术官大会（China CTO Congress 2017,简称CCTOC2017）&rdquo;将于2017年9月22日在浙江省杭州市召开。大会旨在为IT和互联网相关领域企业的首席技术官、技术总监、总工程师、研发总监、技术部经理、产品经理、系统架构师、高级技术专家等技术精英人群提供一个创新创业和技术交流的平台，共同促进国内IT技术的发展。大会以&ldquo;数据驱动智慧中国&rdquo;为主题，将吸引超过2,000位知名互联网企业的技术精英参会交流。', '大会简介      由中国首席技术官联盟、科技部现代服务产业联盟主办、杭州市移动互联网技术学会联合承办的 &ldquo;2017中国首席技术官大会（China CTO Congress 2017,简称CCTOC2017）&rdquo;将于2017年9月22日在浙江省杭州市召开。大会旨在为IT和互联网相关领域企业的首席技术官、技术总监、总工程师、研发总监、技术部经理、产品经理、系统架构师、高级技术专家等技术精英人群提供一个创新创业和技术交流的平台，共同促进国内IT技术的发展。大会以&ldquo;数据驱动智慧中国&rdquo;为主题，将吸引超过2,000位知名互联网企业的技术精英参会交流。&lt;img src=&quot;http://www.gonghoo.com/image/20170724/1500876440253052344.png&quot; title=&quot;1500876440253052344.png&quot; alt=&quot;CTO嘉宾图.png&quot; _src=&quot;http://www.gonghoo.com/image/20170724/1500876440253052344.png&quot;&gt;大会议程&lt;img src=&quot;http://www.gonghoo.com/image/20170811/1502429502737004459.png&quot; title=&quot;1502429502737004459.png&quot; alt=&quot;2017年中国首席技术官大会-时间表.png&quot; _src=&quot;http://www.gonghoo.com/image/20170811/1502429502737004459.png&quot;&gt;主会场&lt;img src=&quot;http://www.gonghoo.com/image/20170817/1502941569657039157.jpg&quot; title=&quot;1502941569657039157.jpg&quot; alt=&quot;微信图片_20170817105621.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170817/1502941569657039157.jpg&quot;&gt;展位效果&lt;img src=&quot;http://www.gonghoo.com/image/20170817/1502941904329025650.jpg&quot; title=&quot;1502941904329025650.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170817/1502941904329025650.jpg&quot;&gt;&lt;img src=&quot;http://www.gonghoo.com/image/20170817/1502941911722088410.jpg&quot; title=&quot;1502941911722088410.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170817/1502941911722088410.jpg&quot;&gt;组织架构&lt;img src=&quot;http://www.gonghoo.com/image/20170817/1502959969888008944.png&quot; title=&quot;1502959969888008944.png&quot; alt=&quot;中国日报@2x.png&quot; _src=&quot;http://www.gonghoo.com/image/20170817/1502959969888008944.png&quot;&gt;&lt;img src=&quot;http://www.gonghoo.com/image/20170708/1499503917387009268.png&quot; title=&quot;1499503917387009268.png&quot; alt=&quot;CTO-往期回顾.png&quot; _src=&quot;http://www.gonghoo.com/image/20170708/1499503917387009268.png&quot;&gt;', 3, 1, '2017-06-28 17:34:52', '2017-08-18 11:01:41', 0, 0);
INSERT INTO `tb_article` VALUES (4, '初橙*阿里校友创业黄埔榜之投融资峰会', '2017-07-05 14:05:00', '/upload/image/20170817/1502970004837072102.jpg', 2, '截止到去年年底&ldquo;阿里校友创业黄埔榜&rdquo;最终收录了阿里校友创业项目高达680个，总估值高达10000亿！为了能够更好的帮助这些项目获得&ldquo;资本甘露&rdquo;，我们已连续三年，每年都举办一场风险投资大会，并获得阿里校友及资本界的强烈响应！', '前言截止到去年年底&ldquo;阿里校友创业黄埔榜&rdquo;最终收录了阿里校友创业项目高达680个，总估值高达10000亿！为了能够更好的帮助这些项目获得&ldquo;资本甘露&rdquo;，我们已连续三年，每年都举办一场风险投资大会，并获得阿里校友及资本界的强烈响应！2017年7月28日，初橙阿里校友创业黄埔榜&middot;风险投资峰会又将席卷而来！这次大会是初橙年中盘点的重要会议，我们将倾尽所有力量，励志做成汇聚顶级投资机构及阿里校友创业项目数量最多且质量最高的一场风险投资峰会！以下是您来可以面对面交流的嘉宾阵容：&lt;img src=&quot;http://www.gonghoo.com/image/20170703/1499063133754038386.jpg&quot; title=&quot;1499063133754038386.jpg&quot; alt=&quot;lADO5DPeRc0CWM0DIA_800_600.jpg_620x10000q90g.jpg&quot; width=&quot;640&quot; height=&quot;1064&quot; _src=&quot;http://www.gonghoo.com/image/20170703/1499063133754038386.jpg&quot;&gt;&lt;img src=&quot;http://www.gonghoo.com/image/20170628/1498642795324042686.jpg&quot; title=&quot;1498642795324042686.jpg&quot; alt=&quot;640.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170628/1498642795324042686.jpg&quot;&gt;大咖持续邀约中，敬请期待！', 3, 1, '2017-06-28 18:06:46', '2017-08-18 11:01:48', 0, 0);
INSERT INTO `tb_article` VALUES (5, '【网易云创CIO沙龙|广州站】从0到1赋能互联网+企业转型', '2017-07-02 20:20:00', '/upload/image/20170817/1502970109326035431.jpg', 2, '网易云创CIO沙龙是由网易主办，网易云承办，以&ldquo;从0到1赋能互联网+企业转型&rdquo;为主题的一次企业转型升级闭门研讨会，将在杭州、广州、北京、上海等13个城市开展巡回沙龙，旨在与各地企业共同探讨在互联网+时代如何从0到1完成转型。', '一、活动背景       网易云创CIO沙龙是由网易主办，网易云承办，以&ldquo;从0到1赋能互联网+企业转型&rdquo;为主题的一次企业转型升级闭门研讨会，将在杭州、广州、北京、上海等13个城市开展巡回沙龙，旨在与各地企业共同探讨在互联网+时代如何从0到1完成转型。       本次沙龙诚邀广州地区具有技术匠心的企业以及业内专家、网易产品、解决方案专家，就商业模式创新、技术赋能、产业数字化升级等话题展开思想碰撞，分享并探讨商业与技术结合的最佳实践等话题，洞见各行业未来发展的新方向。二、活动概况时间：2017年8月31日 14:00地点：广州 丽思卡尔顿酒店形式：闭门沙龙规模：50人 三、活动亮点网易20年技术巡礼      网易20年技术沉淀首次走进广州，与IT领袖深入研讨云计算如何助力传统企业互联网+；1对1深度解答     深入解读网易司南战略，剖析网易专属云计划，网易云解决方案专家与您面对面讨论；网易猪肉现场品尝   一场最富网易特色的闭门宴会，私享IT大佬最爱网易黑猪肉、伴手礼；四、活动议程13:30-14:00嘉宾签到14:00-14:20解读广州互联网+转型发展新形势转型家 CEO 彭芳泉14:20-14:50网易知识体系：三步赋能传统行业互联网+转型网易云互联网+行业部总经理  岳峥辉14:50-15:20网易&ldquo;互联网+&rdquo;实战分享：严选如何从0到1定义新电商网易云解决方案总架构师 刘超15:20-15:50网易云双引擎让企业互联网+转型落地网易云互联网+首席架构师  李鲁15:50-17:20网易黑猪宴+茶歇分组讨论总结', 3, 1, '2017-06-28 18:21:15', '2017-08-18 11:01:57', 0, 0);
INSERT INTO `tb_article` VALUES (6, '2017网易中国创业家大赛', '2017-07-03 10:22:00', '/upload/image/20170817/1502970409346028016.jpg', 2, '2017网易中国创业家大赛', ' &lt;img src=&quot;http://www.gonghoo.com/image/20170717/1500262523891038779.jpg&quot; title=&quot;1500262523891038779.jpg&quot; alt=&quot;信息长图0714 17点(小图).jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170717/1500262523891038779.jpg&quot;&gt;', 3, 1, '2017-06-28 18:22:29', '2017-08-18 11:02:04', 0, 0);
INSERT INTO `tb_article` VALUES (7, '前6届大会融资8.9亿，第７届BAT精英巅峰会再次席卷杭城创业圈，带着200亿资金，看你的BP！', '2017-07-05 08:33:00', '/upload/image/20170817/1502970521386074745.jpg', 2, '前6届大会融资8.9亿，第７届BAT精英巅峰会再次席卷杭城创业圈，带着200亿资金，看你的BP！', '&lt;p style=&quot;font: 14px/20px 宋体; margin: 0px; padding: 0px; height: auto !important; text-align: center; color: rgb(51, 51, 51); text-transform: none; text-indent: 0px; letter-spacing: 1px; word-spacing: 0px; white-space: normal; -ms-word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important; widows: 1; font-size-adjust: none; font-stretch: normal; background-color: rgb(255, 255, 255); -webkit-tap-highlight-color: transparent; -webkit-text-stroke-width: 0px;&quot;&gt;&lt;img title=&quot;&quot; class=&quot;lazy&quot; alt=&quot;&quot; src=&quot;http://www.gonghoo.com/article/images/1493703841699_17944680.png@1280w&quot; _src=&quot;http://www.gonghoo.com/article/images/1493703841699_17944680.png@1280w&quot;&gt;&lt;img title=&quot;&quot; class=&quot;lazy&quot; alt=&quot;&quot; src=&quot;http://www.gonghoo.com/article/images/1493703842088_17944680.png@1280w&quot; _src=&quot;http://www.gonghoo.com/article/images/1493703842088_17944680.png@1280w&quot;&gt;&lt;p style=&quot;font: 14px/20px 宋体; margin: 0px; padding: 0px; height: auto !important; text-align: center; color: rgb(51, 51, 51); text-transform: none; text-indent: 0px; letter-spacing: 1px; word-spacing: 0px; white-space: normal; -ms-word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important; widows: 1; font-size-adjust: none; font-stretch: normal; background-color: rgb(255, 255, 255); -webkit-tap-highlight-color: transparent; -webkit-text-stroke-width: 0px;&quot;&gt;&lt;img title=&quot;&quot; class=&quot;lazy&quot; alt=&quot;&quot; src=&quot;http://www.gonghoo.com/article/images/1493703842363_17944680.png@1280w&quot; _src=&quot;http://www.gonghoo.com/article/images/1493703842363_17944680.png@1280w&quot;&gt;&lt;p style=&quot;font: 14px/20px 宋体; margin: 0px; padding: 0px; height: auto !important; text-align: center; color: rgb(51, 51, 51); text-transform: none; text-indent: 0px; letter-spacing: 1px; word-spacing: 0px; white-space: normal; -ms-word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important; widows: 1; font-size-adjust: none; font-stretch: normal; background-color: rgb(255, 255, 255); -webkit-tap-highlight-color: transparent; -webkit-text-stroke-width: 0px;&quot;&gt;&lt;img title=&quot;&quot; class=&quot;lazy&quot; alt=&quot;&quot; src=&quot;http://www.gonghoo.com/article/images/1493703842562_17944680.png@1280w&quot; _src=&quot;http://www.gonghoo.com/article/images/1493703842562_17944680.png@1280w&quot;&gt;&lt;img title=&quot;&quot; class=&quot;lazy&quot; alt=&quot;&quot; src=&quot;http://www.gonghoo.com/article/images/1493703842634_17944680.jpg@1440w&quot; _src=&quot;http://www.gonghoo.com/article/images/1493703842634_17944680.jpg@1440w&quot;&gt;&lt;p style=&quot;font: 14px/20px 宋体; margin: 0px; padding: 0px; height: auto !important; text-align: center; color: rgb(51, 51, 51); text-transform: none; text-indent: 0px; letter-spacing: 1px; word-spacing: 0px; white-space: normal; -ms-word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important; widows: 1; font-size-adjust: none; font-stretch: normal; background-color: rgb(255, 255, 255); -webkit-tap-highlight-color: transparent; -webkit-text-stroke-width: 0px;&quot;&gt;&lt;img title=&quot;&quot; class=&quot;lazy&quot; alt=&quot;&quot; src=&quot;http://www.gonghoo.com/article/images/1493703842771_17944680.png@1280w&quot; _src=&quot;http://www.gonghoo.com/article/images/1493703842771_17944680.png@1280w&quot;&gt;拟邀&lt;p style=&quot;font: 14px/20px 宋体; margin: 0px; padding: 0px; height: auto !important; text-align: center; color: rgb(51, 51, 51); text-transform: none; text-indent: 0px; letter-spacing: 1px; word-spacing: 0px; white-space: normal; -ms-word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important; widows: 1; font-size-adjust: none; font-stretch: normal; background-color: rgb(255, 255, 255); -webkit-tap-highlight-color: transparent; -webkit-text-stroke-width: 0px;&quot;&gt;&lt;img title=&quot;&quot; class=&quot;lazy&quot; alt=&quot;&quot; src=&quot;http://www.gonghoo.com/article/images/1493703842810_17944680.jpg@1080w&quot; _src=&quot;http://www.gonghoo.com/article/images/1493703842810_17944680.jpg@1080w&quot;&gt;&lt;p style=&quot;font: 14px/20px 宋体; margin: 0px; padding: 0px; height: auto !important; text-align: center; color: rgb(51, 51, 51); text-transform: none; text-indent: 0px; letter-spacing: 1px; word-spacing: 0px; white-space: normal; -ms-word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important; widows: 1; font-size-adjust: none; font-stretch: normal; background-color: rgb(255, 255, 255); -webkit-tap-highlight-color: transparent; -webkit-text-stroke-width: 0px;&quot;&gt;&lt;img title=&quot;&quot; class=&quot;lazy&quot; alt=&quot;&quot; src=&quot;http://www.gonghoo.com/article/images/1493703843038_17944680.png@1280w&quot; _src=&quot;http://www.gonghoo.com/article/images/1493703843038_17944680.png@1280w&quot;&gt;&lt;p style=&quot;font: 14px/20px 宋体; margin: 0px; padding: 0px; height: auto !important; text-align: center; color: rgb(51, 51, 51); text-transform: none; text-indent: 0px; letter-spacing: 1px; word-spacing: 0px; white-space: normal; -ms-word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important; widows: 1; font-size-adjust: none; font-stretch: normal; background-color: rgb(255, 255, 255); -webkit-tap-highlight-color: transparent; -webkit-text-stroke-width: 0px;&quot;&gt;&lt;img title=&quot;&quot; class=&quot;lazy&quot; alt=&quot;&quot; src=&quot;http://www.gonghoo.com/article/images/1493703843109_17944680.png@1280w&quot; _src=&quot;http://www.gonghoo.com/article/images/1493703843109_17944680.png@1280w&quot;&gt;&lt;img title=&quot;&quot; class=&quot;lazy&quot; alt=&quot;&quot; src=&quot;http://www.gonghoo.com/article/images/1493703843320_17944680.png@1280w&quot; _src=&quot;http://www.gonghoo.com/article/images/1493703843320_17944680.png@1280w&quot;&gt;&lt;img title=&quot;&quot; class=&quot;lazy&quot; alt=&quot;&quot; src=&quot;http://www.gonghoo.com/article/images/1493703843508_17944680.png@1280w&quot; _src=&quot;http://www.gonghoo.com/article/images/1493703843508_17944680.png@1280w&quot;&gt;&lt;p style=&quot;font: 14px/20px 宋体; margin: 0px; padding: 0px; height: auto !important; text-align: center; color: rgb(51, 51, 51); text-transform: none; text-indent: 0px; letter-spacing: 1px; word-spacing: 0px; white-space: normal; -ms-word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important; widows: 1; font-size-adjust: none; font-stretch: normal; background-color: rgb(255, 255, 255); -webkit-tap-highlight-color: transparent; -webkit-text-stroke-width: 0px;&quot;&gt;&lt;img title=&quot;&quot; class=&quot;lazy&quot; alt=&quot;&quot; src=&quot;http://www.gonghoo.com/article/images/1493703843607_17944680.jpg@720w&quot; _src=&quot;http://www.gonghoo.com/article/images/1493703843607_17944680.jpg@720w&quot;&gt;&lt;p style=&quot;font: 14px/20px 宋体; margin: 0px; padding: 0px; height: auto !important; text-align: center; color: rgb(51, 51, 51); text-transform: none; text-indent: 0px; letter-spacing: 1px; word-spacing: 0px; white-space: normal; -ms-word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important; widows: 1; font-size-adjust: none; font-stretch: normal; background-color: rgb(255, 255, 255); -webkit-tap-highlight-color: transparent; -webkit-text-stroke-width: 0px;&quot;&gt;&lt;img title=&quot;&quot; class=&quot;lazy&quot; alt=&quot;&quot; src=&quot;http://www.gonghoo.com/article/images/1493703843700_17944680.png@1280w&quot; _src=&quot;http://www.gonghoo.com/article/images/1493703843700_17944680.png@1280w&quot;&gt;&lt;p style=&quot;font: 14px/20px 宋体; margin: 0px; padding: 0px; height: auto !important; text-align: center; color: rgb(51, 51, 51); text-transform: none; text-indent: 0px; letter-spacing: 1px; word-spacing: 0px; white-space: normal; -ms-word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important; widows: 1; font-size-adjust: none; font-stretch: normal; background-color: rgb(255, 255, 255); -webkit-tap-highlight-color: transparent; -webkit-text-stroke-width: 0px;&quot;&gt;&lt;img title=&quot;&quot; class=&quot;lazy&quot; alt=&quot;&quot; src=&quot;http://www.gonghoo.com/article/images/1493703843901_17944680.png@1280w&quot; _src=&quot;http://www.gonghoo.com/article/images/1493703843901_17944680.png@1280w&quot;&gt;&lt;p style=&quot;font: 14px/20px 宋体; margin: 0px; padding: 0px; height: auto !important; text-align: center; color: rgb(51, 51, 51); text-transform: none; text-indent: 0px; letter-spacing: 1px; word-spacing: 0px; white-space: normal; -ms-word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important; widows: 1; font-size-adjust: none; font-stretch: normal; background-color: rgb(255, 255, 255); -webkit-tap-highlight-color: transparent; -webkit-text-stroke-width: 0px;&quot;&gt;全国8大分站巅峰会报名入口：&lt;p style=&quot;font: 14px/28px &quot;&gt;巅峰会武汉站：（时间暂定6月30日）&lt;p style=&quot;font: 14px/28px &quot;&gt;&lt;a style=&quot;height: auto !important; font-size: 12px; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://h5.welian.com/event/i/Mjg4NDU=&quot; target=&quot;_blank&quot;&gt;http://h5.welian.com/event/i/Mjg4NDU=&lt;p style=&quot;font: 14px/28px &quot;&gt;&lt;a style=&quot;height: auto !important; font-size: 12px; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://h5.welian.com/event/i/Mjg4NDU=&quot; target=&quot;_blank&quot;&gt;巅峰会北京站：&lt;a style=&quot;height: auto !important; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://www.huodongxing.com/event/4363407003400&quot; target=&quot;_blank&quot;&gt;（时间确定4月8日）&lt;p style=&quot;font: 14px/28px &quot;&gt;&lt;a style=&quot;height: auto !important; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://h5.welian.com/event/i/Mjg5NzE=&quot; target=&quot;_blank&quot;&gt;http://h5.welian.com/event/i/Mjg5NzE=&lt;p style=&quot;font: 14px/28px &quot;&gt;&lt;a style=&quot;height: auto !important; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://h5.welian.com/event/i/Mjg5NzE=&quot; target=&quot;_blank&quot;&gt;巅峰会杭州站：&lt;a style=&quot;height: auto !important; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://www.huodongxing.com/event/4363407003400&quot; target=&quot;_blank&quot;&gt;（时间暂定5月28日）&lt;p style=&quot;font: 14px/28px &quot;&gt;&lt;a style=&quot;height: auto !important; font-size: 12px; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://h5.welian.com/event/i/Mjg5NTM=&quot; target=&quot;_blank&quot;&gt;http://h5.welian.com/event/i/Mjg5NTM=&lt;p style=&quot;font: 14px/28px &quot;&gt;&lt;a style=&quot;height: auto !important; font-size: 12px; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://h5.welian.com/event/i/Mjg5NTM=&quot; target=&quot;_blank&quot;&gt;巅峰会成都站：&lt;a style=&quot;height: auto !important; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://www.huodongxing.com/event/4363407003400&quot; target=&quot;_blank&quot;&gt;（时间&lt;a style=&quot;height: auto !important; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://www.huodongxing.com/event/4363407003400&quot; target=&quot;_blank&quot;&gt;暂定6月24日）&lt;p style=&quot;font: 14px/28px &quot;&gt;&lt;a style=&quot;height: auto !important; font-size: 12px; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://h5.welian.com/event/i/Mjg5NTc=&quot; target=&quot;_self&quot;&gt;http://h5.welian.com/event/i/Mjg5NTc=&lt;p style=&quot;font: 14px/28px &quot;&gt;&lt;a style=&quot;height: auto !important; font-size: 12px; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://h5.welian.com/event/i/Mjg5NTc=&quot; target=&quot;_self&quot;&gt;巅峰会上海站：&lt;a style=&quot;height: auto !important; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://www.huodongxing.com/event/4363407003400&quot; target=&quot;_blank&quot;&gt;（时间&lt;a style=&quot;height: auto !important; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://www.huodongxing.com/event/4363407003400&quot; target=&quot;_blank&quot;&gt;&lt;a style=&quot;height: auto !important; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://www.huodongxing.com/event/4363407003400&quot; target=&quot;_blank&quot;&gt;暂定7月22日）&lt;p style=&quot;font: 14px/28px &quot;&gt;&lt;a style=&quot;height: auto !important; font-size: 12px; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://h5.welian.com/event/i/Mjg5NTk=&quot; target=&quot;_blank&quot;&gt;http://h5.welian.com/event/i/Mjg5NTk=&lt;p style=&quot;font: 14px/28px &quot;&gt;&lt;a style=&quot;height: auto !important; font-size: 12px; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://h5.welian.com/event/i/Mjg5NTk=&quot; target=&quot;_blank&quot;&gt;巅峰会厦门站：&lt;a style=&quot;height: auto !important; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://www.huodongxing.com/event/4363407003400&quot; target=&quot;_blank&quot;&gt;（时间&lt;a style=&quot;height: auto !important; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://www.huodongxing.com/event/4363407003400&quot; target=&quot;_blank&quot;&gt;&lt;a style=&quot;height: auto !important; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://www.huodongxing.com/event/4363407003400&quot; target=&quot;_blank&quot;&gt;暂定8月26日）&lt;p style=&quot;font: 14px/28px &quot;&gt;&lt;a style=&quot;height: auto !important; font-size: 12px; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://h5.welian.com/event/i/Mjg5NjE=&quot; target=&quot;_self&quot;&gt;http://h5.welian.com/event/i/Mjg5NjE=&lt;p style=&quot;font: 14px/28px &quot;&gt;&lt;a style=&quot;height: auto !important; font-size: 12px; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://h5.welian.com/event/i/Mjg5NjE=&quot; target=&quot;_self&quot;&gt;巅峰会深圳站：&lt;a style=&quot;height: auto !important; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://www.huodongxing.com/event/4363407003400&quot; target=&quot;_blank&quot;&gt;（时间&lt;a style=&quot;height: auto !important; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://www.huodongxing.com/event/4363407003400&quot; target=&quot;_blank&quot;&gt;&lt;a style=&quot;height: auto !important; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://www.huodongxing.com/event/4363407003400&quot; target=&quot;_blank&quot;&gt;暂定9月23日）&lt;p style=&quot;font: 14px/28px &quot;&gt;&lt;a style=&quot;height: auto !important; font-size: 12px; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://h5.welian.com/event/i/Mjg5NjM=&quot; target=&quot;_blank&quot;&gt;http://h5.welian.com/event/i/Mjg5NjM=&lt;p style=&quot;font: 14px/28px &quot;&gt;&lt;a style=&quot;height: auto !important; font-size: 12px; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://h5.welian.com/event/i/Mjg5NjM=&quot; target=&quot;_blank&quot;&gt;巅峰会广州站：&lt;a style=&quot;height: auto !important; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://www.huodongxing.com/event/4363407003400&quot; target=&quot;_blank&quot;&gt;（时间&lt;a style=&quot;height: auto !important; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://www.huodongxing.com/event/4363407003400&quot; target=&quot;_blank&quot;&gt;&lt;a style=&quot;height: auto !important; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://www.huodongxing.com/event/4363407003400&quot; target=&quot;_blank&quot;&gt;暂定10月21日）&lt;p style=&quot;font: 14px/28px &quot;&gt;&lt;a style=&quot;height: auto !important; font-size: 12px; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://h5.welian.com/event/i/Mjg5NjU=&quot; target=&quot;_blank&quot;&gt;http://h5.welian.com/event/i/Mjg5NjU=&lt;p style=&quot;font: 14px/20px 宋体; margin: 0px; padding: 0px; height: auto !important; text-align: center; color: rgb(51, 51, 51); text-transform: none; text-indent: 0px; letter-spacing: 1px; word-spacing: 0px; white-space: normal; -ms-word-wrap: break-word !important; max-width: 100%; box-sizing: border-box; widows: 1; font-size-adjust: none; font-stretch: normal; background-color: rgb(255, 255, 255); -webkit-tap-highlight-color: transparent; -webkit-text-stroke-width: 0px;&quot;&gt;&lt;a style=&quot;height: auto !important; font-size: 12px; text-decoration: none; white-space: normal !important; max-width: 100%; box-sizing: border-box; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://h5.welian.com/event/i/Mjg5NjU=&quot; target=&quot;_blank&quot;&gt;&lt;p style=&quot;font: 14px/20px 宋体; margin: 0px; padding: 0px; height: auto !important; text-align: center; color: rgb(51, 51, 51); text-transform: none; text-indent: 0px; letter-spacing: 1px; word-spacing: 0px; white-space: normal; -ms-word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important; widows: 1; font-size-adjust: none; font-stretch: normal; background-color: rgb(255, 255, 255); -webkit-tap-highlight-color: transparent; -webkit-text-stroke-width: 0px;&quot;&gt;&lt;a style=&quot;height: auto !important; text-decoration: none; white-space: normal !important; -ms-word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://www.huodongxing.com/event/5370167861400&quot; target=&quot;_blank&quot;&gt;&lt;p style=&quot;font: 14px/20px 宋体; margin: 0px; padding: 0px; height: auto !important; text-align: center; color: rgb(51, 51, 51); text-transform: none; text-indent: 0px; letter-spacing: 1px; word-spacing: 0px; white-space: normal; -ms-word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important; widows: 1; font-size-adjust: none; font-stretch: normal; background-color: rgb(255, 255, 255); -webkit-tap-highlight-color: transparent; -webkit-text-stroke-width: 0px;&quot;&gt;&lt;a style=&quot;height: auto !important; text-decoration: none; white-space: normal !important; -ms-word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important; -webkit-tap-highlight-color: transparent;&quot; href=&quot;http://www.huodongxing.com/event/5370167861400&quot; target=&quot;_blank&quot;&gt;&lt;img title=&quot;&quot; class=&quot;lazy&quot; alt=&quot;&quot; src=&quot;http://www.gonghoo.com/article/images/1493703843997_17944680.png@1280w&quot; _src=&quot;http://www.gonghoo.com/article/images/1493703843997_17944680.png@1280w&quot;&gt;&lt;img title=&quot;&quot; class=&quot;lazy&quot; alt=&quot;&quot; src=&quot;http://www.gonghoo.com/article/images/1493703844234_17944680.png@1280w&quot; _src=&quot;http://www.gonghoo.com/article/images/1493703844234_17944680.png@1280w&quot;&gt;&lt;img title=&quot;&quot; class=&quot;lazy&quot; alt=&quot;&quot; src=&quot;http://www.gonghoo.com/article/images/1493703844479_17944680.png@1280w&quot; _src=&quot;http://www.gonghoo.com/article/images/1493703844479_17944680.png@1280w&quot;&gt;', 3, 1, '2017-06-28 22:38:15', '2017-08-18 11:02:11', 0, 0);
INSERT INTO `tb_article` VALUES (8, '预测未来最好的方式就是创造未来｜美国最前沿AR/MR课程体验', '2017-05-20 13:00:00', '/upload/image/20170817/1502970752578032476.jpg', 1, '到2017年年底，AR市场将增长至52亿美元，年增长率竟逼近100%。随着大量资金注入AR项目及AR创业公司，尤其是随着谷歌、佳能、高通、微软等大公司的入场，我们已经看到第一批消费级AR产品的涌现。随着实际商业利益的出现，AR将成为消费、医疗、移动、汽车以及制造市场中的&ldquo;下一件大事&rdquo;。', '一、AR/MR的发展历史&lt;img src=&quot;http://www.gonghoo.com/image/20170511/1494466760307069424.jpg&quot; title=&quot;1494466760307069424.jpg&quot; alt=&quot;01.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170511/1494466760307069424.jpg&quot;&gt;二、AR/MR将引领下一代科技革命浪潮这是一个&ldquo;黑科技&rdquo;层出不穷的时代，大数据、云计算、物联网、工业4.0等概念频出，智能设备、人工智能、传感交互、深度学习等技术正从神坛落地，走入现实场景中。除了物联网、人工智能、智能硬件、车联网、智能生活等常规的戏码之外，和VR虚拟现实、AR/MR增强现实相关的设备及技术一下子成为了最大的&ldquo;黑马&rdquo;，在给观众带来震撼般的体验和感知的同时，也宣告了一个全新的交互体验新时代的开启。&lt;img src=&quot;http://www.gonghoo.com/image/20170511/1494466784715052425.png&quot; title=&quot;1494466784715052425.png&quot; alt=&quot;02.png&quot; _src=&quot;http://www.gonghoo.com/image/20170511/1494466784715052425.png&quot;&gt;AR/MR会带来一个&ldquo;后互联网&rdquo;时代。可以想象，目前的互联网、移动互联网讲的是入口和用户体验，通过大的入口效应，来与用户的应用和服务需求接驳，AR/MR技术改变的就是前端与用户连接的&ldquo;接触层&rdquo;，让接入和交互的方式更自然，甚至与现实完全叠加在一起，这就意味着，过去移动互联网、互联网所提供的入口将被彻底&ldquo;解耦&rdquo;，与现实世界高度对接在一起。&ldquo;后互联网时代&rdquo;是一个用AR/MR设备、应用及技术取代各个服务入口的形态，而真正的入口会被高度分散、匹配到现实世界中任何一个真实场景中，用眼球、手势等最自然的交互方式来开启、调用服务，同时也是场景服务时代的加强版，让场景的体验更极致，更接近真实。AR/MR将开启智能穿戴领域的新纪元。三、未来AR/MR的应用领域更广泛     到2017年年底，AR市场将增长至52亿美元，年增长率竟逼近100%。随着大量资金注入AR项目及AR创业公司，尤其是随着谷歌、佳能、高通、微软等大公司的入场，我们已经看到第一批消费级AR产品的涌现。随着实际商业利益的出现，AR将成为消费、医疗、移动、汽车以及制造市场中的&ldquo;下一件大事&rdquo;。在个人信息消费领域、商业应用领域、政府及其他领域，颠覆现有的众多应用模式。&lt;img src=&quot;http://www.gonghoo.com/image/20170511/1494466807089065379.jpg&quot; title=&quot;1494466807089065379.jpg&quot; alt=&quot;03.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170511/1494466807089065379.jpg&quot;&gt;&middot;《星战前传》的全息影像沟通，显示效果体验更好，双目透视三维空间实现虚拟和现实结合，参与感强于虚拟现实；&lt;img src=&quot;http://www.gonghoo.com/image/20170511/1494466820269018481.png&quot; title=&quot;1494466820269018481.png&quot; alt=&quot;04.png&quot; _src=&quot;http://www.gonghoo.com/image/20170511/1494466820269018481.png&quot;&gt;&middot; 远程教育打破传统教育的时空限制，能够用手势和体感操控全息影像；&lt;img src=&quot;http://www.gonghoo.com/image/20170511/1494466828405041072.jpg&quot; title=&quot;1494466828405041072.jpg&quot; alt=&quot;05.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170511/1494466828405041072.jpg&quot;&gt;&middot; 在现实环境中学习和指导医学培训。为您的患者规划和研究适当的医疗保健解决方案；&lt;img src=&quot;http://www.gonghoo.com/image/20170511/1494466839442001905.jpg&quot; title=&quot;1494466839442001905.jpg&quot; alt=&quot;06.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170511/1494466839442001905.jpg&quot;&gt;&middot;增强现实场景下的游戏场景；&lt;img src=&quot;http://www.gonghoo.com/image/20170511/1494466855462003783.gif&quot; title=&quot;1494466855462003783.gif&quot; alt=&quot;07.gif&quot; _src=&quot;http://www.gonghoo.com/image/20170511/1494466855462003783.gif&quot;&gt;&middot; 未来的全息工程设计，对信息展示方式、媒介及人机交互方式带来了巨大变化， 无需使用者进行任何思维加工；呈现在观察者面前的投影除了无法碰触以外，与实物并无差别。&lt;img src=&quot;http://www.gonghoo.com/image/20170511/1494466868015029631.jpg&quot; title=&quot;1494466868015029631.jpg&quot; alt=&quot;08.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170511/1494466868015029631.jpg&quot;&gt;数据显示，2014和2015年期间，国内外投资机构对于AR增强现实领域的投资开始大幅增加，累计投资额已经达数十亿美元，投资主要集中在AR智能硬件和AR底层技术算法上面，看得出来投资市场对未来智能硬件发展的信心。而另外一组数据也显示，到2020年，AR和VR 市场收入规模将达到1500亿美元，其中AR规模为1200亿美元，超越VR成为又一片创新的沃土。科技改变生活，我们有理由相信，随着AR技术在行业应用的积累，AR技术在消费者应用也会走向成熟和爆发， AR增强现实真正步入经济和生活也不再遥远。四、课程体系AR/MR课程体系WyzLink Inc.(以下简称WyzLink) 是一家诞生于美国西雅图的基于AR/VR/MR、大数据和人工智能前沿技术及应用的职业培训平台。 在北美与多个州立大学合作。WyzLink 拥有创新的课程设计理念及教学经验，目前已经与Bellevue 市政府, Bellevue College, Amdocs, Coding dojo, EHang, 微软和清华启迪建立了业务合作关系，培训讲师都来自美国顶尖科技公司的优秀科技人员和领军人物。WyzLink的混合现实课程也已在美国卡内基&middot;梅隆大学计算机学院和华盛顿州Bellevue College 正式授课。 五、课程内容1.了解 VR/AR/MR 行业的现状及未来发展趋势     2.典型的应用场景（教育、科研、医疗、娱乐、消费、设计...）     3.分享和 AR/VR/MR 相关的最前沿的信息（主要团队来自美国，和微软、ODG 深度合作）4.引领新一代人机交互方式 ,体验黑科技（HoloLens/ODG）5. 问答环节          六、课程亮点1.成熟课程，已经在中美高校教课2.以微软的HoloLens和 ODG作为授课设备，课程内容适用多种设备3.学员亲自体验AR技术与设备，体验未来，参与未来4.全面了解AR开发环境，技术，量身定制AR应用课程5.了解开发技术，激发产品创意6.体会三维动漫，智能制造和医疗教育  七、授课讲师 &lt;img src=&quot;http://www.gonghoo.com/image/20170511/1494466885516094438.png&quot; title=&quot;1494466885516094438.png&quot; alt=&quot;09.png&quot; _src=&quot;http://www.gonghoo.com/image/20170511/1494466885516094438.png&quot;&gt;于昕混合现实解决方案专家毕业于清华大学，WyzLink中国公司COO，从事IT行业20年，在微软从事企业解决方案设计及开发多年，在支付、智能制造、智慧城市等行业及AR/MR大数据及人工智能等新技术领域，混合现实应用研发拥有丰富的研发经验。  八、活动嘉宾 &lt;img src=&quot;http://www.gonghoo.com/image/20170511/1494469659105004758.jpg&quot; title=&quot;1494469659105004758.jpg&quot; alt=&quot;10.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170511/1494469659105004758.jpg&quot;&gt;  九、往期课堂&lt;img src=&quot;http://www.gonghoo.com/image/20170511/1494466917274040599.jpg&quot; title=&quot;1494466917274040599.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170511/1494466917274040599.jpg&quot;&gt;&lt;img src=&quot;http://www.gonghoo.com/image/20170511/1494466917392010687.jpg&quot; title=&quot;1494466917392010687.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170511/1494466917392010687.jpg&quot;&gt;&lt;img src=&quot;http://www.gonghoo.com/image/20170511/1494466917405019402.jpg&quot; title=&quot;1494466917405019402.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170511/1494466917405019402.jpg&quot;&gt;', 3, 1, '2017-06-29 20:01:15', '2017-08-18 10:56:30', 0, 0);
INSERT INTO `tb_article` VALUES (9, 'i&middot;Focus创新者大赏&mdash;&mdash;智享时代 镜面未来', '2017-08-18 09:54:00', '/upload/image/20170817/1502970975908056118.jpg', 2, '指间划过屏幕，创造出了满足人们大部分需求的生活场景；人脑更迭代码，却带来了超越人类自身能力的深度思考；虚拟现实的出现让人们模糊了虚拟和现实之间的界限，基因科技的突破，让人类未来能够有可能站在疾病的对岸。', '指间划过屏幕，创造出了满足人们大部分需求的生活场景；人脑更迭代码，却带来了超越人类自身能力的深度思考；虚拟现实的出现让人们模糊了虚拟和现实之间的界限，基因科技的突破，让人类未来能够有可能站在疾病的对岸。这是一个信仰科技与创新的年代，这是一个智慧边界交叉的年代，人人都希望从不确定性中探寻确定性......2017年5月，杭州，i&middot;Focus创新者大会-[智享时代 镜面未来]，将同大家一同关注这巨变的未来，共同畅想尖端科技与创新技术的走向。活动时间2017年5月19日活动地点杭州&middot;西溪宾馆西溪厅主办单位杭州市科学技术委员会银江孵化器浙江大学管理学院协办单位微链银江资本上午场大咖嘉宾主题演讲分享&lt;img src=&quot;http://www.gonghoo.com/image/20170516/1494919018558084827.jpg&quot; title=&quot;1494919018558084827.jpg&quot; alt=&quot;微信图片_20170516151635.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170516/1494919018558084827.jpg&quot;&gt;下午场海外项目展示路演值得期待的亮点国内重磅嘉宾阵容硅谷、以色列优质创业项目汇聚银江观察，告诉你2017全创业新趋势产学研融合碰撞新思路投资机构汇集IDG资本、纪源资本、北极光创投、君联资本、联想之星、真格基金、创新工场、达晨创投、红杉资本、经纬中国、顺为资本、金沙江创投、德同资本、海泉基金峰瑞资本、如山资本、元璟资本、云启资本、火山石资本、高榕资本、华创资本　华映资本、启明创投、执一资本、银杏谷资本、戈壁创投、真顺基金、基石资本　丰厚资本、蓝湖资本、蓝驰创投、紫牛资本、头头是道、愉悦资本、赛富亚洲投资基金、复星同浩资本、梅花天使创投、威基金、赛伯乐投资、普华资本、海邦基金、真新资本、老鹰基金、华睿资本、银江资本....持续邀请(排名不分先后）合作机构支持3Wcoffice、功虎社区、氧气资本、王道公园、赢加创投、青创学院、聚募众筹、传媒梦工场、MissX女性众创平台、云咖啡、云犀直播、金柚网、Vphotos...（排名不分先后）特邀媒体曝光新华社、中新社、浙江日报、钱江晚报、科技金融时报、新浪科技、界面、腾讯科技、网易科技、凤凰科技、好奇心日报、澎湃新闻、财新、第一财经、雷锋网、虎嗅、36kr、动点科技、极客公园、浙商杂志雷科技、威锋网、爱范儿、投资界、亿邦动力、钛媒体、、新京报、南方日报、21世纪经济报道、东方早报、腾讯大浙网...持续邀请(排名不分先后）', 3, 1, '2017-08-17 19:56:18', '2017-08-18 11:02:22', 0, 0);
INSERT INTO `tb_article` VALUES (10, '聚信&middot;赋能 信核数据 2017 ICP V6认证培训计划', '2017-08-18 09:57:00', '/upload/image/20170817/1502971144341026107.jpg', 2, '信核数据作为国内首家在存储虚拟化与数据容灾保护领域拥有完全自主知识产权的厂商，为促进相互合作，共建企业生态，决定自4月起启动&ldquo;聚信 &middot; 赋能&rdquo;2017 ICP V6渠道认证计划，届时将会有技术专家、行业领袖现场分享干货，分享应用保护与备份容灾精彩案例，更有资深工程师现场讲解与实操培训环节，以此提高渠道工程师的专业知识水平及实施能力，为渠道赋能。', '背景▼草树知春不久归，百般红紫斗芳菲。企业级市场的机会属于整个产业链，共建生态是发展的必然趋势，渠道建设更是生态建设中必不可少的一环。企业级IT行业拥有无限的可能，同时也时刻面临着危机，经历了过去几年企业级市场的起起伏伏，我们更加深刻地意识到，加强渠道升级与产业链之间互相合作已迫在眉睫。 简介▼信核数据作为国内首家在存储虚拟化与数据容灾保护领域拥有完全自主知识产权的厂商，为促进相互合作，共建企业生态，决定自4月起启动&ldquo;聚信 &middot; 赋能&rdquo;2017 ICP V6渠道认证计划，届时将会有技术专家、行业领袖现场分享干货，分享应用保护与备份容灾精彩案例，更有资深工程师现场讲解与实操培训环节，以此提高渠道工程师的专业知识水平及实施能力，为渠道赋能。我们会为参加培训及通过考试的渠道工程师颁发ICP V6认证证书（InfoCore Certified Professional V6 信核数据认证V6专业工程师）。 报名方式▼课程培训认证定价5888元（含所有课程费用、资料费用及2天住宿与中餐），请于现场签到时支付（发票将于7个工作日内寄出）。现在，预付199元即可锁定席位！（预付费用将在现场签到后返还）针对资深合作伙伴，我们特别推出VIP合作伙伴票，名额有限，请在报名后马上联系对口销售获取。 ICP V6介绍▼ICP V6：InfoCore Certified Professional V6 信核数据认证V6专业工程师 ICP V6认证定位于信核产品线体系下V6版本的管理与实施。通过ICP V6认证，将证明您对存储虚拟化与数据容灾保护有全面深入的了解，全面掌握信核数据V6产品线的功能与应用，并具备独立操作实施Streamer V6产品的能力。拥有通过ICP V6认证的工程师，意味着企业有能力向用户推荐信核V6产品线，将企业所需的存储虚拟化、存储双活、数据容灾保护、存储阵列产品全面地集成到IT系统中之中，并且能满足使用需求，进而提供较高的安全性、可用性和可靠性。 咨询▼林先生18768407360yuexiang.lin@infocore.cn 课程安排▼&lt;img src=&quot;http://www.gonghoo.com/image/20170331/1490948174719019960.jpg&quot; title=&quot;1490948174719019960.jpg&quot; alt=&quot;图片2.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170331/1490948174719019960.jpg&quot;&gt;', 3, 1, '2017-08-17 19:59:07', '2017-08-18 11:02:38', 0, 0);
INSERT INTO `tb_article` VALUES (11, '2017中国企业互联网峰会（第四届）', '2017-08-18 10:00:00', '/upload/image/20170817/1502971217506077165.jpg', 2, '每个企业都渴望成为改变世界的力量，因为&ldquo;万一&rdquo;改变了，就可以象微信那样可以收获无穷无尽的红利。然并卵！那，我们所要面对的对象到底应该是什么呢？ 模式？业务？技术？产品？客户？市场？', '会议简介   每个企业都渴望成为改变世界的力量，因为&ldquo;万一&rdquo;改变了，就可以象微信那样可以收获无穷无尽的红利。然并卵！那，我们所要面对的对象到底应该是什么呢？ 模式？业务？技术？产品？客户？市场？    都不是！    是【场景】！    很多旧的业务场景被解构，更多新的业务场景被缔造。或是源于突发奇想，或是久病成医不得不破。在B端的场景革命，更是鲜活与波澜壮阔，如大海的洋流。我们发现，恰恰是场景，是超越模式，超越业务，超越技术，超越产品，超越客户，超越市场的新的工作对象。     ......     2017，我们不能等待！    2017，中国企业互联网峰会（第四届），为你而来。&lt;img src=&quot;http://www.gonghoo.com/image/20170324/1490319821375050043.jpg&quot; title=&quot;1490319821375050043.jpg&quot; alt=&quot;6362589304473575205533.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170324/1490319821375050043.jpg&quot;&gt;购票即有机会获得云南双人六日游、日本6日5晚跟团游、巴厘岛7日5晚半自助游。前1000名嘉宾，将获赠价值价值249元的办公逸微信考勤机1台。同时前300名赠送图书一套。', 3, 1, '2017-08-17 20:00:21', '2017-08-18 11:03:00', 0, 0);
INSERT INTO `tb_article` VALUES (12, '赋能.连接--轻松融入云时代 网易云技术布道系列活动', '2017-08-18 10:01:00', '/upload/image/20170817/1502971268487092121.jpg', 2, '赋能.连接--轻松融入云时代 网易云技术布道系列活动', '&lt;img src=&quot;http://www.gonghoo.com/image/20170324/1490343424947076794.jpg&quot; title=&quot;1490343424947076794.jpg&quot; alt=&quot;长图.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170324/1490343424947076794.jpg&quot;&gt;', 3, 1, '2017-08-17 20:01:44', '2017-08-18 11:04:27', 0, 0);
INSERT INTO `tb_article` VALUES (13, '2017中国SaaS产业大会', '2017-08-18 10:02:00', '/upload/image/20170817/1502971397436010028.jpg', 2, '2017中国SaaS产业大会', '&lt;img src=&quot;http://www.gonghoo.com/image/20170627/1498531184601071579.jpg&quot; title=&quot;1498531184601071579.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170627/1498531184601071579.jpg&quot;&gt;&lt;img src=&quot;http://www.gonghoo.com/image/20170627/1498531187968019921.jpg&quot; title=&quot;1498531187968019921.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170627/1498531187968019921.jpg&quot;&gt;&lt;img src=&quot;http://www.gonghoo.com/image/20170627/1498532297869069047.jpg&quot; title=&quot;1498532297869069047.jpg&quot; alt=&quot;5.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170627/1498532297869069047.jpg&quot;&gt;&lt;img src=&quot;http://www.gonghoo.com/image/20170627/1498531191840031813.jpg&quot; title=&quot;1498531191840031813.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170627/1498531191840031813.jpg&quot;&gt;&lt;img src=&quot;http://www.gonghoo.com/image/20170627/1498531195723029860.jpg&quot; title=&quot;1498531195723029860.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170627/1498531195723029860.jpg&quot;&gt;', 3, 1, '2017-08-17 20:03:47', '2017-08-18 11:04:20', 0, 0);
INSERT INTO `tb_article` VALUES (14, '短视频风口，如何解读用户、技术和内容？', '2017-08-18 10:05:00', '/upload/image/20170817/1502971490662021884.jpg', 2, '短视频风口，如何解读用户、技术和内容？', '&lt;img src=&quot;http://www.gonghoo.com/image/20170518/1495095224183008338.jpg&quot; title=&quot;1495095224183008338.jpg&quot; alt=&quot;TTGO内容页.jpg&quot; _src=&quot;http://www.gonghoo.com/image/20170518/1495095224183008338.jpg&quot;&gt;', 3, 1, '2017-08-17 20:04:53', '2017-08-18 11:04:13', 0, 0);
INSERT INTO `tb_article` VALUES (15, '喔喔贷 ', '2017-08-18 10:08:00', '/upload/image/20170817/1502971711668074744.jpg', 2, '喔喔贷于2015年1月10日上线，是上海晓晟投资管理有限公司旗下独立品牌，市国内覆盖范围较广，资金保障更安全、诚信可靠的金融网络服务平台，基于专业的风控团队，地域化的项目管理经验，便携自助的操作形式、差异化的定价机制相结合的特色运营模式，构建出一个开放、便携的网络投融资平台。', '					喔喔贷于2015年1月10日上线，是上海晓晟投资管理有限公司旗下独立品牌，市国内覆盖范围较广，资金保障更安全、诚信可靠的金融网络服务平台，基于专业的风控团队，地域化的项目管理经验，便携自助的操作形式、差异化的定价机制相结合的特色运营模式，构建出一个开放、便携的网络投融资平台。&lt;img class=&quot;lazy&quot; src=&quot;http://www.erongdu.com/uploads/allimg/161031/1-1610311K514.png&quot; _src=&quot;http://www.erongdu.com/uploads/allimg/161031/1-1610311K514.png&quot;&gt;&lt;img class=&quot;lazy&quot; src=&quot;http://www.erongdu.com/uploads/allimg/161031/1-1610311K517.png&quot; _src=&quot;http://www.erongdu.com/uploads/allimg/161031/1-1610311K517.png&quot;&gt;&lt;a href=&quot;/new/case/guarantee/2016/1027/2470.html&quot; class=&quot;prev-news&quot;&gt;&lt;a href=&quot;/new/case/index.html&quot; class=&quot;show-list&quot;&gt;&lt;a href=&quot;/new/case/guarantee/2016/1207/2513.html&quot; class=&quot;next-news&quot;&gt;', 4, 1, '2017-08-17 20:14:40', '2017-08-18 11:04:05', 0, 0);
INSERT INTO `tb_article` VALUES (16, '粤财控股 ', '2017-08-18 10:16:00', '/upload/image/20170817/1502972213989093725.jpg', 2, ' 粤财汇是粤财控股携手易方达、恒生电子、赛富资管、壹乾坤基金倾力打造的互联网金融平台，粤财汇将紧跟金融全球化发展趋势，采用互联网新技术、新方式，以创新的金融服务方式、健全的风险管控体系，为企业、机构及个人提供专业、高效、安全的综合性金融服务，致力于通过优质的金融服务及创新的金融产品设计，提高金融交易效率，优化金融资源配置，打造国内领先的金融资产供应商与金融交易平台。', '	      粤财汇是粤财控股携手易方达、恒生电子、赛富资管、壹乾坤基金倾力打造的互联网金融平台，粤财汇将紧跟金融全球化发展趋势，采用互联网新技术、新方式，以创新的金融服务方式、健全的风险管控体系，为企业、机构及个人提供专业、高效、安全的综合性金融服务，致力于通过优质的金融服务及创新的金融产品设计，提高金融交易效率，优化金融资源配置，打造国内领先的金融资产供应商与金融交易平台。	 &lt;img class=&quot;lazy&quot; src=&quot;http://www.erongdu.com/uploads/allimg/160113/4-160113162402.jpg&quot; _src=&quot;http://www.erongdu.com/uploads/allimg/160113/4-160113162402.jpg&quot;&gt;&lt;img class=&quot;lazy&quot; src=&quot;http://www.erongdu.com/uploads/allimg/160113/4-160113162408.jpg&quot; _src=&quot;http://www.erongdu.com/uploads/allimg/160113/4-160113162408.jpg&quot;&gt;&lt;a href=&quot;/new/case/soe/2015/0616/1223.html&quot; class=&quot;prev-news&quot;&gt;&lt;a href=&quot;/new/case/index.html&quot; class=&quot;show-list&quot;&gt;&lt;a href=&quot;/new/case/soe/2016/1027/2464.html&quot; class=&quot;next-news&quot;&gt;', 4, 1, '2017-08-17 20:16:55', '2017-08-18 11:03:58', 0, 0);
INSERT INTO `tb_article` VALUES (17, '报喜鸟', '2017-08-18 10:17:00', '/upload/image/20170817/1502972257497036685.jpg', 2, '温州贷&mdash;&mdash;民间借贷信息平台，总部位于温州，由温州网诚电子商务有限公司发起成立，并对温州贷&mdash;&mdash;民间借贷信息平台产生的经营风险承担无限责任。温州贷积极响应温州市金融综合改革实验区的政策，以实现民间借贷公开化、阳光化而努力，以创新的思维，诚信的态度为中小微企业、三农与中小投资者打造一个公开透明的民间借贷信息平台。', '					温州贷&mdash;&mdash;民间借贷信息平台，总部位于温州，由温州网诚电子商务有限公司发起成立，并对温州贷&mdash;&mdash;民间借贷信息平台产生的经营风险承担无限责任。温州贷积极响应温州市金融综合改革实验区的政策，以实现民间借贷公开化、阳光化而努力，以创新的思维，诚信的态度为中小微企业、三农与中小投资者打造一个公开透明的民间借贷信息平台。&lt;img class=&quot;lazy&quot; src=&quot;http://www.erongdu.com/uploads/allimg/141205/1-141205225246.png&quot; _src=&quot;http://www.erongdu.com/uploads/allimg/141205/1-141205225246.png&quot;&gt;&lt;img class=&quot;lazy&quot; src=&quot;http://www.erongdu.com/uploads/allimg/141205/1-141205225249.png&quot; _src=&quot;http://www.erongdu.com/uploads/allimg/141205/1-141205225249.png&quot;&gt;&lt;a href=&quot;/new/case/listedcompanies/2014/1205/627.html&quot; class=&quot;prev-news&quot;&gt;&lt;a href=&quot;/new/case/index.html&quot; class=&quot;show-list&quot;&gt;&lt;a href=&quot;/new/case/listedcompanies/2014/1205/627.html&quot; class=&quot;next-news&quot;&gt;', 4, 1, '2017-08-17 20:18:19', '2017-08-18 11:03:15', 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for tb_invitation
-- ----------------------------
DROP TABLE IF EXISTS `tb_invitation`;
CREATE TABLE `tb_invitation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sponsor` varchar(64) DEFAULT NULL COMMENT '主办方',
  `linkman` varchar(16) DEFAULT NULL COMMENT '联系人',
  `phone` varchar(32) DEFAULT NULL COMMENT '手机号',
  `desc` varchar(255) DEFAULT NULL COMMENT '需求描述',
  `budget` decimal(10,0) DEFAULT NULL COMMENT '预算',
  `tutor_id` bigint(20) DEFAULT NULL COMMENT '导师id',
  `handle_status` tinyint(2) DEFAULT NULL COMMENT '0-未处理，1-已处理',
  `handle_remarks` varchar(128) DEFAULT NULL COMMENT '处理备注',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL ,
  `delete_flag` tinyint(2) DEFAULT '0' COMMENT '0-正常，1-删除',
  `status` tinyint(2) DEFAULT '0' COMMENT '0-正常，1-停用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COMMENT='邀请函';

-- ----------------------------
-- Records of tb_invitation
-- ----------------------------
BEGIN;
INSERT INTO `tb_invitation` VALUES (33, '123', '1', '15767564532', '', 32, 3, NULL, NULL, '2017-07-06 15:39:19', '2017-07-09 20:53:05', 1, 0);
INSERT INTO `tb_invitation` VALUES (34, '1', '1', '15767564532', '', 23, 3, NULL, NULL, '2017-07-06 15:39:55', '2017-07-06 15:39:55', 0, 0);
INSERT INTO `tb_invitation` VALUES (35, '1', '1', '15767564532', '', 23, 3, NULL, NULL, '2017-07-06 15:48:18', '2017-07-06 15:48:18', 0, 0);
INSERT INTO `tb_invitation` VALUES (36, '1', '1', '15767564532', '', 32, 3, NULL, NULL, '2017-07-06 15:48:54', '2017-07-06 15:48:54', 0, 0);
INSERT INTO `tb_invitation` VALUES (37, '1', '1', '15767564532', '', 23, 3, NULL, NULL, '2017-07-06 15:49:11', '2017-07-06 15:49:11', 0, 0);
INSERT INTO `tb_invitation` VALUES (38, '1', '1', '15767564532', '', 23, 3, NULL, NULL, '2017-07-06 15:52:35', '2017-07-06 15:52:35', 0, 0);
INSERT INTO `tb_invitation` VALUES (39, '1', '1', '15767564532', '', 1, 3, NULL, NULL, '2017-07-06 15:53:23', '2017-07-06 15:53:23', 0, 0);
INSERT INTO `tb_invitation` VALUES (40, '123', '123', '13427883530', '', 1000, 11, NULL, NULL, '2017-07-07 15:16:03', '2017-07-07 15:16:03', 0, 0);
INSERT INTO `tb_invitation` VALUES (41, '123', '123', '13427883530', '', 1000, 11, NULL, NULL, '2017-07-07 15:16:19', '2017-07-07 15:16:19', 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for tb_open_class
-- ----------------------------
DROP TABLE IF EXISTS `tb_open_class`;
CREATE TABLE `tb_open_class` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `course_name` varchar(64) DEFAULT NULL COMMENT '课程名',
  `img_path` varchar(255) DEFAULT NULL COMMENT '宣传图片路径',
  `sponsor` varchar(64) DEFAULT NULL COMMENT '主办方',
  `lecturer` varchar(16) DEFAULT NULL COMMENT '讲师',
  `place` varchar(255) DEFAULT NULL COMMENT '地点',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL ,
  `delete_flag` tinyint(2) DEFAULT '0' COMMENT '0-正常，1-删除',
  `status` tinyint(2) DEFAULT '0' COMMENT '0-正常，1-停用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_open_class
-- ----------------------------
BEGIN;
INSERT INTO `tb_open_class` VALUES (1, '一个IT人的自我修养', '/upload/image/20170703/1499077213934019459.png', '行者', '尹登峰', '聚牛咖啡', '2017-07-04 22:19:00', '2017-07-04 22:19:00', '2017-07-03 18:21:42', '2017-07-03 18:42:48', 0, 0);
INSERT INTO `tb_open_class` VALUES (2, '一个IT人的自我修养', '/upload/image/20170703/1499077213934019459.png', '行者', '尹登峰', '聚牛咖啡', '2017-07-04 22:19:00', '2017-07-04 22:19:00', '2017-07-03 18:21:42', '2017-07-03 18:42:48', 0, 0);
INSERT INTO `tb_open_class` VALUES (3, '一个IT人的自我修养', '/upload/image/20170703/1499077213934019459.png', '行者', '尹登峰', '聚牛咖啡', '2017-07-04 22:19:00', '2017-07-04 22:19:00', '2017-07-03 18:21:42', '2017-07-03 18:42:48', 0, 0);
INSERT INTO `tb_open_class` VALUES (4, '一个IT人的自我修养', '/upload/image/20170703/1499077213934019459.png', '行者', '尹登峰', '聚牛咖啡', '2017-07-04 22:19:00', '2017-07-04 22:19:00', '2017-07-03 18:21:42', '2017-07-03 18:42:48', 0, 0);
INSERT INTO `tb_open_class` VALUES (5, '一个IT人的自我修养', '/upload/image/20170703/1499077213934019459.png', '行者', '尹登峰', '聚牛咖啡', '2017-07-04 22:19:00', '2017-07-04 22:19:00', '2017-07-03 18:21:42', '2017-07-06 03:01:13', 1, 0);
INSERT INTO `tb_open_class` VALUES (6, '一个IT人的自我修养', '/upload/image/20170703/1499077213934019459.png', '行者', '尹登峰', '聚牛咖啡', '2017-07-05 12:19:00', '2017-07-05 12:19:00', '2017-07-03 18:21:42', '2017-07-06 02:06:06', 0, 1);
INSERT INTO `tb_open_class` VALUES (7, '一个IT人的自我修养', '/upload/image/20170703/1499077213934019459.png', '行者', '尹登峰', '聚牛咖啡', '2017-07-04 22:19:00', '2017-07-04 22:19:00', '2017-07-03 18:21:42', '2017-07-06 02:58:42', 1, 0);
COMMIT;

-- ----------------------------
-- Table structure for tb_slideshow
-- ----------------------------
DROP TABLE IF EXISTS `tb_slideshow`;
CREATE TABLE `tb_slideshow` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `is_article` tinyint(2) DEFAULT NULL COMMENT '是否文章，0-是，1-否',
  `article_id` bigint(20) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL COMMENT '轮播图名称',
  `img_path` varchar(255) DEFAULT NULL COMMENT '图片路径',
  `img_link` varchar(255) DEFAULT NULL COMMENT '图片指向的链接',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL ,
  `delete_flag` tinyint(2) DEFAULT '0' COMMENT '0-正常，1-删除',
  `status` tinyint(2) DEFAULT '0' COMMENT '0-正常，1-停用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='轮播图';

-- ----------------------------
-- Records of tb_slideshow
-- ----------------------------
BEGIN;
INSERT INTO `tb_slideshow` VALUES (1, 1, NULL, '颠倒是非', '/upload/image/20170814/1502678187843028197.jpg', 'https://www.baidu.com', '2017-06-30 01:47:03', '2017-08-14 10:36:28', 0, 0);
INSERT INTO `tb_slideshow` VALUES (2, 0, 6, 'dd', '', '/tbArticle/editPage?id=6', '2017-06-30 03:49:58', '2017-07-09 22:41:37', 1, 0);
INSERT INTO `tb_slideshow` VALUES (3, 1, NULL, '网易云创', '/upload/image/20170703/1499067881606026873.jpg', '#', '2017-07-03 15:44:20', '2017-07-09 22:41:46', 1, 0);
INSERT INTO `tb_slideshow` VALUES (4, 0, 4, '网易云创大会', '/upload/image/20170630/1498833361092032071.jpg', '/front/activitydetatils.html?id=4', '2017-07-09 22:37:20', '2017-07-12 14:29:21', 1, 0);
INSERT INTO `tb_slideshow` VALUES (5, 1, NULL, '', '/upload/image/20170814/1502678306122046471.jpg', '', '2017-07-12 18:00:44', '2017-08-14 10:38:29', 0, 0);
INSERT INTO `tb_slideshow` VALUES (6, 1, NULL, 'banner03', '/upload/image/20170814/1502678251101064988.jpg', '', '2017-07-12 18:01:30', '2017-08-14 10:37:34', 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for tb_tutor
-- ----------------------------
DROP TABLE IF EXISTS `tb_tutor`;
CREATE TABLE `tb_tutor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(16) DEFAULT NULL COMMENT '姓名',
  `name_pinyin` varchar(64) DEFAULT NULL COMMENT '姓名全拼（包含英文字母不处理）',
  `name_initial` varchar(1) DEFAULT NULL COMMENT '姓名首字母',
  `head_img` varchar(255) DEFAULT NULL COMMENT '头像路径',
  `is_home_display` tinyint(2) DEFAULT '1' COMMENT '是否首页展示，0-是，1-否',
  `head_img_extra` varchar(255) DEFAULT NULL COMMENT '头像路径（额外的首页展示）',
  `company` varchar(128) DEFAULT NULL COMMENT '公司',
  `position` varchar(64) DEFAULT NULL COMMENT '职位',
  `permanent_land` varchar(64) DEFAULT NULL COMMENT '常驻地',
  `short_intro` varchar(64) DEFAULT NULL COMMENT '简短简介',
  `intro` text COMMENT '简介',
  `is_master` tinyint(2) DEFAULT NULL COMMENT '是否大咖，0-是，1-否',
  `domain_type` varchar(16) DEFAULT NULL COMMENT '领域类型id,如：,1,2,',
  `sort_value` int(11) DEFAULT NULL COMMENT '排序值，值越大排越前',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL ,
  `delete_flag` tinyint(2) DEFAULT '0' COMMENT '0-正常，1-删除',
  `status` tinyint(2) DEFAULT '0' COMMENT '0-正常，1-停用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=304 DEFAULT CHARSET=utf8 COMMENT='导师';

-- ----------------------------
-- Records of tb_tutor
-- ----------------------------
BEGIN;
INSERT INTO `tb_tutor` VALUES (1, '吴吉义', 'wujiyi', 'w', '/upload/image/20170713/1499928061190087978.png', 0, '/upload/image/20170705/1499234428771051465.jpg', '功虎创学院', '院长', '北京', '功虎创学院院长', '博士，研究员，中国计算机学会会员工委副主任、CCTV证券资讯频道《创智》编导、第十届浙江省科协常委、CCFYOCSEF杭州主席、杭州市移动互联网技术学会秘书长。08年作为总策划人和主要创始人参与了阿里巴巴商学院的筹建。先后在浙江大学获得硕士和博士学位，多次受国家公派赴美国、日本等国进行技术交流，擅长领域包括SaaS技术架构、商务大数据。公开出版《电子商务服务》等书籍10余部，获各类专利和著作权11项。担任&ldquo;中国首席技术官大会&rdquo;、&ldquo;中国移动互联网大会&rdquo;、&ldquo;中国IT武林大会&rdquo;等千人行业盛会组织主席。', 0, '1', 1, '2017-07-07 20:36:22', '2017-07-13 14:41:03', 0, 0);
INSERT INTO `tb_tutor` VALUES (2, '杜跃进', 'duyuejin', 'd', '/upload/image/20170713/1499927846760001760.png', 0, '/upload/image/20170630/1498826357777091749.jpg', '阿里巴巴', '技术副总裁', '杭州', '阿里巴巴技术副总裁', '阿里巴巴集团安全部副总裁、前国家网络信息安全技术研究所所长、曾任北京奥运、上海世博等安保工作组专家。拥有十余年互联网安全经验，完成多项国家级科研项目，获得国务院特殊津贴。', 0, '5', 1, '2017-06-30 00:52:38', '2017-07-13 14:50:08', 0, 0);
INSERT INTO `tb_tutor` VALUES (3, '何刚', 'hegang', 'h', '/upload/image/20170713/1499927905792048852.png', 0, '/upload/image/20170630/1498826376438074453.jpg', '京东', '技术副总裁兼首席科学家', '北京', '京东技术副总裁兼首席科学家', '原为亚马逊负责云计算项目技术负责人，2011年盛大云从盛大创新院脱胎而成，肩负着盛大云计算全产品链服务重任。何刚和原盛大在线首席安全官季昕华以联席CEO的身份，共同主政。2012年4月加盟京东商城，2013年领导京东商城云计算平台隆重推出京东电商云四大解决方案，构建了完整开放生态链。', 1, '5', 1, '2017-06-30 01:25:37', '2017-07-13 14:38:28', 0, 0);
INSERT INTO `tb_tutor` VALUES (4, '芮勇', 'ruiyong', 'r', '/upload/image/20170713/1499927947928000801.png', 0, '/upload/image/20170630/1498826438724064023.jpg', '联想集团', '高级副总裁', '北京', '联想集团高级副总裁', '曾任微软总部研究院主任研究员，全面负责研究院多媒体协同系统中的科研。 在美国微软总部工作7年后，芮勇于2006年初回到中国。先后担任微软亚太研发集团首任战略总监 (2006-2008), 负责制定和执行集团的整体研发战略; 研发总监 (2008-2010)，全面负责公司第一款教育产品在华的研发； 以及微软亚洲工程院副院长及微软亚太研发集团中国创新孵化部高级总监总经理 (2010-2012)，负责集团在华产品技术的创新及孵化。', 1, '4', 1, '2017-06-30 01:33:59', '2017-07-13 14:39:10', 0, 0);
INSERT INTO `tb_tutor` VALUES (5, '李未', 'liwei', 'l', '/upload/image/20170707/b933f65e-55f0-4afc-a448-247a220b3750', 0, '/upload/image/20170630/1498826504005010988.jpg', '中国科学院', '院士', '北京', '中国科学院院士', '曾任北京航空航天大学校长，第十、十一届全国政协委员，第二至六届国务院学位委员会委员，国务院参事室特约研究员，教育部计算机教学指导委员会主任，英国科学与工程委员会高级访问研究员，不来梅大学教授级研究员，萨尔布吕肯大学“祖思”（Zuse）讲座教授。李未院士在并发程序设计理论、数理逻辑和海量信息处理、群体软件工程等方面取得国际原创性研究成果，出版中英文专著6部，曾获国家自然科学、国家科技进步奖、国家教学成果奖、何梁何利科技进步奖以及俄罗斯“齐奥尔科夫斯基”勋章等多项国际、国内学术界的至高荣誉，是我国计算机软件领域的权威专家和知名学者。', 1, '1', 1, '2017-07-07 20:36:21', '2017-07-10 17:00:35', 0, 0);
INSERT INTO `tb_tutor` VALUES (6, '冯科', 'fengke', 'f', '/upload/image/20170702/1498971275004073466.jpg', 1, NULL, '融都科技', '董事长兼CEO', '杭州', '融都科技董事长兼CEO', '融都科技董事长兼CEO，曾任塔塔集团、查信科技资深软件工程师，浙江省国际金融学会企业主席团常委、&ldquo;2015浙江青年金融才俊奖&rdquo;获得者、融都科技互联网金融智库创始人、中国互联网金融早起实践者，10年以上互联网技术研发及管理经验。', 0, '2', 1, '2017-07-02 12:56:41', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (7, '郑小林', 'zhengxiaolin', 'z', '/upload/image/20170707/486dacf4-d5f6-4610-83e5-a67265a6fa7e', 1, NULL, '浙江大学', '副教授、博导', '杭州市', '浙江大学副教授、博导', '博士，2012-2014斯坦福大学访问学者。现为浙江大学计算机学院副教授、博士生导师，浙江大学人工智能研究所副所长，浙江大学现代服务创新实验室主任。IEEE资深会员，中国计算机学会高级会员，中国计算机学会服务计算专委会委员，2015-2016CCFYOCSEF杭州主席。2011年11月入选浙江省151人才工程第三层次培养人员。近几年承担了10多项国家、省部级电子商务和现代服务业领域相关课题，以及多项阿里巴巴、百度、IBM等企业合作研发项目。作为主要骨干参加了2014杭州市智慧经济规划（一号工程），作为负责人承担了贵州、新疆等市县的电子商务规划。获得国家发明专利授权4项，发表SCI/EI检索的高水平期刊会议论文50多篇。作为合伙人创办智慧养老云服务平台和企业，获得5050海外高层次人才创业资助，担任该公司首席科学家。作为主要骨干获得2014浙江省科技进步一等奖，2010年度高等学校科技进步奖二等奖、2009&ldquo;中国商业联合会服务业科技创新奖特等奖&rdquo;。荣获2009年度', 0, '1,2,5', 1, '2017-07-07 20:34:07', '2017-07-10 17:17:25', 0, 0);
INSERT INTO `tb_tutor` VALUES (8, '王峰', 'wangfeng', 'w', '/upload/image/20170702/1498972746891048811.jpg', 1, NULL, '爱学贷', '联合创始人', '杭州', '爱学贷联合创始人', '爱学贷联合创始人、互联网金融领域王牌操盘手、杭州金融圈内以快闻名的工程师，26岁成为金融软件寡头恒生电子的首任电子银行事业部总经理，恒生电子史上最年轻的事业部总监，也是恒生电子银行部的创立者，后加入隐形支付大鳄连连集团，首创新一代话费充值运营平台&ldquo;空中充值&rdquo;，后任职连连运通副总裁，主导跨境支付和移动支付业务的运营，之后又成功创办网络安全细分市场里的领导企业-盈高科技。', 0, '2', 1, '2017-07-02 13:19:46', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (9, '胡德华', 'hudehua', 'h', '/upload/image/20170707/d16f8e39-2cdf-4508-9261-ff95a03a3789', 1, NULL, '投融家', '联合创始人兼CEO', '杭州市', '投融家联合创始人兼CEO', '现任投融家CEO，联合创始人；曾是国内知名P2B平台鑫合汇创始人及董事长兼CEO。1976年1月出生于宁夏，国内第一批软件工程硕士（浙江大学）。先后就职于军队系统、互联网社交平台、恒生电子、中新力合等知名企业。中国云计算专家委员会成员，著有《SOA之道》，拥有专利一项。公司介绍：投融家是港股上市集团投融长富旗下互联网金融业务平台，股票代码00850.HK，投融长富是一家业内领先的经营领域跨越互联网金融、基金管理、财富管理、投行业、保险销售的大型金融服务企业，致力于为客户提供一流的资产增值、投资策略、综合理财服务。创新金融模式业内创新建立金融资产抵质押借款互联网金融运营模式，依托于一级优质金融资产，为高净值、高信用的合格投资者持有金融资产份额提供抵质押或转让及回购模式的借贷服务，满足其资产变现、流动性需求或消费性融资需求。产品优势：低风险：以一级优质金融资产为依托的金融资产抵质押借款交易结构由互联网金融法律专家、中国政法大学互联网金融法律研究院院长李爱君教授设计，服务于资信良好、偿还能力强、持有金融资产认购份额的合格投资者，并设定安全抵质押率，保证投资产品的低风险及相对高收益。高收益：平均预期年化收益率10%，最高可达15%周期活：最短15天，3个月、6个月、12个月、18个月投资周期可选；平台提供基于债权转让模式的二级交易市场，为用户提供流动性支持。核心产品快乐稳赢起投金额：1000元投资期限：15天&mdash;18个月预期年化收益：8%&mdash;15%开心超赚起投金额：1000元投资期限：15天&mdash;18个月预期年化收益：9%&mdash;15%', 0, '1,2', 1, '2017-07-07 20:34:09', '2017-07-13 14:50:48', 0, 0);
INSERT INTO `tb_tutor` VALUES (10, '李华', 'lihua', 'l', '/upload/image/20170702/1498972909231060126.jpg', 1, NULL, '不知', '不知', '杭州', '曾开发全国个人所得税代扣代缴软件 从事IT行业19年', '从事IT软件行业19年，开发的全国个人所得税代扣代缴软件在全国23个省级单位使用，负责集团O2O转型，内部大数据、信息化管理体系建设，业务战略管理，品牌管理和并购投资，税友集团为税务软件行业龙头企业之一，系统集成一级资质，cmmi4级资质，国家金税三期核心供应商。', 0, '1', 1, '2017-07-02 13:22:03', '2017-07-11 15:17:35', 1, 0);
INSERT INTO `tb_tutor` VALUES (11, '尹登峰', 'yindengfeng', 'y', '/upload/image/20170702/1498972979739009605.jpg', 1, NULL, '浙江工信保网络科技股份有限公司', '消费金融产品总监', '杭州', '浙江工信保网络科技股份有限公司 消费金融产品总监', '浙江工信保网络科技股份有限公司消费金融产品总监、浙江大学生物医学工程学与金融学双硕士，15年以上IT经验，10年以上产品经理从业经验，曾就职于海尔集团旗下支付公司快捷通，以及融都科技、车果网络等互联网金融企业，具备丰富的IT软件研发流程管理经验，精通软件产品与互联网产品规划设计，资深互联网金融业务规划专家。', 0, '2', 1, '2017-07-02 13:23:34', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (12, '黄筱赟', 'huangxiaoyun', 'h', '/upload/image/20170702/1498973066645047259.jpg', 1, NULL, '前思科系统（中国）研发有限公司', '金融IT产品规划和风险管理专家', '杭州', '金融IT产品规划和风险管理专家', '曾任恒生电子高级产品经理、恒生网络合规部经理，10年金融IT从业经验，曾参与证券行业多项标准制定，金融IT产品规划和风险管理专家。', 0, '2', 1, '2017-07-02 13:24:50', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (13, '吴忠旺', 'wuzhongwang', 'w', '/upload/image/20170702/1498973121621058464.jpg', 1, NULL, '融都科技', '技术中心副总裁', '杭州', '融都科技技术中心副总裁', '先后任职于恒生电子、国都时代、信雅达等国内知名企业，专注于产品设计和技术管理，在银行、证券、支付、互联网金融等信息化领域有超过15年的工作经验，专业特长：业务支撑、金融风控、支付结算、资产证券化、互联网金融、技术开发与管理。', 0, '2', 1, '2017-07-02 13:25:50', '2017-07-14 11:51:53', 0, 0);
INSERT INTO `tb_tutor` VALUES (14, '王建军', 'wangjianjun', 'w', '/upload/image/20170702/1498973190768040479.jpg', 1, NULL, '融都科技', '信息中心总监', '杭州', '融都科技信息中心总监', '8年Linux系统运维经验，专注于系统软硬件架构及业务系统安全管理，具有丰富的系统运维及突发性事件处理经验，对大型互联网金融软硬件系统架构有较深的认识，成功处理多起突发性黑客攻击安全事件。', 0, '2', 1, '2017-07-02 13:26:55', '2017-07-14 11:53:28', 0, 0);
INSERT INTO `tb_tutor` VALUES (16, '胡佳骏', 'hujiajun', 'h', '', 1, '', '前思科系统（中国）研发有限公司', '资深项目经理', '杭州', '', '前思科系统（中国）研发有限公司资深项目经理，PMP项目管理专业人士资格认证（Project  Management  Professional），CMS认证的敏捷项目管理专家（Certified Scrum Master）,10年以上IT、互联网技术研发、项目管理经验，小贷行业规划、设计、统筹专家，具有突出的沟通协调能力、团队协作能力和执行力。', 0, '1', 1, '2017-07-07 14:38:21', '2017-07-11 15:17:44', 1, 0);
INSERT INTO `tb_tutor` VALUES (186, '李东杭', 'lidonghang', 'l', '/upload/image/20170707/e0eb3585-de7b-4b5a-8795-49f5716c0e8e', 1, NULL, '米订网', 'CEO', '杭州市', '米订网CEO', '米订网、米订商学院创始人，现任米订网CEO。资深酒店管理专家，先之国际酒店管理学院客座教授。近30年国际及国内知名酒店管理经验。2009年荣获&ldquo;建国60周年&bull;中国酒店精英金奖&rdquo;殊荣。并于2015年1月在中国规格最高，最具影响力的酒店&ldquo;CHIA-五星钻石奖&rdquo;颁奖典礼上，荣获了&ldquo;中国酒店业最具影响力领袖人物&rdquo;称号。2012年起致力于研究中国酒店在互联网大数据时代下营销问题的研究，是酒店移动互联网领域的领航人。近年来，多次受邀参加各个高端峰会论坛，如澳门国际旅游博览会、中国饭店业信息化大会、亚洲酒店论坛年会暨星光奖颁奖典礼、迈点新技术研讨会、中国酒店业新发展大会、上海酒店发展大会、中国智慧酒店论坛等，通过精彩演讲向酒店揭示移动互联网营销的真谛，致力研究酒店智慧生态圈的打造。2015年，李东杭先生创立了米订商学院，以培养酒店专业化互联网运营人才为核心目标，填补了酒店移动互联网领域运营人才缺乏的空白。为酒店移动营销迎来了一个新时代。', 1, '1,5', 1, '2017-07-07 20:34:06', '2017-07-10 16:22:18', 0, 0);
INSERT INTO `tb_tutor` VALUES (188, '金家芳', 'jinjiafang', 'j', '/upload/image/20170707/1f41f816-8659-47a1-8cb6-1e627990a3ed', 1, NULL, '维信金融集团', 'CTO', '杭州市', '维信金融集团CTO', '金融硕士，维信金融集团CTO，1998年毕业于南昌大学计算机系，后获清华大学经济学、欧洲商学院金融学硕士。注册金融分析师、金融风险管理师，美国项目管理协会会员，上海软件行业协会开源软件专业委员会委员，上海软件培训中心培训讲师。曾为中石化、菲利浦、复星集团、麦考林、欧莱雅、远东租赁等传统及互联网企业提供咨询服务，曾任香港红筹上市公司远东宏信CIO，2013年起任维信金融集团CTO，负责公司科技金融、大数据风控及互联网金融产品创新、产品设计及运营推广。有丰富的FinTech、供应链金融、小微信贷、消费金融从业和实践经验，现致力于互联网金融、大数据应用领域的商业模式、产品设计、风控、运营落地。', 1, '1,2', 1, '2017-07-07 20:34:08', '2017-07-10 17:12:29', 0, 0);
INSERT INTO `tb_tutor` VALUES (189, '李海江', 'lihaijiang', 'l', '/upload/image/20170707/69f48f13-fc31-4dab-b617-804186b97b4e', 1, NULL, '杭州龙骞科技有限公司／流＋', '副总经理', '杭州市', '杭州龙骞科技有限公司／流＋副总经理', '李海江，男，1978年生，2003年毕业于浙江工业大学，获硕士学位，现在读浙江大学工商管理MBA。硕士毕业后进入浙江鸿程计算机系统有限公司工作，2005年5月联合创办杭州龙骞科技有限公司担任技术副总经理。 目前是中国电子商务协会移动商务专家委员会委员，浙江省计算机应用与教育学会理事，杭州市移动互联网技术学会理事，杭州市经信委工业与信息化专家。获国家注册信息系统高级项目管理师，高级工程师职称。 近五年来，在信息通讯、融合通信、数字文化阅读等领域研发多款互联网产品，面向百万级政企客户提供信息化产品服务，在企业服务领域积累了丰富的实战经验。', 1, '1,6', 1, '2017-07-07 20:34:08', '2017-07-10 17:01:01', 0, 0);
INSERT INTO `tb_tutor` VALUES (191, '孔旋', 'kongxuan', 'k', '/upload/image/20170707/310a81d8-5732-45ed-9f08-6b7e00788e4c', 1, NULL, '浙江碳盈金融信息服务有限公司', 'CEO', '北京', '浙江碳盈金融信息服务有限公司CEO', '教育背景：中国海洋大学英国杜伦大学职业经历：公务员制造业董事、董事会秘书上市公司董事长助理互联网金融公公司CEO互联网金融上市公司事业部老总创业中。。。。。。丰富的职场经历：横跨体制内、民营企业、上市公司、大型互联网公司上市公司、创业团队纵横开阔的人生轨迹：公务员考试笔试第一、面试第一辞职、出国留学制造业董事、董事会秘书，年少便身居高位人生顶峰后遭遇重大挫折曾有整整一年找不到工作的经历裸辞、毅然进入互联网金融领域，跳海学习仅3个月时间，冲刺为行业第二，领悟互联网哲学精要担任华夏基金、瑞士信贷、威灵顿资产管理、达晨创投、贝恩资本、国金证券等机构的外部顾问，提供咨询服务', 1, '1,2', 1, '2017-07-07 20:34:09', '2017-07-10 16:55:37', 0, 0);
INSERT INTO `tb_tutor` VALUES (192, '程波', 'chengbo', 'c', '/upload/image/20170707/a9c4eb62-c47e-43a8-bae0-8e45f08d5bfe', 1, NULL, '建设银行', '高级产品经理', '杭州市', '建设银行高级产品经理', '十几年金融系统软件开发、项目管理经验，精通银行互联网金融产品全过程的创新设计、生产营销、管理服务和应用实施。与300余家互联网公司进行过交流，深谙各类互联网企业运作模式，对互联网金融、供应链金融、传统行业互联网转型具有独特的理解。', 1, '1,2', 1, '2017-07-07 20:34:10', '2017-07-10 17:10:04', 0, 0);
INSERT INTO `tb_tutor` VALUES (193, '梁俊景', 'liangjunjing', 'l', '/upload/image/20170707/208eb482-ae36-4500-8d0d-df3665739c79', 1, NULL, '浙江麦迪律师事务所', '联合创始人/常务主任', '杭州市', '浙江麦迪律师事务所联合创始人/常务主任', '梁俊景，2005年毕业于国内著名法学院，毕业后一直在律师事务所工作，2012年与数名执业近20年的国内著名律师一起共同创办浙江麦迪律师事务所，现任该所常务主任。取名麦迪，意在“MyIdea”，用团队独到的观点和思想，力争为客户寻求法律问题的最佳解决之道。带领律所团队在证券资本市场、公司投融资等领域以团队整体力量及时、准确向客户提供了高质量、全方位的专业法律服务。为杭州艾及帝文化创意有限公司A轮融资、杭州威佩网络科技有限公司A、B、C轮融资（目前已超1.5亿元人民币）等国内多家公司企业提供专项全程法律服务，近期接受国内专业的融资服务交易平台“投融界”的专访，投融界前身是浙江省浙商投资研究会(原浙江省经济建设开发促进会)，始建于1995年，拥有长达20年的投融资服务经验，成功帮助企业融资超1000亿元，麦迪律所是国内第一家接受专访的律所，随后在微课堂（第一讲）为498名中小企业家授课。梁俊景带领律所团队在企业IPO、创业板、新三板融资上市领域异军突起、发展迅速，为舟山奥兰仕有限公司、上海东海电器有限公司、江苏泗洲城农业发展有限公司、浙江永利环保有限公司等多家公司正在全程提供新三板挂牌上市专项法律服务。律所团队在民商事代理、刑事辩护领域曾经办理轰动全国的“中国工商银行储户资金被盗案（涉案2600万）、香港周大福珠宝被告侵权案（一审二审均胜诉）、为省高院童某（省法院系统涉案最高级别官员）辩护案等一大批有一定社会影响的案件。经过近几年的快速发展，律所已初具规模，并获得业界与客户的一致好评，是“香港范家碧律师行”在大陆合作的首批律所，与美国、英国、加拿大、澳大利亚等国外律师机构建立了比较紧密的长期合作关系，籍此以保障麦迪律所的客户在世界范围内迅速获得高效优质的法律服务。梁俊景带领团队曾先后获得“浙江省消费者最信赖单位”、“杭州市最佳服务机构”、“西湖区最具影响力律师”等荣誉称号，创建了“中国计量学院法学院实践基地”、“杭州师范大学法学院实践基地”、“杭州师范大学科技园共建法律服务中心”、“国众司法考试培训学校（原:浙江大学司法考试培训学校）优秀学员实践基地”，梁俊景是“杭州西湖风景名胜区、市园文局、市运保卫文明督导员、行风监督员”、是众多商会、协会的常年法律顾问、总裁班私董会创始人等，近期接受投融界、杭州日报、人民日报、人民网专访、CCTV采访等。', 1, '1', 1, '2017-07-07 20:34:10', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (194, '洪波', 'hongbo', 'h', '/upload/image/20170707/415ea673-fbe0-4afc-b13d-e7de0189ad0b', 1, NULL, '杭州鸿泰股权投资基金管理有限公司', '投资总监', '杭州市', '杭州鸿泰股权投资基金管理有限公司投资总监', '洪波毕业于杭州电子科技大学，杭州鸿泰股权投资基金管理有限公司资深投资总监、研究员、投资决策委员会委员。管理的资金规模已超20亿，熟悉中国的资本市场，擅长宏观经济的研究和投资机会把握，具备扎实的投资分析能力和私募股权基金&ldquo;募、投、管、退、&rdquo;的方式方法。主要的投资案例有S&bull;collodi、百成新能源等。关注领域新能源、生物医药、影视文化投资阶段PE、PRE-IPO投资案例S&bull;collodi、百成新能源', 1, '1,3', 1, '2017-07-07 20:34:11', '2017-07-10 17:02:09', 0, 0);
INSERT INTO `tb_tutor` VALUES (195, '袁智勇', 'yuanzhiyong', 'y', '/upload/image/20170707/9519fbf8-9981-43de-b824-28a0e85d1469', 1, NULL, '赛伯乐投资', '合伙人', '杭州市', '赛伯乐投资合伙人', '现任浙江赛伯乐投资合伙人拥有技术经济及管理专业硕士学位，曾任职国内知名企业集团高级管理人，有十余年的项目投资、企业并购及企业管理经验。现受聘为IBMsmartcamp浙江区创业导师中国创新创业大赛评委杭州市等多地市人才创业项目评委主要关注云计算与大数据、信息安全、企业服务、集成电路、医疗服务、互联网金融等领域的投资机会负责投资过阜博通音影科技、深迪半导体、翰思安信、安畅云、哲信信息、华澜微、摸象数据、仰高医疗、才云科技等多个企业。单个项目的投资额度一般在1000万-6000万间。', 1, '1,3', 1, '2017-07-07 20:34:11', '2017-07-10 17:11:51', 0, 0);
INSERT INTO `tb_tutor` VALUES (196, '揭泽江', 'jiezejiang', 'j', '/upload/image/20170707/abd6fb7f-ae1e-4ba1-91d7-c53fa265d048', 1, NULL, '杭州魔豆工坊创业投资股份有限公司', '投资总监', '杭州市', '杭州魔豆工坊创业投资股份有限公司投资总监', '毕业于浙江大学曾在天堂硅谷担任高级投资经理目前在魔豆工坊担任投资总监。', 1, '1,3', 1, '2017-07-07 20:34:12', '2017-07-10 16:58:16', 0, 0);
INSERT INTO `tb_tutor` VALUES (197, '刘波', 'liubo', 'l', '/upload/image/20170707/11987056-9466-4e62-a6d2-29ff80745d7b', 1, NULL, '浙江银杏谷投资有限公司', '基金合伙人', '杭州市', '浙江银杏谷投资有限公司基金合伙人', '银杏谷资本高级基金经理，基金合伙人。&ldquo;创新牧场&rdquo;创业投资服务团队核心成员。银杏谷&ldquo;1024&rdquo;创业计划和孵化器创始人之一。刘波曾服务于多家科技公司，在科技企业投资兼并、产品、管理、市场与运营等领域具有丰富经验。对IT科技、仪器仪表、移动互联网、物联网和节能环保行业有较深理解。负责或参与了乐米科技、行者、数梦工场等多个项目的投资。', 1, '1,3,6', 1, '2017-07-07 20:34:12', '2017-07-10 16:45:09', 0, 0);
INSERT INTO `tb_tutor` VALUES (198, '孙桂军', 'sunguijun', 's', '/upload/image/20170707/5b69db61-360f-4398-ad93-336d9f033db6', 1, NULL, 'OK无忧', '联合创始人', '杭州市', 'OK无忧联合创始人', '十三年互联网行业经验、原支付宝O2O行业负责人、蚂蚁金服授权讲师；O2O项目孵化专家、互联网金融管理师、高级国际财务管理师SIFM 。OK无忧平台是浙江省政府、杭州市政府采购综合电商平台，同时也是面向企业的(B+S)2B的办公商用采购一站式平台，拥有共享经济模式的服务平台、产业供应链金融服务平台。', 1, '1,2,6', 1, '2017-07-07 20:34:12', '2017-07-10 16:55:57', 0, 0);
INSERT INTO `tb_tutor` VALUES (199, '吴嘉诚', 'wujiacheng', 'w', '/upload/image/20170707/bb16ec77-79a5-4bf4-8ed3-8f37099f0c02', 1, NULL, '杭州青士投资管理有限公司', '董事长', '杭州市', '杭州青士投资管理有限公司董事长', '杭州青士投资管理有限公司创始合伙人，毕业于浙江工商大学金融系。曾在美国哥伦比亚大学、土耳其卡迪尔&middot;哈斯大学访学交流。5年投资经历，曾就职永安期货股份有限公司、海宁中国皮革城互联网金融服务有限公司，善于发掘投资主题，构建投资组合，追求稳定收益。', 1, '1,2,3', 1, '2017-07-07 20:34:13', '2017-07-10 16:51:48', 0, 0);
INSERT INTO `tb_tutor` VALUES (200, '叶旭建', 'yexujian', 'y', '/upload/image/20170707/cba790de-3be9-4335-9321-c1125ac56df8', 1, NULL, '易介集团', '董事局主席兼总裁', '北京', '易介集团董事局主席兼总裁', '叶旭建先生，易介集团董事局主席兼总裁、中国互联网协会理事、网络品牌保护专家。1972年生于浙江青田，中共党员，1990年参加工作，曾在中国工商银行任职12年，经济师职称，担任总会计、总稽核等职务；自2000年起至今致力于网络品牌方案解决、域名应用、商标等知识产权品牌保护解决，在金融及网络品牌方案解决、域名投资领域有独到见解，系域名投资业界资深专家、网络品牌方案解决策划专家。', 1, '1,2,3', 1, '2017-07-07 20:34:13', '2017-07-10 16:49:57', 0, 0);
INSERT INTO `tb_tutor` VALUES (201, '郭新伟', 'guoxinwei', 'g', '/upload/image/20170707/97006d2a-30bd-43ad-ba3a-9e9fe3abafb4', 1, NULL, '草根投资', '风控总监', '杭州市', '草根投资风控总监', '浙江财经大学工商管理硕士，超过10年的中小企业金融融资服务经历。管理过超过500家中小企业的信用调查和资信评级，深谙中小企业融资难的现状。致力于从社会草根的视角解决中小企业的融资问题，优化金融资源的重新配置，在风险控制和风险管理能力方面具有丰富的经验。', 1, '1,2', 1, '2017-07-07 20:34:14', '2017-07-10 16:30:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (202, '黄昱程', 'huangyucheng', 'h', '/upload/image/20170707/83e2ac7b-786b-40d1-a04e-2f4587d8443f', 1, NULL, '南太湖金融创新基金', '执行合伙人', '杭州市', '南太湖金融创新基金执行合伙人', '创新基金执行合伙人，硕士学位；曾就职于电信局、邮政银行、南京新港高科、德国先灵、药物不良反应监测网；关注领域：移动互联网、新金融、股权投资等等；获各类专利和软件著作权17项。', 1, '1,2', 1, '2017-07-07 20:34:14', '2017-07-10 17:13:01', 0, 0);
INSERT INTO `tb_tutor` VALUES (203, '杨跃仁', 'yangyueren', 'y', '/upload/image/20170707/ad6ca302-4c83-435d-8f28-5aefc7410a23', 1, NULL, '贝兜金服', '副总经理', '杭州市', '贝兜金服副总经理', '杨跃仁，资深互联网人士，阿里巴巴资深小二，分别在阿里巴巴国际站，阿里集团市场部任职。对传统企业的互联网+有独到见解，从传统企业营销到互联网销售、地推，再到互联网运营、品牌策划，还有现在的互联网金融，从业经验丰富。连续创业者。', 1, '1,5,6', 1, '2017-07-07 20:34:15', '2017-07-10 17:01:42', 0, 0);
INSERT INTO `tb_tutor` VALUES (204, '闫春明', 'yanchunming', 'y', '/upload/image/20170707/ea94155f-2d2c-4da4-8633-8f599741edc8', 1, NULL, '网贷天下', '创始人兼COO', '杭州市', '网贷天下创始人兼COO', '6年互联网线上运营管理经验，是一个工作认真负责、积极主动、较强创新意识、执行力强，善于团队工作的人。先后做过保险门户网站，房地产门户，车贷P2P和股票配资P2P，熟知网站seo/sem，网站产品设计、活动策划、网站各渠道推广方式、网站品牌征信提升，熟知渠道广告投放方式。精确把控公司网站预算投入和产出；具有管理全公司网站运营50人以上团队经验；P2P网站注册量10万投资金额10亿。', 1, '1,2', 1, '2017-07-07 20:34:15', '2017-07-10 17:12:45', 0, 0);
INSERT INTO `tb_tutor` VALUES (205, '陈建文', 'chenjianwen', 'c', '/upload/image/20170707/5f33682f-0f4d-41b6-82d9-f5934c80600e', 1, NULL, '锦天城律师事务所', '合伙人', '杭州市', '锦天城律师事务所合伙人', '98年混迹江湖，三尺讲台显神威，上下五千年数理化纵横在神的世界里；00年成神人，是来杭第一站，深水游走在保险、培训、传销等神的圈子，说着神话，受着不同非常的激励，人生不经受洗脑，不经受成功学磨砺，不能说曾经年轻过，人定胜天与人由缘定，顺其自然，是进与退的人生哲学；02年成律界小兵，边缘游走，看了一点灰暗的社会，发现朋友太小，格局眼界太小，内功不够；从律界小兵混圈到媒界，经历媒体记者的主动追逐大佬，听其故事，观其作派，聊其趣味，发现人生还有多种可能，经济基础决定上层建筑，从听他人故事的天上人与凡间的律界人混圈，扎根着律界深入发展；06年投身律界的名门大派，沪上的“武当派”，绵里掌，登云梯，苦练内功十余载，集律界的诉讼达人与投资、知识产权、互联网创业等武当绝技一身，斗转星移，公司与家庭、创业与投资、上市与谋划纵横江湖，集合武当派各师兄师姐师弟师妹，融媒界的百晓生与肩挑妙笔的大佬奇人名仕，谱写一段律界小兵成长小伽史；15年互联网+“猪旋风”起，橙公益易成功揭杆起，阿里助阵律界小伽勇攀神女峰，聚小伙伴同闯创业，大数据、大学生、大平台、前期项目、精准广告、社交平台、众筹、商业模式、风险控制、股权激励、上市退出。。。。风云起，剑在手，问天下你是英雄！！！一起来，志同道合当称雄！', 1, '1', 1, '2017-07-07 20:34:16', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (206, '琚春华', 'juchunhua', 'j', '/upload/image/20170707/c82a1365-31dd-4b8e-918c-4c0abce8fb10', 1, NULL, '杭州市移动互联网技术学会', '副理事长', '杭州市', '杭州市移动互联网技术学会副理事长', '浙江工商大学计算机与信息工程学院院长，教授、博士、博士生导师。浙江工商大学电子服务与现代物流研究中心主任；教育部新世纪优秀人才、浙江省有突出贡献中青年专家、浙江省&ldquo;151人才工程&rdquo;第一层次人选、国务院特殊津贴获得者、教育部电子商务教指委委员、浙江省高等学校&ldquo;电子商务与物流优化创新团队&rdquo;带头人、浙江省重点学科（A类）&ldquo;管理科学与工程&rdquo;带头人、浙江省重点专业&ldquo;电子商务专业&rdquo;负责人、浙江省软件与集成电路重大专项组专家、浙江省服务业专家组成员、中国科技情报学会常务理事。主持国家自然科学基金、国家社科基金、国家科技支撑课题、教育部博士点基金等国家级、省部级项目20多项。以第一完成人获浙江省科学技术奖一等奖1次、省科学技术奖二等奖3次、省哲学社会科学优秀成果奖三等奖1次、省级教学成果奖一等奖1次；以第二完成人获浙江省科学技术奖一等奖1次、二等奖1次，省哲学社会科学优秀成果奖三等奖1次；发表学术论文50多篇，其中特级期刊、一级期刊、SCI/EI论文30多篇；主编出版学术著作5部、教材3部。获教育部骨干教师基金、包玉刚留学基金、教育部出国留学基金，在英国Aberdeen大学、美国Missouri大学、加拿大McMaster大学访问学习。多年来致力于企业信息化管理理论、电子商务与物流、决策支持系统等方面的研究，科研成果在学术界具有一定的影响。', 1, '1,6', 1, '2017-07-07 20:34:16', '2017-07-10 17:09:27', 0, 0);
INSERT INTO `tb_tutor` VALUES (207, '蔡维格', 'caiweige', 'c', '/upload/image/20170707/0c7db134-cbbd-438b-ad49-81042056f382', 1, NULL, '融牛网', '技术总监', '杭州市', '融牛网技术总监', '本科毕业于浙江理工大学信息计算科学系，北京大学MBA管理培训。2008年合伙创立在线婚庆O2O服务平台，主要负责技术研发，平台先后服务超过2000多对新人。2010年自主创业，主营互联网系统开发业务。2012年涉足P2P网络借贷领域，先后参与温州贷等国内领先的P2P平台前期筹备工作。2012年4月联合创办杭州融都科技有限公司，任高级副总裁兼技术总监。融都科技站在行业的风口发展迅速，2年多的时间里，先后开发了近500家P2P平台。融都科技于2015年04月被恒生电子收购，同年6月成功在新三板上市。在圈子中被大家称为90后互联网金融创业之星。现主持融牛网技术研发，证券期货在线策略交易项目。目前同时带领团队研究机器人智能交易项目。', 1, '1,2', 1, '2017-07-07 20:34:16', '2017-07-10 17:11:36', 0, 0);
INSERT INTO `tb_tutor` VALUES (208, '何人及', 'herenji', 'h', '/upload/image/20170707/127544cd-5599-42bf-97a3-e639144ba541', 1, NULL, '浙江聚泓投资管理邮箱公司', '总经理', '杭州市', '浙江聚泓投资管理邮箱公司总经理', '1997年毕业于哈尔滨工程大学经济管理学院，2000年获得浙江大学硕士学位。曾服务华为技术有限公司、中华英才网、中兴通讯等企业，历任人力资源经理、销售总监、分公司总经理等职务；2011伊始在云计算领域、投资领域自主创业，现任浙江聚泓投资管理有限公司创始人，总经理。', 1, '1,3', 1, '2017-07-07 20:34:17', '2017-07-10 16:36:01', 0, 0);
INSERT INTO `tb_tutor` VALUES (209, '陈刚', 'chengang', 'c', '/upload/image/20170707/230b7363-0f10-4fd8-8032-9b0e7f7ebf76', 1, NULL, '网易杭州研究院', '院长', '北京', '网易杭州研究院院长', '教授，博士生导师，2013年获得浙江省青年科技奖，2012年获得中创软件人才奖，入选2007年度教育部新世纪人才支持计划。近五年来，在863、国家科技支撑、国家自然科学基金等项目的资助下，申请人在数据库及大数据研究领域，围绕云数据库管理技术、互联网大数据处理技术、多源异构大数据并行处理技术等方向展开研究工作，取得了多项重要的创新成果，近五年来在CCFA类国际期刊和学术会议上发表论文40余篇，研究工作分别获得CCFA类国际会议VLDB2014最佳论文奖和IDCE2012、ICDE2015、ACMMM2015最佳论文提名。获授权国家专利34项，其中发明专利29项，获得转让和授权使用15项。', 1, '1,4', 1, '2017-07-07 20:34:17', '2017-07-10 16:32:23', 0, 0);
INSERT INTO `tb_tutor` VALUES (210, '漆远', 'qiyuan', 'q', '/upload/image/20170707/9f54e254-5b5d-49ba-8147-f5a7925f7318', 1, NULL, '蚂蚁金服', '副总裁、首席数据科学家', '北京', '蚂蚁金服副总裁、首席数据科学家', '麻省理工学院博士，国家千人特聘专家，现任蚂蚁金服副总裁、首席数据科学家。目前致力于大规模机器学习和深度学习平台的建立及其在蚂蚁金服各项业务的应用。担任过机器学习权威杂志JournalofMachineLearningResearch的执行编辑和全球机器学习顶级会议ICML的领域主席，获得过微软牛顿研究奖和美国科学基金NSFCareer奖。', 1, '1,2', 1, '2017-07-07 20:36:20', '2017-07-10 17:02:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (212, '山口 耀司', 'shankou yaosi', 's', '/upload/image/20170707/00abb75c-8d9f-4e96-83ce-45fe467aa673', 1, NULL, 'SKO株式会社', '理事', '北京', 'SKO株式会社理事', '1957年出生，曾担任麻生教育服务会社咨询董事会的部长，现在作为SKO株式会社的理事负责亚洲地区的教育人才开发，包括组织开发、人才开发提案咨询，业务改善支援提案咨询等。业务主要以行政、上市企业、地方中小企业、医院、养老设施为主，无论是营利型企业还是非营利型企业都有良好的成绩。', 1, '1,6', 1, '2017-07-07 20:36:21', '2017-07-10 16:57:28', 0, 0);
INSERT INTO `tb_tutor` VALUES (214, '叶新江', 'yexinjiang', 'y', '/upload/image/20170707/9c778004-a790-4b9b-bb29-f5f4874fe404', 1, NULL, '个推', 'CTO', '北京', '个推CTO', '2006-2011就职于Ericsson广州研发中心任高级架构师、微软MSN任首席架构师。2011至今在个推担任CTO，个推省级企业大数据研究院院长。个推已成为中国最大最专业的手机信息推送服务提供商。在无线通信领域、手机应用开发、大规模并发平台、分布搜索系统、大数据处理系统等领域，积累了丰富的实战经验。在大型系统项目的管理上具有相当的经验，对于打造高效团队方面亦拥有丰富经验。', 1, '1', 1, '2017-07-07 20:36:23', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (215, '李兰娟', 'lilanjuan', 'l', '/upload/image/20170707/d1156eac-3d4c-491d-889b-5c1d5676b03c', 1, NULL, '中国工程院', '院士', '杭州市', '中国工程院院士', '', 1, '1', 1, '2017-07-07 20:36:23', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (216, '黄海钧', 'huanghaijun', 'h', '/upload/image/20170707/2ed63d7e-e177-4068-a07a-5f1dc18e5835', 1, NULL, '纷享销客', '高级副总裁', '北京', '纷享销客高级副总裁', '', 1, '1,6', 1, '2017-07-07 20:36:24', '2017-07-10 17:13:30', 0, 0);
INSERT INTO `tb_tutor` VALUES (217, '涂子沛', 'tuzipei', 't', '/upload/image/20170707/69f586be-edd1-4a4c-85e5-c7fa24ab9e4d', 1, NULL, '观数科技', '联合创始人', '北京', '观数科技联合创始人', '', 1, '1,6', 1, '2017-07-07 20:36:24', '2017-07-10 17:02:27', 0, 0);
INSERT INTO `tb_tutor` VALUES (218, '章苏阳', 'zhangsuyang', 'z', '/upload/image/20170707/f4ef2b02-5608-4082-addd-583b2758695a', 1, NULL, '火山石资本', '创始合伙人', '上海', '火山石资本创始合伙人', '', 1, '1,3', 1, '2017-07-07 20:36:25', '2017-07-10 17:10:24', 0, 0);
INSERT INTO `tb_tutor` VALUES (219, '车品觉', 'chepinjue', 'c', '/upload/image/20170707/34049c4f-8fed-4f2e-abc6-188414a36c89', 1, NULL, '红杉资本中国基金', '专家合伙人', '北京', '红杉资本中国基金专家合伙人', '', 1, '1,3', 1, '2017-07-07 20:36:25', '2017-07-10 17:12:13', 0, 0);
INSERT INTO `tb_tutor` VALUES (220, '石鹏峰', 'shipengfeng', 's', '/upload/image/20170707/8725869e-c6c5-42b6-af8e-881726f0e4f1', 1, NULL, '网贷之家', '联合创始人', '北京', '网贷之家联合创始人', '', 1, '1,2', 1, '2017-07-07 20:36:26', '2017-07-10 17:09:46', 0, 0);
INSERT INTO `tb_tutor` VALUES (221, '朱啸虎', 'zhuxiaohu', 'z', '/upload/image/20170707/0cd89561-0171-47e5-87af-82d344ffd02b', 1, NULL, '金沙江创投基金', '合伙人、董事总经理', '北京', '金沙江创投基金合伙人、董事总经理', '', 1, '1,3', 1, '2017-07-07 20:36:26', '2017-07-10 16:58:35', 0, 0);
INSERT INTO `tb_tutor` VALUES (222, '钱蓓蕾', 'qianbeilei', 'q', '/upload/image/20170707/62ea43c4-50a7-4f39-b5a6-755d4b2efe12', 1, NULL, '网易', '高级总监', '杭州市', '网易高级总监', '', 1, '1', 1, '2017-07-07 20:36:27', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (223, '李爱君', 'liaijun', 'l', '/upload/image/20170707/048b9fac-727e-44f7-aff6-7b8f751cb2e0', 1, NULL, '中国政法大学', '教授、博导', '北京', '中国政法大学教授、博导', '', 1, '1,2', 1, '2017-07-07 20:36:27', '2017-07-10 17:01:22', 0, 0);
INSERT INTO `tb_tutor` VALUES (224, '倪光南', 'niguangnan', 'n', '/upload/image/20170707/8a567c4e-451e-4b0f-8d57-4a9192dab94e', 1, NULL, '中国工程院', '院士', '北京', '中国工程院院士', '', 1, '1', 1, '2017-07-07 20:36:28', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (225, '王阳', 'wangyang', 'w', '/upload/image/20170707/a771ed30-bc6b-4c8e-899a-6ce8d2dde149', 1, NULL, '赛伯乐投资集团', '总裁', '北京', '赛伯乐投资集团总裁', '', 1, '1,3', 1, '2017-07-07 20:36:28', '2017-07-10 17:03:20', 0, 0);
INSERT INTO `tb_tutor` VALUES (226, '何一兵', 'heyibing', 'h', '/upload/image/20170707/2b1af3bd-02c6-4f09-ae44-ff1419e3216e', 1, NULL, '脸脸', 'CEO', '杭州市', '脸脸CEO', '脸脸CEO，e签宝董事长，中国互联网&ldquo;骨灰级&rdquo;人物，1995年和马云联合创建&ldquo;中国黄页&rdquo;，旗下&ldquo;e签宝&rdquo;已成为全国领先的互联网电子签名服务平台，脸脸APP已成为中国购物中心与实体商业领域最大的移动互联网技术服务商。', 1, '1,6', 1, '2017-07-07 20:36:29', '2017-07-10 16:36:26', 0, 0);
INSERT INTO `tb_tutor` VALUES (227, '潘豪杰', 'panhaojie', 'p', '/upload/image/20170707/9c341bff-4fb3-4fed-9dce-38147548ce82', 1, NULL, '分期管家', '创始人', '杭州市', '分期管家创始人', '', 1, '1,2', 1, '2017-07-07 20:36:29', '2017-07-10 17:03:01', 0, 0);
INSERT INTO `tb_tutor` VALUES (228, '张斯成', 'zhangsicheng', 'z', '/upload/image/20170707/6fea7dbc-60d0-4d5b-9075-cf12a0633bb4', 1, NULL, '钉钉', '副总裁', '杭州市', '钉钉副总裁', '现任职于阿里巴巴集团钉钉事业部，商务副总裁，毕业于香港城市大学。', 1, '1', 1, '2017-07-07 20:36:29', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (229, '范凯', 'fankai', 'f', '/upload/image/20170707/0b77d89f-84d1-43e7-80a9-a22116de1f87', 1, NULL, '丁香园', 'CTO', '杭州市', '丁香园CTO', '范凯，JavaEye技术网站创始人，资深软件工程师，多年从事专业的软件技术咨询、培训、软件架构设计和软件开发解决方案，具有丰富的应用软件设计和开发的经验。2003年创建JavaEye开发者社区，2006年开始创业正式运营JavaEye网站，2010年被CSDN收购，任CSDN产品副总裁与CTO等职务。2015年8月，正式加盟丁香园出任技术副总裁，负责公司技术团队的管理工作。', 1, '1,6', 1, '2017-07-07 20:36:30', '2017-07-10 16:29:54', 0, 0);
INSERT INTO `tb_tutor` VALUES (230, '程立', 'chengli', 'c', '/upload/image/20170707/a23fbb91-b0f7-4cf9-8c04-e1cd8f72c600', 1, NULL, '蚂蚁金服', 'CTO', '杭州市', '蚂蚁金服CTO', '程立，花名鲁肃；阿里巴巴集团合伙人、蚂蚁金服CTO。2004年2月，一个偶然的机会，程立接触到阿里巴巴，此时他正在上海交通大学攻读博士学位；2005年2月，他正式加入了支付宝，是支付宝技术平台的奠基人之一，主持了支付宝各代技术架构的规划与基础技术平台的建设，设计并实施了支付宝一系列关键业务系统。其严谨务实、逻辑严密，被阿里员工誉为&ldquo;神一样的存在&rdquo;。', 1, '1,2,5', 1, '2017-07-07 20:36:30', '2017-07-10 16:31:29', 0, 0);
INSERT INTO `tb_tutor` VALUES (231, '董建刚', 'dongjiangang', 'd', '/upload/image/20170707/dfc743c6-477a-4c29-a694-3a395f568c46', 1, NULL, '米庄理财', 'CTO', '杭州市', '米庄理财CTO', '毕业于浙江工商大学信息管理学院，有十几年的IT行业从业经验；曾任国泰科技副总经理一职，目前担任米庄理财CTO，全面负责公司产品技术方面的设计、架构、开发工作。对企业技术管理有着深厚的理解，是公司技术战略的重要执行者，从技术角度高效地推动公司发展。', 1, '1,2', 1, '2017-07-07 20:36:31', '2017-07-10 17:11:01', 0, 0);
INSERT INTO `tb_tutor` VALUES (232, '刘清富', 'liuqingfu', 'l', '/upload/image/20170707/f052c78d-97fc-4939-96b8-34ac5d1cc59e', 1, NULL, '挖财', 'CTO', '杭州市', '挖财CTO', '本科和研究生就读于哈尔滨工业大学，获得工学学士和工学硕士学位。研究生毕业后先后任职中国联通研发工程师，亚信科技技术总监。2004年-2007年任职百度（中国）有限公司技术总监，负责百度平台技术部、百度全部产品线的产品和技术质量保证。2008-2014年任职阿里巴巴集团资深总监，先后负责阿里巴巴B2B CRM&amp;QA技术；阿里国际事业群的技术和产品管理、团队管理；速卖通Aliexpress.com事业部的技术和产品管理、团队管理。2014年2月到2016年6月先后任职蚂蚁金服集团财富事业群资深技术总监、蚂蚁金服集团CTO助理、蚂蚁金服技术战略工作组成员，负责蚂蚁金服财富线余额宝、招财宝、保险技术和众筹技术、基金技术开发和财富技术部团队管理。2016年7月加入挖财，出任挖财网络技术有限公司 首席技术官（CTO），全面负责挖财的技术战略发展和工程技术团队管理。', 1, '1,2', 1, '2017-07-07 20:36:32', '2017-07-10 16:46:22', 0, 0);
INSERT INTO `tb_tutor` VALUES (233, '周华平', 'zhouhuaping', 'z', '/upload/image/20170707/c9c03250-212f-4de7-bd9d-cd27d53fed70', 1, NULL, '分期管家', 'CTO', '杭州市', '分期管家CTO', '09年到13年，在淘宝历任资深Java工程师，iOS工程师，先后参与淘江湖、淘金币、淘伴等产品研发和小组管理工作。离开淘宝之后，加入创业大军，作为技术负责人加入到多听FM，在移动互联网的风口上，快速把用户做到数千万规模。16年加入分期管家任职CTO，投身普惠金融行业，让金融更好地为每一个人服务。', 1, '1,2', 1, '2017-07-07 20:36:32', '2017-07-10 16:55:08', 0, 0);
INSERT INTO `tb_tutor` VALUES (234, '徐琨', 'xukun', 'x', '/upload/image/20170707/eeb0c79f-ca34-4f05-abbd-77c13d276d21', 1, NULL, 'Testin云测', '总裁', '杭州市', 'Testin云测总裁', '中国最早的移动互联网公司PICA创始员工，曾任PICA技术副总裁。国内顶级HTML5游戏公司山水地信息创始人。主导开发运营过千万级用户的移动社交平台，目前在Testin云测担任总裁一职，为所有移动开发者提供从功能测试，到兼容测试，到自动化测试，到发布后的质量跟踪一站式测试服务。', 1, '1', 1, '2017-07-07 20:37:34', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (235, '翟景龙', 'zhaijinglong', 'z', '/upload/image/20170707/daf6f4f1-bf98-4060-8410-c319910820dd', 1, NULL, '爱贷网', 'CTO', '杭州市', '爱贷网CTO', '毕业于长春工业大学，从事技术工作10多年。擅长技术研发和创新、系统架构和项目管理，有互联网金融行业技术体系搭建、技术项目开发和运作的成功经验。曾就职于淘宝网络技术公司、中策公司。', 1, '1,2', 1, '2017-07-07 20:37:34', '2017-07-10 17:10:42', 0, 0);
INSERT INTO `tb_tutor` VALUES (236, '胡红霞', 'huhongxia', 'h', '/upload/image/20170707/2e15a144-c293-4e75-a370-ca22aa9693bb', 1, NULL, '微医集团', 'CTO', '杭州市', '微医集团CTO', '2012年加入微医，组建微医强大的技术团队，主导和推动微医所有重大业务系统的研发，并获得多项互联网医疗技术专利；曾任阿里巴巴集团高级架构师，负责诸如来往等过多个业务。微医为中国上亿用户提供可信赖的预约挂号、在线问诊、远程会诊、电子处方、药品配送等互联网医疗服务，已覆盖29个省，连接2400多家重点医院，拥有1.5亿实名用户和26万名专家。', 1, '1', 1, '2017-07-07 20:37:35', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (237, '蒋蓬', 'jiangpeng', 'j', '/upload/image/20170707/3463d41c-f975-4fb5-9bba-cfb02a7d0143', 1, NULL, '杭州联合银行', '网络金融部总经理', '杭州市', '杭州联合银行网络金融部总经理', '蒋蓬，现任杭州联合银行网络金融部总经理。从软件开发岗做起，从事过市民卡、中间业务、核心业务系统开发。3年后成功转型业务，历任宝善支行副行长、行长一职。在支行一线工作5年的工作中，较好的平衡了业务发展和风险控制。后调任总行信息科技部副总经理，2015年9月担任新成立的网络金融部总经理。牵头开发了杭州联合银行第一款互联网金融产品&ldquo;联银e贷&rdquo;，并上线了第一款房租贷产品。将房租分期申请嵌入租房场景，还在银行业内率先试点&ldquo;电子签名&rdquo;和&ldquo;人脸识别&rdquo;技术，完美解决了客户&ldquo;开卡&rdquo;和&ldquo;面签&rdquo;的痛点。上线不到3个月，新增贷款3000多笔，贷款余额近8000万元。此外，还上线了&ldquo;市民贷&rdquo;产品，客户只要&ldquo;刷脸&rdquo;就可以实时获取授信额度，最高额度50万。大大提升了客户体验度，对传统银行的授信模式有了较大的突破。', 1, '1,2', 1, '2017-07-07 20:37:35', '2017-07-10 17:11:19', 0, 0);
INSERT INTO `tb_tutor` VALUES (238, '朱克强', 'zhukeqiang', 'z', '/upload/image/20170707/f88c7dee-63e9-423c-ad6f-e9285534557a', 1, NULL, '名医主刀', 'CTO', '杭州市', '名医主刀CTO', '曾就职于美国亚马逊，回国后加入阿里巴巴，担任天猫超市架构师，天猫搜索技术负责人等职位。2015年加入创业大军，曾以合伙人兼CTO的身份帮助福佑卡车完成从0到1的阶段，目前是名医主刀的CTO。', 1, '1', 1, '2017-07-07 20:37:36', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (239, '芦宇峰', 'luyufeng', 'l', '/upload/image/20170707/2ba3c69b-e739-406a-9842-d79466e3f961', 1, NULL, '二维火', 'CTO', '杭州市', '二维火CTO', '1996年起在江西从事软件行业，经验丰富，技术纯熟，积累了一定名气。2005年同微软团队合作，通过交流受益匪浅，在技术上迈入更广阔天地。2005年下半年，在杭州创办自己的公司，在中国博客网工作，是NET俱乐部杭州站的组建负责人。2006年12月进入阿里巴巴工作，从M2、M3级员工中脱颖而出，进入胡汉管理学院。2011年，进入支付宝，带领着50多人的团队，负责用户端的开发、运营和维护。2012年8月，离开阿里巴巴，自主创业，打造了一款移动旅游问答APP——“旅途求助”。2014年夏，加盟杭州二维火（餐饮业为客户提供智能收银机业务的O2O平台），如今担任技术研发中心CTO，在其技术主导下，平台日渐完善，绿茶、新白鹿、外婆家等越来越多的餐饮连锁品牌选择了二维火，该平台于2016年7月拿到支付宝数亿元B轮融资。', 1, '1', 1, '2017-07-07 20:37:36', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (240, '郭英凯', 'guoyingkai', 'g', '/upload/image/20170707/31efce80-a2ab-4325-9a34-9a575768d573', 1, NULL, '上海北信源信息技术有限公司', '技术副总裁', '上海', '上海北信源信息技术有限公司技术副总裁', '1988-1992, 1994-1997年在西北工业大学计算机系学习，先后获学士、硕士学位； 2001年于上海交通大学图像所获得博士学位。ＩＲＣＡ注册信息安全／ＩＴ服务管理／质量管理主任审核员，信息系统审计与控制协会(ISACA)和信息安全论坛（ISF）的会员。 先后任职于申银万国证券有限公司、BSI 英国标准协会、上海北信源信息技术有限公司，主要从事信息战略规划、信息系统规划、信息安全管理、ＩＴ服务管理、数据中心运行维护管理以及信息系统管理体系的的设计、实施以及改进工作。涉及信息系统规划、信息资源管理、信息安全、IT服务管理、业务连续性管理、信息系统审计、全面质量管理、SoX合规内审和外审、IT治理以及过程改进多个领域。', 1, '1', 1, '2017-07-07 20:37:37', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (241, '郭威', 'guowei', 'g', '/upload/image/20170707/9c32fd9b-8dd5-4f6f-a062-89b21f7e2fa1', 1, NULL, '51信用卡', 'CTO', '杭州市', '51信用卡CTO', '浙大本硕；从网易杭研到Vobile再到阿里巴巴，完整经历了中国互联网从Web2.0开始的全过程；离开阿里前担任天猫资深技术专家(总监)，负责天猫的消费者端业务和服饰/美妆/魅力惠等垂直行业的技术团队，担任2015年阿里巴巴双11电商技术总负责人；2016年7月加入51信用卡担任CTO。', 1, '1,2', 1, '2017-07-07 20:37:38', '2017-07-10 16:06:44', 0, 0);
INSERT INTO `tb_tutor` VALUES (242, '徐志欣', 'xuzhixin', 'x', '/upload/image/20170707/9b9ac921-726e-4476-82ae-85a1c5f209a1', 1, NULL, '惠盈金服', 'CTO', '北京', '惠盈金服CTO', '10几年的软件、互联网工作经验，从事过企业信息化、MIS、ERP、物联网、电子政务、电商的开发、管理等工作； 曾任京东医药城首席技术官，目前就职于北京惠众商务顾问有限公司担任首席技术官一职。在系统及架构设计方面，有自己独到的见解。', 1, '1,2', 1, '2017-07-07 20:37:38', '2017-07-10 16:57:55', 0, 0);
INSERT INTO `tb_tutor` VALUES (243, '刘丽娟', 'liulijuan', 'l', '/upload/image/20170707/15b079d8-a369-48bb-bcaa-27ce4684ff6d', 1, NULL, '阿里巴巴', 'B2B技术部数据应用总监', '杭州市', '阿里巴巴B2B技术部数据应用总监', '1992年毕业于浙江大学，获得电机工程系应用电子专业学士学位，96年毕业于美国犹他州立大学，获得计算机科学硕士学位，2004年毕业于比利时鲁汶大学获得工业管理硕士学位。刘丽娟硕士毕业后在美国的IT业做软件工程师，在获得工业管理硕士学位后，开始从事大数据科学方面的工作。2005-2010年就职Yahoo期间担任Yahoo风控算法团队的高级算法工程师、架构师；2010-2016年间分别就职于美国IAC, OpenX, Zefr担任大数据部门总监、资深总监。2016至今在Alibaba B2B担任大数据部门负责人。', 1, '1', 1, '2017-07-07 20:37:39', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (244, '尉建锋', 'weijianfeng', 'w', '/upload/image/20170707/9e3412b6-2810-41e8-ba0d-87e8e8ad0395', 1, NULL, '卓健科技', '创始人', '杭州市', '卓健科技创始人', '杭州卓健科技创始人、董事长、CEO首席执行官，浙江省特聘专家，外科学留美博士，浙大一院肝胆外科医生，是国内“掌上医院”首创者，卓健科技产品总设计师。2012年全国首发掌上医院项目，2013年入选浙江省引进海外高层次人才“千人计划”、杭州市全球引才“521”计划创业人才、杭州市“131”中青年人才“一层次”培养计划人选。2014年国内首次提出“医患友好度”，持续引领以患者为中心的医院信息化建设，被《健康报》及卫计委确定为行业标准。2015年全国首创移动远程医疗概念，探索全新的移动就医模式。', 1, '1', 1, '2017-07-07 20:37:39', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (253, '汪素琴', 'wangsuqin', 'w', '/upload/image/20170707/f793a4d8-cb72-4873-800e-8adf14a3c5c4', 1, NULL, '莫干山国家高新园区科技新城管理委员会', '副主任', '杭州市', '莫干山国家高新园区科技新城管理委员会副主任', '毕业于浙江林学院，分别在团县委、乡镇、发改委及科技新城工作，在发改委主要从事五年发展规划制定、经济运行分析、投资管理和项目审批、服务业发展规划、产业政策制定等工作，目前主要工作是科技新城省级金融产业集聚区规划和产业发展，产业基金运作、高层次人才项目引进等工作。本人熟悉县域经济发展空间规划、政府产业政策、税收政策、人才政策，可为项目做好前期政策资源对接和政策咨询筹划。', 1, '2', 1, '2017-07-07 20:39:59', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (259, '陈斌', 'chenbin', 'c', '/upload/image/20170707/d4c6fc71-0e16-49a0-adaa-46dcb48af300', 1, NULL, '浙江赛伯乐基金', '创始合伙人', '北京', '浙江赛伯乐基金创始合伙人', '现任浙江赛伯乐创业投资管理公司创始合伙人兼总裁、管理100多亿人民币基金，国家千人计划评审专家，浙江省股权投资协会常务副会长、浙江省创业风险投资协会常务副会长,浙江省金融十大领军人物，浙江大学经济学院兼职硕士导师，同时担任几十家高科技成长型企业的董事。荣获&ldquo;2011年中国新锐投资人50强&rdquo;和&ldquo;浙江省十大金融领军人物&rdquo;等称号。', 1, '2,3', 1, '2017-07-07 20:40:02', '2017-07-10 16:32:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (267, '张新波', 'zhangxinbo', 'z', '/upload/image/20170707/c9599ef5-9f91-4351-bd09-60d69b1245e0', 1, NULL, '同盾科技', '联合创始人兼技术副总裁', '杭州市', '同盾科技联合创始人兼技术副总裁', '同盾科技联合创始人兼技术副总裁，主要负责公司核心产品的研发、技术体系的搭建和研发效率的提升，在高可用技术架构和性能优化、实时计算、大数据平台、自动化运维等方面有较多实践。09年加入阿里巴巴AliExpress风控团队，作为创始成员开始参与第一代国际支付风控体系的研发，之后负责过国际站、集团安全部多个重要风控系统和相关业务。', 1, '2', 1, '2017-07-07 20:40:06', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (272, '吴逖', 'wuti', 'w', '/upload/image/20170707/c097043c-3d10-4272-91b1-44fa6e513409', 1, NULL, '众安保险', '副总裁', '杭州市', '众安保险副总裁', '现任众安在线财产保险股份有限公司副总经理，分管个人信保部、机构金融部、投资产品部、资产管理部等。曾毕业于江西财经大学，获得学士学位，历任平安保险（集团）股份有限公司综合金融团体客户市场部副总经理，上海陆家嘴国际金融交易市场股份有限公司产品研发部、会员管理部、贸易融资部副总经理等职务。', 1, '2,3', 1, '2017-07-07 20:41:14', '2017-07-10 16:52:46', 0, 0);
INSERT INTO `tb_tutor` VALUES (273, '刘思宇', 'liusiyu', 'l', '/upload/image/20170707/7e4ba2c2-4f2e-48a5-9cdb-190fe5db99fd', 1, NULL, '贷款钱包', 'CEO', '杭州市', '贷款钱包CEO', '担任首都经贸大学特聘教师、中国政法大学创业导师、阿里巴巴·蚂蚁金服顾问、长江证券顾问等社会职务。2014年与校友共同创立中国著名股权众筹平台天使街，目前已经成为中国首批在中国证券业协会备案的股权众筹平台，设立子公司50余家，为数十个创业项目完成近2亿的融资。2015年创立51理财APP，以过亿估值获业内专业投资机构和投资人认可。连续十四年致力于社会公益事业并做出贡献，当选2011中国教育十大影响人物，被媒体誉为“中国当代先锋”。', 1, '2', 1, '2017-07-07 20:41:15', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (286, '安连武', 'anlianwu', 'a', '/upload/image/20170707/685e86a9-6cd5-428a-bc57-8021feaa8776', 1, NULL, '投融家', 'CTO', '杭州市', '投融家CTO', '国内知名 P2B 平台 核心创始成员,任职 CTO，拥有 15 年以上技术研发、项目管理经验。曾先后任职于上海力铭、恒生电子、鑫合汇，带领完成20多家商业银行的票据项目研发。主导完成了多个大型平台和系统的架构设计、研发等工作,包括银行内部系统、互联网金融平台及企业大数据征信平台。在大数据和高并发系统的构建，及团队建设方面有丰富实战经验；精于互联网金融领域的技术开发及运营。目前任职于投融家，总体把控技术战略方向、产品研发和互联网金融平台的运营。', 1, '3', 1, '2017-07-07 20:43:19', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (287, '江南愤青', 'jiangnanfenqing', 'j', '/upload/image/20170707/967f49bf-6133-40da-a886-9f670932e89a', 1, NULL, '自由投资人', '著名财经评论家', '杭州市', '自由投资人著名财经评论家', '原名陈宇，江南1535茶馆创始人，聚秀资本合伙人。目前投资近两百家互联网企业，担任京东金融、挖财等多家知名互联网金融企业的首席战略顾问。', 1, '3', 1, '2017-07-07 20:43:19', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (294, '谭书华', 'tanshuhua', 't', '/upload/image/20170707/84356487-95c5-43c4-9ae4-c87f3c0d3508', 1, NULL, '圆通速递', 'CTO', '上海', '圆通速递CTO', '谭书华，圆通速递有限公司CTO。有13年多信息化研发及管理经验，一直从事信息化的规划、架构、研发、运维管理等工作，曾负责多个物流快递核心系统的架构设计、开发及团队管理工作，加入圆通速递以来，先后负责圆通5.0核心系统、金刚核心系统、CRM系统等信息化系统的建设和管理。同时担任CCF YOCSEF上海AC委员。', 1, '5', 1, '2017-07-07 20:45:44', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (296, '陈忠才', 'chenzhongcai', 'c', '/upload/image/20170707/31f364dc-3b5f-49c7-a13e-d8f3c78904e6', 1, NULL, '硅谷堂', 'CEO', '杭州市', '硅谷堂CEO', '硅谷堂创始人/CEO，硅谷堂是互联网领域的知识获取和分享平台。基于数据挖掘，为企业和企业员工提供定制化的知识获取和管理服务。本人先后就读于浙江大学、北京大学和中国科学院，2011年至2013年在浙江大学公共管理学院做企业博士后，研究课题《面向执行的企业创新体系构建研究》。自2009年起，我在浙江大学和浙大网新集团，先后从事过学历教育、企业家培训、政府干部培训、IT培训等领域。同时具有互联网和教育、理科和管理学的知识背景。2012年至2013年，在斯坦福大学商学院访学，期间调研过硅谷Google、Intel、Apple、IBM等公司，2015年成立杭州图知信息科技有限公司，推出硅谷堂。', 1, '6', 1, '2017-07-07 20:46:28', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (297, '赵红军', 'zhaohongjun', 'z', '/upload/image/20170707/baa3dd58-de26-47f4-b9c3-5645fddc4441', 1, NULL, '和财富', 'CEO', '杭州市', '和财富CEO', '浙江大学本科，在校期间就有多次创业经历。歌亿财富创始人，主持发行多支政府类基金、对冲量化基金。曾供职于农业银行、方正证券等金融机构创新业务部。毕业后凭借扎实的金融专业知识相继成为农业银行、方正证券、宝汇金融等公司的核心成员。曾作为主要成员陆续参与宝汇金融、第五金融网等项目，2012年主导宝汇理财app项目。2015年7月获得胡润百富财富新势力50强，成为业界黑马。', 1, '6', 1, '2017-07-07 20:46:29', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (302, '李彦强', 'liyanqiang', 'l', '/upload/image/20170707/a9e6f1a7-7708-4c53-bfb0-8a913cb16e93', 1, NULL, '美国道富银行', '技术副总裁', '杭州市', '美国道富银行技术副总裁', '杭州市经济信息人才理事。毕业于浙江大学计算机学院，12年软件开发，构架和团队管理经验，主导开发过多个金融、电商软件平台，和App系统。主导开发的App正品折扣获得数百万的下载量。擅长Java,Ruby,iOS等多种编程技术。国内首批IOS技术开发者，开发APP产品近30款，有丰富的团队组建和团队管理经验，组建和管理了开发，构架，系统分析，UED，数据分析等多个团队。曾任支付宝技术专家，现担美国道富银行技术副总裁。', 1, '6', 1, '2017-07-07 20:46:31', '2017-07-10 16:20:43', 0, 0);
INSERT INTO `tb_tutor` VALUES (303, '胡德华', 'hudehua', 'h', '/upload/image/20170713/1499928469033027182.jpg', 1, '', '投融家', '联合创始人兼CEO', '杭州', '', '', 0, '2', 1, '2017-07-13 14:13:33', '2017-07-13 14:50:39', 1, 0);
COMMIT;

-- ----------------------------
-- Table structure for tb_type_article
-- ----------------------------
DROP TABLE IF EXISTS `tb_type_article`;
CREATE TABLE `tb_type_article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL ,
  `delete_flag` tinyint(2) DEFAULT '0' COMMENT '0-正常，1-删除',
  `status` tinyint(2) DEFAULT '0' COMMENT '0-正常，1-停用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='信息类型';

-- ----------------------------
-- Records of tb_type_article
-- ----------------------------
BEGIN;
INSERT INTO `tb_type_article` VALUES (1, '资讯', '2017-06-28 14:46:09', '2017-07-05 14:06:38', 0, 0);
INSERT INTO `tb_type_article` VALUES (2, '活动', '2017-06-28 14:47:11', '2017-06-28 14:48:26', 0, 0);
INSERT INTO `tb_type_article` VALUES (3, '动态', '2017-06-28 14:48:10', '2017-06-28 14:48:10', 0, 0);
INSERT INTO `tb_type_article` VALUES (4, '成功案例', '2017-08-17 20:06:41', '2017-08-17 20:06:41', 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for tb_type_domain
-- ----------------------------
DROP TABLE IF EXISTS `tb_type_domain`;
CREATE TABLE `tb_type_domain` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL ,
  `delete_flag` tinyint(2) DEFAULT '0' COMMENT '0-正常，1-删除',
  `status` tinyint(2) DEFAULT '0' COMMENT '0-正常，1-停用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='领域类型';

-- ----------------------------
-- Records of tb_type_domain
-- ----------------------------
BEGIN;
INSERT INTO `tb_type_domain` VALUES (1, '互联网', '2017-06-28 14:32:10', '2017-06-28 14:43:27', 0, 0);
INSERT INTO `tb_type_domain` VALUES (2, '金融', '2017-06-28 14:32:10', '2017-06-28 14:43:07', 0, 0);
INSERT INTO `tb_type_domain` VALUES (3, '投融资', '2017-06-28 14:32:10', '2017-06-28 14:43:07', 0, 0);
INSERT INTO `tb_type_domain` VALUES (4, '互联网+', '2017-06-28 14:32:10', '2017-06-28 14:43:08', 0, 0);
INSERT INTO `tb_type_domain` VALUES (5, '电子商务', '2017-06-28 14:32:10', '2017-06-28 14:43:09', 0, 0);
INSERT INTO `tb_type_domain` VALUES (6, '创业', '2017-06-28 14:32:10', '2017-06-28 14:43:10', 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for tb_type_source
-- ----------------------------
DROP TABLE IF EXISTS `tb_type_source`;
CREATE TABLE `tb_type_source` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL ,
  `delete_flag` tinyint(2) DEFAULT '0' COMMENT '0-正常，1-删除',
  `status` tinyint(2) DEFAULT '0' COMMENT '0-正常，1-停用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='来源类型';

-- ----------------------------
-- Records of tb_type_source
-- ----------------------------
BEGIN;
INSERT INTO `tb_type_source` VALUES (1, '行者原创', '2017-06-28 14:59:10', '2017-06-28 14:59:10', 0, 0);
INSERT INTO `tb_type_source` VALUES (2, '功虎社区', '2017-08-18 10:59:29', '2017-08-18 10:59:29', 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `login_name` varchar(64) NOT NULL COMMENT '登陆名',
  `name` varchar(64) NOT NULL COMMENT '用户名',
  `password` varchar(64) NOT NULL COMMENT '密码',
  `salt` varchar(36) DEFAULT NULL COMMENT '密码加密盐',
  `sex` tinyint(2) NOT NULL DEFAULT '0' COMMENT '性别',
  `age` tinyint(2) DEFAULT '0' COMMENT '年龄',
  `phone` varchar(20) DEFAULT NULL COMMENT '手机号',
  `user_type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '用户类别',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '用户状态',
  `organization_id` int(11) NOT NULL DEFAULT '0' COMMENT '所属机构',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDx_user_login_name` (`login_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES (1, 'admin', 'admin', '05a671c66aefea124cc08b76ea6d30bb', 'test', 0, 25, '12345678', 0, 0, 1, '2015-12-06 13:14:05');
INSERT INTO `user` VALUES (13, 'tech', 'tech', '05a671c66aefea124cc08b76ea6d30bb', 'test', 0, 25, '12345678', 1, 0, 3, '2015-10-01 13:12:07');
INSERT INTO `user` VALUES (14, 'xingzhe', 'xingzhe', '05a671c66aefea124cc08b76ea6d30bb', 'test', 0, 25, '12345678', 1, 0, 5, '2015-10-11 23:12:58');
INSERT INTO `user` VALUES (15, 'test', 'test', '05a671c66aefea124cc08b76ea6d30bb', 'test', 0, 25, '12345678', 1, 0, 6, '2015-12-06 13:13:03');
COMMIT;

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `user_id` bigint(19) NOT NULL COMMENT '用户id',
  `role_id` bigint(19) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`),
  KEY `idx_user_role_ids` (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 COMMENT='用户角色';

-- ----------------------------
-- Records of user_role
-- ----------------------------
BEGIN;
INSERT INTO `user_role` VALUES (79, 1, 1);
INSERT INTO `user_role` VALUES (80, 1, 2);
INSERT INTO `user_role` VALUES (81, 1, 7);
INSERT INTO `user_role` VALUES (66, 13, 2);
INSERT INTO `user_role` VALUES (67, 14, 7);
INSERT INTO `user_role` VALUES (68, 15, 8);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;

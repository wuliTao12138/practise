package com.xingzhe.controller;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.xingzhe.model.TbArticle;
import com.xingzhe.model.TbOpenClass;
import com.xingzhe.service.ITbArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.xingzhe.commons.result.PageInfo;
import com.xingzhe.model.TbSlideshow;
import com.xingzhe.service.ITbSlideshowService;
import com.xingzhe.commons.base.BaseController;

/**
 * <p>
 * 轮播图 前端控制器
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
@Controller
@RequestMapping("/tbSlideshow")
public class TbSlideshowController extends BaseController {

    @Autowired private ITbSlideshowService tbSlideshowService;
    @Autowired private ITbArticleService tbArticleService;
    @GetMapping("/manager")
    public String manager() {
        return "admin/tbSlideshow/tbSlideshowList";
    }
    
    @PostMapping("/dataGrid")
    @ResponseBody
    public PageInfo dataGrid(TbSlideshow tbSlideshow, Integer page, Integer rows, String sort,String order,String q) {
        PageInfo pageInfo = new PageInfo(page, rows, sort, order);
        //只查找未假删除
        tbSlideshow.setDeleteFlag(0);
        EntityWrapper<TbSlideshow> ew = new EntityWrapper<TbSlideshow>(tbSlideshow);
        if(q!=null){
            ew.like("name",q);
        }
        Page<TbSlideshow> pages = getPage(pageInfo);
        pages = tbSlideshowService.selectPage(pages, ew);
        pageInfo.setRows(pages.getRecords());
        pageInfo.setTotal(pages.getTotal());
        return pageInfo;
    }
    
    /**
     * 添加页面
     * @return
     */
    @GetMapping("/addPage")
    public String addPage() {
        return "admin/tbSlideshow/tbSlideshowAdd";
    }
    
    /**
     * 添加
     * @param 
     * @return
     */
    @PostMapping("/add")
    @ResponseBody
    public Object add(@Valid TbSlideshow tbSlideshow) {
        adapterForArticle(tbSlideshow);
        tbSlideshow.setCreateTime(new Date());
        tbSlideshow.setUpdateTime(new Date());
        tbSlideshow.setDeleteFlag(0);
        boolean b = tbSlideshowService.insert(tbSlideshow);
        if (b) {
            return renderSuccess("添加成功！");
        } else {
            return renderError("添加失败！");
        }
    }

    private void adapterForArticle(@Valid TbSlideshow tbSlideshow) {
        if(tbSlideshow.getIsArticle()==0)
        {
           TbArticle tbArticle =  tbArticleService.selectById(tbSlideshow.getArticleId());
           if(tbArticle!=null){
               tbSlideshow.setName(tbArticle.getTitle());
               tbSlideshow.setImgPath(tbArticle.getTitleImg());
               tbSlideshow.setImgLink("/front/activitydetatils.html?id="+tbArticle.getId());
           }
        }
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @PostMapping("/delete")
    @ResponseBody
    public Object delete(Long id) {
        TbSlideshow tbSlideshow = new TbSlideshow();
        tbSlideshow.setId(id);
        tbSlideshow.setUpdateTime(new Date());
        tbSlideshow.setDeleteFlag(1);
        boolean b = tbSlideshowService.updateById(tbSlideshow);
        if (b) {
            return renderSuccess("删除成功！");
        } else {
            return renderError("删除失败！");
        }
    }
    /**
     * 批量删除
     * @param strIds
     * @return
     */
    @PostMapping("/batchDelete")
    @ResponseBody
    public Object batchDelete(String strIds) {
        String[] ids = strIds.split(",");
        List<TbSlideshow> list = new ArrayList<TbSlideshow>();
        for (String temp : ids){
            TbSlideshow entity = new TbSlideshow();
            entity.setId(Long.parseLong(temp));
            entity.setUpdateTime(new Date());
            entity.setDeleteFlag(1);
            list.add(entity);
        }
        boolean b = tbSlideshowService.updateBatchById(list);
        if (b) {
            return renderSuccess("删除成功！");
        } else {
            return renderError("删除失败！");
        }
    }
    /**
     * 编辑
     * @param model
     * @param id
     * @return
     */
    @GetMapping("/editPage")
    public String editPage(Model model, Long id) {
        TbSlideshow tbSlideshow = tbSlideshowService.selectById(id);
        model.addAttribute("tbSlideshow", tbSlideshow);
        return "admin/tbSlideshow/tbSlideshowEdit";
    }
    
    /**
     * 编辑
     * @param 
     * @return
     */
    @PostMapping("/edit")
    @ResponseBody
    public Object edit(@Valid TbSlideshow tbSlideshow) {
        adapterForArticle(tbSlideshow);
        tbSlideshow.setUpdateTime(new Date());
        boolean b = tbSlideshowService.updateById(tbSlideshow);
        if (b) {
            return renderSuccess("编辑成功！");
        } else {
            return renderError("编辑失败！");
        }
    }
}

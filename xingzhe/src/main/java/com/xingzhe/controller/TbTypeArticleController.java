package com.xingzhe.controller;

import javax.validation.Valid;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.xingzhe.commons.result.PageInfo;
import com.xingzhe.model.TbTypeArticle;
import com.xingzhe.service.ITbTypeArticleService;
import com.xingzhe.commons.base.BaseController;

/**
 * <p>
 * 信息类型 前端控制器
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
@Controller
@RequestMapping("/tbTypeArticle")
public class TbTypeArticleController extends BaseController {

    @Autowired private ITbTypeArticleService tbTypeArticleService;
    
    @GetMapping("/manager")
    public String manager() {
        return "admin/tbTypeArticle/tbTypeArticleList";
    }
    
    @PostMapping("/dataGrid")
    @ResponseBody
    public PageInfo dataGrid(TbTypeArticle tbTypeArticle, Integer page, Integer rows, String sort,String order) {
        PageInfo pageInfo = new PageInfo(page, rows, sort, order);
        EntityWrapper<TbTypeArticle> ew = new EntityWrapper<TbTypeArticle>(tbTypeArticle);
        Page<TbTypeArticle> pages = getPage(pageInfo);
        pages = tbTypeArticleService.selectPage(pages, ew);
        pageInfo.setRows(pages.getRecords());
        pageInfo.setTotal(pages.getTotal());
        return pageInfo;
    }
    @PostMapping("/dataAll")
    @ResponseBody
    public Object dataAll(TbTypeArticle tbTypeArticle, Integer page, Integer rows, String sort,String order) {
        PageInfo pageInfo = new PageInfo(page, rows, sort, order);
        EntityWrapper<TbTypeArticle> ew = new EntityWrapper<TbTypeArticle>(tbTypeArticle);
        Page<TbTypeArticle> pages = getPage(pageInfo);
        pages = tbTypeArticleService.selectPage(pages, ew);
        pageInfo.setRows(pages.getRecords());
        pageInfo.setTotal(pages.getTotal());
        return pages.getRecords();
    }
    
    /**
     * 添加页面
     * @return
     */
    @GetMapping("/addPage")
    public String addPage() {
        return "admin/tbTypeArticle/tbTypeArticleAdd";
    }
    
    /**
     * 添加
     * @param 
     * @return
     */
    @PostMapping("/add")
    @ResponseBody
    public Object add(@Valid TbTypeArticle tbTypeArticle) {
        tbTypeArticle.setCreateTime(new Date());
        tbTypeArticle.setUpdateTime(new Date());
        tbTypeArticle.setDeleteFlag(0);
        boolean b = tbTypeArticleService.insert(tbTypeArticle);
        if (b) {
            return renderSuccess("添加成功！");
        } else {
            return renderError("添加失败！");
        }
    }
    
    /**
     * 删除
     * @param id
     * @return
     */
    @PostMapping("/delete")
    @ResponseBody
    public Object delete(Long id) {
        TbTypeArticle tbTypeArticle = new TbTypeArticle();
        tbTypeArticle.setId(id);
        tbTypeArticle.setUpdateTime(new Date());
        tbTypeArticle.setDeleteFlag(1);
        boolean b = tbTypeArticleService.updateById(tbTypeArticle);
        if (b) {
            return renderSuccess("删除成功！");
        } else {
            return renderError("删除失败！");
        }
    }
    
    /**
     * 编辑
     * @param model
     * @param id
     * @return
     */
    @GetMapping("/editPage")
    public String editPage(Model model, Long id) {
        TbTypeArticle tbTypeArticle = tbTypeArticleService.selectById(id);
        model.addAttribute("tbTypeArticle", tbTypeArticle);
        return "admin/tbTypeArticle/tbTypeArticleEdit";
    }
    
    /**
     * 编辑
     * @param 
     * @return
     */
    @PostMapping("/edit")
    @ResponseBody
    public Object edit(@Valid TbTypeArticle tbTypeArticle) {
        tbTypeArticle.setUpdateTime(new Date());
        boolean b = tbTypeArticleService.updateById(tbTypeArticle);
        if (b) {
            return renderSuccess("编辑成功！");
        } else {
            return renderError("编辑失败！");
        }
    }
}

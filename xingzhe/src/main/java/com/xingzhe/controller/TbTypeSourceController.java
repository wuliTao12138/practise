package com.xingzhe.controller;

import javax.validation.Valid;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.xingzhe.commons.result.PageInfo;
import com.xingzhe.model.TbTypeSource;
import com.xingzhe.service.ITbTypeSourceService;
import com.xingzhe.commons.base.BaseController;

/**
 * <p>
 * 来源类型 前端控制器
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
@Controller
@RequestMapping("/tbTypeSource")
public class TbTypeSourceController extends BaseController {

    @Autowired private ITbTypeSourceService tbTypeSourceService;
    
    @GetMapping("/manager")
    public String manager() {
        return "admin/tbTypeSource/tbTypeSourceList";
    }
    
    @PostMapping("/dataGrid")
    @ResponseBody
    public PageInfo dataGrid(TbTypeSource tbTypeSource, Integer page, Integer rows, String sort,String order) {
        PageInfo pageInfo = new PageInfo(page, rows, sort, order);
        EntityWrapper<TbTypeSource> ew = new EntityWrapper<TbTypeSource>(tbTypeSource);
        Page<TbTypeSource> pages = getPage(pageInfo);
        pages = tbTypeSourceService.selectPage(pages, ew);
        pageInfo.setRows(pages.getRecords());
        pageInfo.setTotal(pages.getTotal());
        return pageInfo;
    }
    @PostMapping("/dataAll")
    @ResponseBody
    public Object dataAll(TbTypeSource tbTypeSource, Integer page, Integer rows, String sort,String order) {
        PageInfo pageInfo = new PageInfo(page, rows, sort, order);
        EntityWrapper<TbTypeSource> ew = new EntityWrapper<TbTypeSource>(tbTypeSource);
        Page<TbTypeSource> pages = getPage(pageInfo);
        pages = tbTypeSourceService.selectPage(pages, ew);
        pageInfo.setRows(pages.getRecords());
        pageInfo.setTotal(pages.getTotal());
        return pages.getRecords();
    }
    /**
     * 添加页面
     * @return
     */
    @GetMapping("/addPage")
    public String addPage() {
        return "admin/tbTypeSource/tbTypeSourceAdd";
    }
    
    /**
     * 添加
     * @param 
     * @return
     */
    @PostMapping("/add")
    @ResponseBody
    public Object add(@Valid TbTypeSource tbTypeSource) {
        tbTypeSource.setCreateTime(new Date());
        tbTypeSource.setUpdateTime(new Date());
        tbTypeSource.setDeleteFlag(0);
        boolean b = tbTypeSourceService.insert(tbTypeSource);
        if (b) {
            return renderSuccess("添加成功！");
        } else {
            return renderError("添加失败！");
        }
    }
    
    /**
     * 删除
     * @param id
     * @return
     */
    @PostMapping("/delete")
    @ResponseBody
    public Object delete(Long id) {
        TbTypeSource tbTypeSource = new TbTypeSource();
        tbTypeSource.setId(id);
        tbTypeSource.setUpdateTime(new Date());
        tbTypeSource.setDeleteFlag(1);
        boolean b = tbTypeSourceService.updateById(tbTypeSource);
        if (b) {
            return renderSuccess("删除成功！");
        } else {
            return renderError("删除失败！");
        }
    }
    
    /**
     * 编辑
     * @param model
     * @param id
     * @return
     */
    @GetMapping("/editPage")
    public String editPage(Model model, Long id) {
        TbTypeSource tbTypeSource = tbTypeSourceService.selectById(id);
        model.addAttribute("tbTypeSource", tbTypeSource);
        return "admin/tbTypeSource/tbTypeSourceEdit";
    }
    
    /**
     * 编辑
     * @param 
     * @return
     */
    @PostMapping("/edit")
    @ResponseBody
    public Object edit(@Valid TbTypeSource tbTypeSource) {
        tbTypeSource.setUpdateTime(new Date());
        boolean b = tbTypeSourceService.updateById(tbTypeSource);
        if (b) {
            return renderSuccess("编辑成功！");
        } else {
            return renderError("编辑失败！");
        }
    }
}

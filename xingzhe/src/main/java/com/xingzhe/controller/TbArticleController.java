package com.xingzhe.controller;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.xingzhe.model.TbArticle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.xingzhe.commons.result.PageInfo;
import com.xingzhe.model.TbArticle;
import com.xingzhe.service.ITbArticleService;
import com.xingzhe.commons.base.BaseController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
@Controller
@RequestMapping("/tbArticle")
public class TbArticleController extends BaseController {

    @Autowired private ITbArticleService tbArticleService;
    
    @GetMapping("/manager")
    public String manager() {
        return "admin/tbArticle/tbArticleList";
    }
    
    @PostMapping("/dataGrid")
    @ResponseBody
    public PageInfo dataGrid(TbArticle tbArticle, Integer page, Integer rows, String sort,String order,String q) {
        PageInfo pageInfo = new PageInfo(page, rows, sort, order);
        tbArticle.setDeleteFlag(0);
        EntityWrapper<TbArticle> ew = new EntityWrapper<TbArticle>(tbArticle);
        if(q!=null){
            ew.like("title",q);
        }
        Page<TbArticle> pages = getPage(pageInfo);
        pages = tbArticleService.selectPage(pages, ew);
        pageInfo.setRows(pages.getRecords());
        pageInfo.setTotal(pages.getTotal());
        return pageInfo;
    }
    @PostMapping("/dataAll")
    @ResponseBody
    public Object dataAll(TbArticle tbArticle, Integer page, Integer rows, String sort,String order,String q) {
        tbArticle.setDeleteFlag(0);
        EntityWrapper<TbArticle> ew = new EntityWrapper<TbArticle>(tbArticle);
        ew.like("title",q);
        List<TbArticle> list = tbArticleService.selectList(ew);
        return list;
    }
    /**
     * 添加页面
     * @return
     */
    @GetMapping("/addPage")
    public String addPage() {
        return "admin/tbArticle/tbArticleAdd";
    }
    
    /**
     * 添加
     * @param 
     * @return
     */
    @PostMapping("/add")
    @ResponseBody
    public Object add(@Valid TbArticle tbArticle) {
        tbArticle.setCreateTime(new Date());
        tbArticle.setUpdateTime(new Date());
        tbArticle.setDeleteFlag(0);
        boolean b = tbArticleService.insert(tbArticle);
        if (b) {
            return renderSuccess("添加成功！");
        } else {
            return renderError("添加失败！");
        }
    }
    
    /**
     * 删除
     * @param id
     * @return
     */
    @PostMapping("/delete")
    @ResponseBody
    public Object delete(Long id) {
        TbArticle tbArticle = new TbArticle();
        tbArticle.setId(id);
        tbArticle.setUpdateTime(new Date());
        tbArticle.setDeleteFlag(1);
        boolean b = tbArticleService.updateById(tbArticle);
        if (b) {
            return renderSuccess("删除成功！");
        } else {
            return renderError("删除失败！");
        }
    }
    /**
     * 批量删除
     * @param strIds
     * @return
     */
    @PostMapping("/batchDelete")
    @ResponseBody
    public Object batchDelete(String strIds) {
        String[] ids = strIds.split(",");
        List<TbArticle> list = new ArrayList<TbArticle>();
        for (String temp : ids){
            TbArticle entity = new TbArticle();
            entity.setId(Long.parseLong(temp));
            entity.setUpdateTime(new Date());
            entity.setDeleteFlag(1);
            list.add(entity);
        }
        boolean b = tbArticleService.updateBatchById(list);
        if (b) {
            return renderSuccess("删除成功！");
        } else {
            return renderError("删除失败！");
        }
    }
    
    /**
     * 编辑
     * @param model
     * @param id
     * @return
     */
    @GetMapping("/editPage")
    public String editPage(Model model, Long id) {
        TbArticle tbArticle = tbArticleService.selectById(id);
        model.addAttribute("tbArticle", tbArticle);
        return "admin/tbArticle/tbArticleEdit";
    }
    
    /**
     * 编辑
     * @param 
     * @return
     */
    @PostMapping("/edit")
    @ResponseBody
    public Object edit(@Valid TbArticle tbArticle) {
        tbArticle.setUpdateTime(new Date());
        boolean b = tbArticleService.updateById(tbArticle);
        if (b) {
            return renderSuccess("编辑成功！");
        } else {
            return renderError("编辑失败！");
        }
    }
}

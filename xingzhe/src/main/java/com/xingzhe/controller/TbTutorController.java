package com.xingzhe.controller;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Condition;
import com.xingzhe.commons.utils.PingYinUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.xingzhe.commons.result.PageInfo;
import com.xingzhe.model.TbTutor;
import com.xingzhe.service.ITbTutorService;
import com.xingzhe.commons.base.BaseController;

/**
 * <p>
 * 导师 前端控制器
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
@Controller
@RequestMapping("/tbTutor")
public class TbTutorController extends BaseController {

    @Autowired private ITbTutorService tbTutorService;
    
    @GetMapping("/manager")
    public String manager() {
        return "admin/tbTutor/tbTutorList";
    }
    
    @PostMapping("/dataGrid")
    @ResponseBody
    public PageInfo dataGrid(TbTutor tbTutor, Integer page, Integer rows, String sort,String order,String q,Integer[] domainTypes) {

        PageInfo pageInfo = new PageInfo(page, rows, sort, order);
        tbTutor.setDeleteFlag(0);
        EntityWrapper<TbTutor> ew = new EntityWrapper<TbTutor>(tbTutor);
        if(q!=null){
            q = "%"+q+"%";
            ew.andNew("name like {0} or company like {1} or position like {2} or permanent_land like {3}",q,q,q,q);
        }
        if(domainTypes!=null&&domainTypes.length>0){
            for (int i = 0; i < domainTypes.length; i++) {
                ew.like("domain_type",domainTypes[i].toString());
            }
        }
        Page<TbTutor> pages = getPage(pageInfo);
        pages = tbTutorService.selectPage(pages, ew);
        pageInfo.setRows(pages.getRecords());
        pageInfo.setTotal(pages.getTotal());
        return pageInfo;
    }

    private String getStrDomainType(Integer[] domainTypes) {
        String temp = "";
        if(domainTypes!=null&&domainTypes.length>0){
            for (int i = 0; i <domainTypes.length ; i++) {
                if(i==domainTypes.length-1){
                    temp += domainTypes[i];
                }else{
                    temp += domainTypes[i]+",";
                }
            }
        }
        return  temp;
    }

    @PostMapping("/dataAll")
    @ResponseBody
    public Object dataAll(TbTutor tbTutor, Integer page, Integer rows, String sort,String order,String q) {
        page = 1;
        rows = 100;
        sort = "id";
        order = "asc";
        PageInfo pageInfo = new PageInfo(page, rows, sort, order);
        EntityWrapper<TbTutor> ew = new EntityWrapper<TbTutor>(tbTutor);
        ew.like("name", q).or().like("company", q);
        Page<TbTutor> pages = getPage(pageInfo);
        pages = tbTutorService.selectPage(pages, ew);
        pageInfo.setRows(pages.getRecords());
        pageInfo.setTotal(pages.getTotal());
        return pages.getRecords();
    }
    /**
     * 添加页面
     * @return
     */
    @GetMapping("/addPage")
    public String addPage() {
        return "admin/tbTutor/tbTutorAdd";
    }
    
    /**
     * 添加
     * @param 
     * @return
     */
    @PostMapping("/add")
    @ResponseBody
    public Object add(@Valid TbTutor tbTutor,Integer[] domainTypes) {

        String temp = getStrDomainType(domainTypes);
        if(StringUtils.isEmpty(temp)){
            tbTutor.setDomainType(temp);
        }
        tbTutor.setNamePinyin(PingYinUtil.getPingYin(tbTutor.getName()));
        tbTutor.setNameInitial(PingYinUtil.getFirstSpell(tbTutor.getName().substring(0,1)));
        tbTutor.setCreateTime(new Date());
        tbTutor.setUpdateTime(new Date());
        tbTutor.setDeleteFlag(0);
        boolean b = tbTutorService.insert(tbTutor);
        if (b) {
            return renderSuccess("添加成功！");
        } else {
            return renderError("添加失败！");
        }
    }
    
    /**
     * 删除
     * @param id
     * @return
     */
    @PostMapping("/delete")
    @ResponseBody
    public Object delete(Long id) {
        TbTutor tbTutor = new TbTutor();
        tbTutor.setId(id);
        tbTutor.setUpdateTime(new Date());
        tbTutor.setDeleteFlag(1);
        boolean b = tbTutorService.updateById(tbTutor);
        if (b) {
            return renderSuccess("删除成功！");
        } else {
            return renderError("删除失败！");
        }
    }
    /**
     * 批量删除
     * @param strIds
     * @return
     */
    @PostMapping("/batchDelete")
    @ResponseBody
    public Object batchDelete(String strIds) {
        String[] ids = strIds.split(",");
        List<TbTutor> list = new ArrayList<TbTutor>();
        for (String temp : ids){
            TbTutor tbTutor = new TbTutor();
            tbTutor.setId(Long.parseLong(temp));
            tbTutor.setUpdateTime(new Date());
            tbTutor.setDeleteFlag(1);
            list.add(tbTutor);
        }
        boolean b = tbTutorService.updateBatchById(list);
        if (b) {
            return renderSuccess("删除成功！");
        } else {
            return renderError("删除失败！");
        }
    }
    
    /**
     * 编辑
     * @param model
     * @param id
     * @return
     */
    @GetMapping("/editPage")
    public String editPage(Model model, Long id) {
        TbTutor tbTutor = tbTutorService.selectById(id);
        model.addAttribute("tbTutor", tbTutor);
        return "admin/tbTutor/tbTutorEdit";
    }
    
    /**
     * 编辑
     * @param 
     * @return
     */
    @PostMapping("/edit")
    @ResponseBody
    public Object edit(@Valid TbTutor tbTutor,Integer[] domainTypes) {
        String temp = getStrDomainType(domainTypes);
        if(temp!=""){
            tbTutor.setDomainType(temp);
        }
        tbTutor.setNamePinyin(PingYinUtil.getPingYin(tbTutor.getName()));
        tbTutor.setNameInitial(PingYinUtil.getFirstSpell(tbTutor.getName()).substring(0,1));
        tbTutor.setUpdateTime(new Date());
        boolean b = tbTutorService.updateById(tbTutor);
        if (b) {
            return renderSuccess("编辑成功！");
        } else {
            return renderError("编辑失败！");
        }
    }
}

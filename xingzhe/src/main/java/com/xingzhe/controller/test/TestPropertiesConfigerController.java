package com.xingzhe.controller.test;

import com.xingzhe.commons.scan.PropertyConfigurer;
import com.xingzhe.commons.utils.PropertyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by zhult on 2017/7/13.
 */
@RestController
@RequestMapping("test")
public class TestPropertiesConfigerController {
    @RequestMapping("pc")
    public String pc(){
        System.out.println("aaaaaaaa:"+PropertyUtil.getProperty("db.master.url"));
        System.out.println("bbbbb:"+(PropertyUtil.getProperty("aaa")==null));
        System.out.println("ccccc:"+(PropertyUtil.getProperty("bbb")==null));
        return  "test";
    }
    @RequestMapping("request")
    public String request(HttpServletRequest request){
        System.out.println(request.getContextPath());
        System.out.println(request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+"/"  );

        return "test";
    }
}

package com.xingzhe.controller.test;

import com.xingzhe.commons.utils.GetUrlPic;
import com.xingzhe.commons.utils.HttpClientUtil;
import com.xingzhe.commons.utils.HttpUtil;
import com.xingzhe.commons.utils.PingYinUtil;
import com.xingzhe.model.TbTutor;
import com.xingzhe.service.ITbTutorService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 爬取数据，每次重新启动，更改相应类型及label
 */
@RestController
public class DataController {
	@Autowired
	ITbTutorService tbTutorService;
	@RequestMapping("test")
	public String test(Integer domainType,String x,Integer page) throws MalformedURLException, ProtocolException, FileNotFoundException, IOException, Exception{
		String picPre ="http://www.gonghoo.com/expert/";
		if(x==null) return "test fail";//0.6330236023863531
		if(domainType==null) return "test fail";
		if(page==null) return "test fail";
        String label = "";
        switch (domainType.intValue()){
            case 1:
                label = "%25E4%25BA%2592%25E8%2581%2594%25E7%25BD%2591";
                break;
            case 2:
                label = "%25E9%2587%2591%25E8%259E%258D";
                break;
            case 3:
                label = "%25E6%258A%2595%25E8%259E%258D%25E8%25B5%2584";
                break;
            case 4:
                label = "%25E4%25BA%2592%25E8%2581%2594%25E7%25BD%2591%252B";
                break;
            case 5:
                label = "%25E7%2594%25B5%25E5%25AD%2590%25E5%2595%2586%25E5%258A%25A1";
                break;
            case 6:
                label = "%25E5%2588%259B%25E4%25B8%259A";
                break;
        }


		String url = "http://www.gonghoo.com/Expert/queryDx.action?i="+x+"&pageNo="+page+"&label="+label+"&name=&py=\n";
/**
 * 6-创业 %25E5%2588%259B%25E4%25B8%259A
 * 4-互联网+ %25E4%25BA%2592%25E8%2581%2594%25E7%25BD%2591%252B
 * 1-互联网 %25E4%25BA%2592%25E8%2581%2594%25E7%25BD%2591
 * 2-金融 %25E9%2587%2591%25E8%259E%258D
 * 3-投融资 %25E6%258A%2595%25E8%259E%258D%25E8%25B5%2584
 * 5-电子商务 %25E7%2594%25B5%25E5%25AD%2590%25E5%2595%2586%25E5%258A%25A1
 */


		Map<String,String> parames = new HashMap<String, String>();
		parames.put("test","test");
		String s = HttpUtil.http(url, parames);
		System.out.println(s);
		JSONObject jsonObject =JSONObject.fromObject(s);
		int totalRecords = jsonObject.getInt("totalRecords");
		int totalPage = totalRecords%24==0?totalRecords/24:totalRecords/24+1;
		totalPage = 1;

		for(int i =1;i<=totalPage;i++){
			String targetURL  = "http://www.gonghoo.com/Expert/queryDx.action?i=0.6330236023863531&pageNo="+page+"&label="+label+"&name=&py=\n";
			String target = HttpUtil.http(targetURL, parames);
			System.out.println(target);
			JSONObject targetJSON =JSONObject.fromObject(s);
			JSONArray jsonArray = targetJSON.getJSONArray("datas");
			JSONObject row = null;
			for (int j= 0; j < jsonArray.size(); j++) {
				row = jsonArray.getJSONObject(j);
				String  ss = row.getString("logo1");
				System.out.println(ss);
				String img = GetUrlPic.getImage(picPre+ss);
				TbTutor tbTutor = new TbTutor();
                tbTutor.setDomainType(domainType+"");
				tbTutor.setName(row.getString("name"));
				tbTutor.setNamePinyin(PingYinUtil.getPingYin(tbTutor.getName()));
				tbTutor.setNameInitial(PingYinUtil.getFirstSpell(tbTutor.getName().substring(0,1)));
				tbTutor.setCompany(row.getString("companyName"));
				tbTutor.setPosition(row.getString("job"));
				tbTutor.setHeadImg("/upload/image/20170707/"+img);
                String urlDetail = "http://www.gonghoo.com/Expert/main_dxdetail.action?id=";
                String id = row.getString("id");
                urlDetail = urlDetail+id;
                String userAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.86 Safari/537.36";
                String web = HttpClientUtil.http(urlDetail, userAgent);
                Document doc = Jsoup.parse(web);
                Elements elements = doc.getElementsByClass("zhiku_detail_place");
               String  location = elements.get(0).child(2).html();
               tbTutor.setPermanentLand(location);
               String intro = doc.getElementsByClass("zhiku_introduce_text").get(0).html();
               tbTutor.setIntro(intro);
               tbTutor.setShortIntro(tbTutor.getCompany()+tbTutor.getPosition());
               tbTutor.setCreateTime(new Date());
                tbTutor.setUpdateTime(new Date());
                tbTutor.setIsMaster(1);
				tbTutorService.insert(tbTutor);
			}
		}
		 
			 return "test";
	}
	public static void main(String[] args) throws MalformedURLException, ProtocolException, FileNotFoundException, IOException, Exception {
		 Map<String,String> parames = new HashMap<String, String>();  
	        parames.put("cid", "2");  //极致美护
	        parames.put("pageNo", "0"); 
	        parames.put("pageSize", "40");
		String s = HttpUtil.http("http://www.bqu.com/m-Wap/Search/GetProduct", parames);
		s = s.substring(0,s.indexOf("<script"));
		System.out.println(s);
		JSONObject jsonObject =JSONObject.fromObject(s); 
		 JSONArray jsonArray = jsonObject.getJSONArray("model");  
		 JSONObject row = null;  
		  row = jsonArray.getJSONObject(0);
			 String  ss = row.getString("ProductName");
			 System.out.println(ss);
			 ss = row.getString("ImgUrl");
			 System.out.println(ss);
			 GetUrlPic.getImage(ss);
		  for (int i = 0; i < jsonArray.size(); i++) {  
//		   row = jsonArray.getJSONObject(i);
//		   
//		 String  ss = row.getString("ProductName");
//		 System.out.println(ss);
		  
		   
		  } 
	}
	/*@RequestMapping("")
	public String index(){
		User user = userMapper.selectByPrimaryKey(1);
		System.out.println(user.getPassword());
		 Map<String,String> parames = new HashMap<String, String>();  
	        parames.put("categoryId", "5");  //极致美护
	        parames.put("type", "1"); 
//		 parames.put("categoryId", "181");  
//	        parames.put("type", "0");
	        parames.put("pageIndex", "2"); 
	        parames.put("pageSize", "40");
		String s = HttpUtil.http("http://www.bqu.com/m-Wap/Search/GetProduct", parames);
		System.out.println(s);
		s = s.substring(0,s.indexOf("<script"));
		System.out.println(s);
		JSONObject jsonObject =JSONObject.parseObject(s);  
		   
		System.out.println("success:"+jsonObject.get("success"));  
		    
		    
		  JSONArray jsonArray = jsonObject.getJSONArray("data");  
		  JSONObject row = null;  
		  for (int i = 0; i < jsonArray.size(); i++) {  
		   row = jsonArray.getJSONObject(i);
		   
		   Product p = new Product();
		   p.setBackImg(row.get("BackImg").toString());
		   p.setProductName(row.get("ProductName").toString());
		   p.setSalePrice(row.getDouble("SalePrice"));
		   p.setMarketPrice(row.getDouble("MarketPrice"));
		   p.setShortDescription(row.getString("ShortDescription"));
		   p.setProductId(row.getInt("ProductId"));
		   System.out.println(p.getBackImg());
		   System.out.println(p.getProductName());
		   System.out.println(p.getSalePrice());
		   System.out.println(p.getMarketPrice());
		   System.out.println(p.getShortDescription());
		   Map<String,String> parames1 = new HashMap<String, String>();  
		   parames1.put("Id",p.getProductId().toString());  
		   String s1 = HttpUtil.http("http://www.bqu.com/m-Wap/Product/LoadProductDetail", parames1);
		   s1 = s1.substring(0,s1.indexOf("<script"));
		   System.out.println(s1);
		   JSONObject jsonObject2 = JSONObject.fromObject(s1);
		   String desc = jsonObject2.getString("ProductDescription");
		   Goods goods = new Goods();
		   goods.setName(p.getProductName());
		   goods.setImg(p.getBackImg());
		   goods.setSalePrice(p.getSalePrice());
		   goods.setMarketPrice(p.getMarketPrice());
		   goods.setShortDescription(p.getShortDescription());
		   goods.setDescription(desc);
		   int hotSell = 0;
		   int preSell = 0;
		   int newItem = 0;
		   int weekHot = 0;
		   if(i<=10)
		   {
			   hotSell = 1;
		   }
		   if(i>10&&i<=20)
		   {
			   preSell = 1;
		   }
		   if(i>20&&i<=30)
		   {
			   newItem = 1;
		   }
		   if(i>30&&i<40)
		   {
			  weekHot = 1;
		   }
		   goods.setHotSell(hotSell);
		   goods.setPreSell(preSell);
		   goods.setNewItem(newItem);
		   goods.setWeekHot(weekHot);
		   goods.setType(GoodsType.type1.getKey());
//		   goods.setType(GoodsType.type2.getKey());//个性配饰
		   goodsMapper.insert(goods);
		   
		  } 

		return "index";
	}*/
}

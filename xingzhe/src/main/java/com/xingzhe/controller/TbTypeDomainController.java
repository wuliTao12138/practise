package com.xingzhe.controller;

import javax.validation.Valid;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.xingzhe.commons.result.PageInfo;
import com.xingzhe.model.TbTypeDomain;
import com.xingzhe.service.ITbTypeDomainService;
import com.xingzhe.commons.base.BaseController;

/**
 * <p>
 * 领域类型 前端控制器
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
@Controller
@RequestMapping("/tbTypeDomain")
public class TbTypeDomainController extends BaseController {

    @Autowired private ITbTypeDomainService tbTypeDomainService;
    
    @GetMapping("/manager")
    public String manager() {
        return "admin/tbTypeDomain/tbTypeDomainList";
    }
    
    @PostMapping("/dataGrid")
    @ResponseBody
    public PageInfo dataGrid(TbTypeDomain tbTypeDomain, Integer page, Integer rows, String sort,String order) {
        PageInfo pageInfo = new PageInfo(page, rows, sort, order);
        EntityWrapper<TbTypeDomain> ew = new EntityWrapper<TbTypeDomain>(tbTypeDomain);
        Page<TbTypeDomain> pages = getPage(pageInfo);
        pages = tbTypeDomainService.selectPage(pages, ew);
        pageInfo.setRows(pages.getRecords());
        pageInfo.setTotal(pages.getTotal());
        return pageInfo;
    }

    @PostMapping("/dataAll")
    @ResponseBody
    public Object dataAll(TbTypeDomain tbTypeDomain, Integer page, Integer rows, String sort,String order) {
        EntityWrapper<TbTypeDomain> ew = new EntityWrapper<TbTypeDomain>(tbTypeDomain);
        List<TbTypeDomain> records= tbTypeDomainService.selectList(ew);
        return records;
    }
    
    /**
     * 添加页面
     * @return
     */
    @GetMapping("/addPage")
    public String addPage() {
        return "admin/tbTypeDomain/tbTypeDomainAdd";
    }
    
    /**
     * 添加
     * @param 
     * @return
     */
    @PostMapping("/add")
    @ResponseBody
    public Object add(@Valid TbTypeDomain tbTypeDomain) {
        tbTypeDomain.setCreateTime(new Date());
        tbTypeDomain.setUpdateTime(new Date());
        tbTypeDomain.setDeleteFlag(0);
        boolean b = tbTypeDomainService.insert(tbTypeDomain);
        if (b) {
            return renderSuccess("添加成功！");
        } else {
            return renderError("添加失败！");
        }
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @PostMapping("/delete")
    @ResponseBody
    public Object delete(Long id) {
        TbTypeDomain tbTypeDomain = new TbTypeDomain();
        tbTypeDomain.setId(id);
        tbTypeDomain.setUpdateTime(new Date());
        tbTypeDomain.setDeleteFlag(1);
        boolean b = tbTypeDomainService.updateById(tbTypeDomain);
        if (b) {
            return renderSuccess("删除成功！");
        } else {
            return renderError("删除失败！");
        }
    }
    
    /**
     * 编辑
     * @param model
     * @param id
     * @return
     */
    @GetMapping("/editPage")
    public String editPage(Model model, Long id) {
        TbTypeDomain tbTypeDomain = tbTypeDomainService.selectById(id);
        model.addAttribute("tbTypeDomain", tbTypeDomain);
        return "admin/tbTypeDomain/tbTypeDomainEdit";
    }
    
    /**
     * 编辑
     * @param 
     * @return
     */
    @PostMapping("/edit")
    @ResponseBody
    public Object edit(@Valid TbTypeDomain tbTypeDomain) {
        tbTypeDomain.setUpdateTime(new Date());
        boolean b = tbTypeDomainService.updateById(tbTypeDomain);
        if (b) {
            return renderSuccess("编辑成功！");
        } else {
            return renderError("编辑失败！");
        }
    }
}

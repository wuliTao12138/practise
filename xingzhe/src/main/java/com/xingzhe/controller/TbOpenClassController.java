package com.xingzhe.controller;

import javax.validation.Valid;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.xingzhe.commons.result.PageInfo;
import com.xingzhe.model.TbTutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xingzhe.model.TbOpenClass;
import com.xingzhe.service.ITbOpenClassService;
import com.xingzhe.commons.base.BaseController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-07-03
 */
@Controller
@RequestMapping("/tbOpenClass")
public class TbOpenClassController extends BaseController {

    @Autowired private ITbOpenClassService tbOpenClassService;
    
    @GetMapping("/manager")
    public String manager() {
        return "admin/tbOpenClass/tbOpenClassList";
    }
    
    @PostMapping("/dataGrid")
    @ResponseBody
    public PageInfo dataGrid(TbOpenClass tbOpenClass, Integer page, Integer rows, String sort, String order,String q) {
        PageInfo pageInfo = new PageInfo(page, rows, sort, order);
        EntityWrapper<TbOpenClass> ew = new EntityWrapper<TbOpenClass>(tbOpenClass);
        ew.eq("delete_flag","0");
        if(q!=null){
            ew.like("course_name",q).or().like("sponsor",q).or().like("lecturer",q);
        }
        Page<TbOpenClass> pages = getPage(pageInfo);
        pages = tbOpenClassService.selectPage(pages, ew);
        pageInfo.setRows(pages.getRecords());
        pageInfo.setTotal(pages.getTotal());
        return pageInfo;
    }
    
    /**
     * 添加页面
     * @return
     */
    @GetMapping("/addPage")
    public String addPage() {
        return "admin/tbOpenClass/tbOpenClassAdd";
    }
    
    /**
     * 添加
     * @param 
     * @return
     */
    @PostMapping("/add")
    @ResponseBody
    public Object add(@Valid TbOpenClass tbOpenClass) {
        tbOpenClass.setCreateTime(new Date());
        tbOpenClass.setUpdateTime(new Date());
        tbOpenClass.setDeleteFlag(0);
        boolean b = tbOpenClassService.insert(tbOpenClass);
        if (b) {
            return renderSuccess("添加成功！");
        } else {
            return renderError("添加失败！");
        }
    }
    
    /**
     * 删除
     * @param id
     * @return
     */
    @PostMapping("/delete")
    @ResponseBody
    public Object delete(Long id) {
        TbOpenClass tbOpenClass = new TbOpenClass();
        tbOpenClass.setId(id);
        tbOpenClass.setUpdateTime(new Date());
        tbOpenClass.setDeleteFlag(1);
        boolean b = tbOpenClassService.updateById(tbOpenClass);
        if (b) {
            return renderSuccess("删除成功！");
        } else {
            return renderError("删除失败！");
        }
    }
    /**
     * 批量删除
     * @param strIds
     * @return
     */
    @PostMapping("/batchDelete")
    @ResponseBody
    public Object batchDelete(String strIds) {
        String[] ids = strIds.split(",");
        List<TbOpenClass> list = new ArrayList<TbOpenClass>();
        for (String temp : ids){
            TbOpenClass entity = new TbOpenClass();
            entity.setId(Long.parseLong(temp));
            entity.setUpdateTime(new Date());
            entity.setDeleteFlag(1);
            list.add(entity);
        }
        boolean b = tbOpenClassService.updateBatchById(list);
        if (b) {
            return renderSuccess("删除成功！");
        } else {
            return renderError("删除失败！");
        }
    }
    
    /**
     * 编辑
     * @param model
     * @param id
     * @return
     */
    @GetMapping("/editPage")
    public String editPage(Model model, Long id) {
        TbOpenClass tbOpenClass = tbOpenClassService.selectById(id);
        model.addAttribute("tbOpenClass", tbOpenClass);
        return "admin/tbOpenClass/tbOpenClassEdit";
    }
    
    /**
     * 编辑
     * @param 
     * @return
     */
    @PostMapping("/edit")
    @ResponseBody
    public Object edit(@Valid TbOpenClass tbOpenClass) {
        tbOpenClass.setUpdateTime(new Date());
        boolean b = tbOpenClassService.updateById(tbOpenClass);
        if (b) {
            return renderSuccess("编辑成功！");
        } else {
            return renderError("编辑失败！");
        }
    }
}

package com.xingzhe.controller;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.xingzhe.commons.utils.StringUtils;
import com.xingzhe.model.TbInvitation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.xingzhe.commons.result.PageInfo;
import com.xingzhe.model.TbInvitation;
import com.xingzhe.service.ITbInvitationService;
import com.xingzhe.commons.base.BaseController;

/**
 * <p>
 * 邀请函 前端控制器
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
@Controller
@RequestMapping("/tbInvitation")
public class TbInvitationController extends BaseController {

    @Autowired private ITbInvitationService tbInvitationService;
    
    @GetMapping("/manager")
    public String manager() {
        return "admin/tbInvitation/tbInvitationList";
    }
    
    @PostMapping("/dataGrid")
    @ResponseBody
    public PageInfo dataGrid(TbInvitation tbInvitation, Integer page, Integer rows, String sort,String order,String q) {
        PageInfo pageInfo = new PageInfo(page, rows, sort, order);
        tbInvitation.setDeleteFlag(0);
        if(tbInvitation.getPhone()!=null&&tbInvitation.getPhone().trim().equals("")){
            tbInvitation.setPhone(null);
        }
        EntityWrapper<TbInvitation> ew = new EntityWrapper<TbInvitation>(tbInvitation);
        if(q!=null){
            q = "%"+q+"%";
            ew.andNew("sponsor like {0} or linkman like {1}",q,q);
        }
        ew.orderBy("handle_status",true);
        Page<TbInvitation> pages = getPage(pageInfo);
        pages = tbInvitationService.selectPage(pages, ew);
        pageInfo.setRows(pages.getRecords());
        pageInfo.setTotal(pages.getTotal());
        return pageInfo;
    }
    
    /**
     * 添加页面
     * @return
     */
    @GetMapping("/addPage")
    public String addPage() {
        return "admin/tbInvitation/tbInvitationAdd";
    }
    
    /**
     * 添加
     * @param 
     * @return
     */
    @PostMapping("/add")
    @ResponseBody
    public Object add(@Valid TbInvitation tbInvitation) {
        tbInvitation.setCreateTime(new Date());
        tbInvitation.setUpdateTime(new Date());
        tbInvitation.setDeleteFlag(0);
        boolean b = tbInvitationService.insert(tbInvitation);
        if (b) {
            return renderSuccess("添加成功！");
        } else {
            return renderError("添加失败！");
        }
    }
    
    /**
     * 删除
     * @param id
     * @return
     */
    @PostMapping("/delete")
    @ResponseBody
    public Object delete(Long id) {
        TbInvitation tbInvitation = new TbInvitation();
        tbInvitation.setId(id);
        tbInvitation.setUpdateTime(new Date());
        tbInvitation.setDeleteFlag(1);
        boolean b = tbInvitationService.updateById(tbInvitation);
        if (b) {
            return renderSuccess("删除成功！");
        } else {
            return renderError("删除失败！");
        }
    }
    /**
     * 批量删除
     * @param strIds
     * @return
     */
    @PostMapping("/batchDelete")
    @ResponseBody
    public Object batchDelete(String strIds) {
        String[] ids = strIds.split(",");
        List<TbInvitation> list = new ArrayList<TbInvitation>();
        for (String temp : ids){
            TbInvitation entity = new TbInvitation();
            entity.setId(Long.parseLong(temp));
            entity.setUpdateTime(new Date());
            entity.setDeleteFlag(1);
            list.add(entity);
        }
        boolean b = tbInvitationService.updateBatchById(list);
        if (b) {
            return renderSuccess("删除成功！");
        } else {
            return renderError("删除失败！");
        }
    }
    
    /**
     * 编辑
     * @param model
     * @param id
     * @return
     */
    @GetMapping("/editPage")
    public String editPage(Model model, Long id) {
        TbInvitation tbInvitation = tbInvitationService.selectById(id);
        model.addAttribute("tbInvitation", tbInvitation);
        return "admin/tbInvitation/tbInvitationEdit";
    }
    
    /**
     * 编辑
     * @param 
     * @return
     */
    @PostMapping("/edit")
    @ResponseBody
    public Object edit(@Valid TbInvitation tbInvitation) {
        tbInvitation.setUpdateTime(new Date());
        boolean b = tbInvitationService.updateById(tbInvitation);
        if (b) {
            return renderSuccess("编辑成功！");
        } else {
            return renderError("编辑失败！");
        }
    }
}

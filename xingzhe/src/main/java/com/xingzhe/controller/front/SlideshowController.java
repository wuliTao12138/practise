package com.xingzhe.controller.front;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.xingzhe.commons.base.BaseController;
import com.xingzhe.commons.result.PageInfo;
import com.xingzhe.commons.utils.RequestUitl;
import com.xingzhe.model.TbArticle;
import com.xingzhe.model.TbOpenClass;
import com.xingzhe.model.TbSlideshow;
import com.xingzhe.model.TbTutor;
import com.xingzhe.service.ITbArticleService;
import com.xingzhe.service.ITbSlideshowService;
import com.xingzhe.service.ITbTutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 轮播图接口
 */
@RestController
@RequestMapping("api/slideshow")
public class SlideshowController extends BaseController{
    @Autowired
    private ITbSlideshowService tbSlideshowService;
    @Autowired
    private HttpServletRequest request;
    @RequestMapping("list")
    public Object slideshow(TbSlideshow tbSlideshow, Integer page, Integer rows, String sort, String order) {
        if(page==null) page = 1;
        if(rows==null) rows = 3;
        if(sort==null) sort = "id";
        if(order==null) order = "asc";
        PageInfo pageInfo = new PageInfo(page, rows, sort, order);
        EntityWrapper<TbSlideshow> ew = new EntityWrapper<TbSlideshow>(tbSlideshow);
        ew.eq("status","0").and().eq("delete_flag","0");
        Page<TbSlideshow> pages = getPage(pageInfo);
        pages = tbSlideshowService.selectPage(pages,ew);
        for(TbSlideshow temp: pages.getRecords()){
            temp.setImgPath(RequestUitl.getDomainAndPort(request)+temp.getImgPath());
        }
        pageInfo.setRows(pages.getRecords());
        pageInfo.setTotal(pages.getTotal());
        return renderSuccess(pageInfo);
    }

}

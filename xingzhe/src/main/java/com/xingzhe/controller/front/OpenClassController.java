package com.xingzhe.controller.front;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.xingzhe.commons.base.BaseController;
import com.xingzhe.commons.result.PageInfo;
import com.xingzhe.commons.utils.RequestUitl;
import com.xingzhe.model.TbOpenClass;
import com.xingzhe.service.ITbOpenClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("api/openClass")
public class OpenClassController extends BaseController{
    @Autowired
    private ITbOpenClassService tbOpenClassService;
    @Autowired
    private HttpServletRequest request;
    @PostMapping("/list")
    @ResponseBody
    public Object list(TbOpenClass tbOpenClass, Integer page, Integer rows, String sort, String order) {
        if(page==null) page = 1;
        if(rows==null) rows = 3;
        if(sort==null) sort = "id";
        if(order==null) order = "asc";
        PageInfo pageInfo = new PageInfo(page, rows, sort, order);
        EntityWrapper<TbOpenClass> ew = new EntityWrapper<TbOpenClass>(tbOpenClass);
        ew.eq("status","0").and().eq("delete_flag","0");
        Page<TbOpenClass> pages = getPage(pageInfo);
        pages = tbOpenClassService.selectPage(pages, ew);
        for(TbOpenClass temp: pages.getRecords()){
            temp.setImgPath(RequestUitl.getDomainAndPort(request)+temp.getImgPath());
        }
        pageInfo.setRows(pages.getRecords());
        pageInfo.setTotal(pages.getTotal());
        return renderSuccess(pageInfo);
    }

    @PostMapping("/listAll")
    @ResponseBody
    public Object listAll(TbOpenClass tbOpenClass) {

        EntityWrapper<TbOpenClass> ew = new EntityWrapper<TbOpenClass>(tbOpenClass);
        ew.eq("status","0").and().eq("delete_flag","0");
        List<TbOpenClass> list = tbOpenClassService.selectList(ew);
        return renderSuccess(list);
    }

}

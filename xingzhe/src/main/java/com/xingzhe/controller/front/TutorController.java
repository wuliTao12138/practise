package com.xingzhe.controller.front;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.xingzhe.commons.base.BaseController;
import com.xingzhe.commons.result.PageInfo;
import com.xingzhe.commons.utils.ABCListUtils;
import com.xingzhe.commons.utils.RequestUitl;
import com.xingzhe.model.TbTutor;
import com.xingzhe.service.ITbTutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("api/tutor")
public class TutorController extends BaseController{
    @Autowired
    private ITbTutorService tbTutorService;
    @Autowired
    private HttpServletRequest request;

    @RequestMapping("list")
    public Object list(TbTutor tbTutor, Integer page, Integer rows, String sort,String order,Integer initialScope,Integer notTutorId,Integer[] domainTypes){
        if(sort==null) sort = "id";
        if(order==null) order = "asc";
        if(page==null) page = 1;
        if(rows==null) rows = 3;
        PageInfo pageInfo = new PageInfo(page, rows, sort, order);
        EntityWrapper<TbTutor> ew = new EntityWrapper<TbTutor>(tbTutor);
        ew.eq("status","0").and().eq("delete_flag","0");
        if(notTutorId!=null){
            ew.ne("id",notTutorId);
        }
        if(domainTypes!=null&&domainTypes.length>0){
            for (int i = 0; i < domainTypes.length; i++) {
                ew.like("domain_type",domainTypes[i].toString());
            }
        }
        ew.orderBy("sort_value",false);
        Page<TbTutor> pages = getPage(pageInfo);
        if(initialScope!=null){
            List<String > list = null;
            switch (initialScope.intValue()){
                case 1:
                    list = ABCListUtils.getAE();
                    break;
                case 2:
                    list = ABCListUtils.getFK();
                    break;
                case 3:
                    list = ABCListUtils.getLQ();
                    break;
                case  4:
                    list = ABCListUtils.getRV();
                    break;
                case 5:
                    list = ABCListUtils.getWZ();
                    break;
            }
            if(list!=null) ew.in("name_initial",list);
        }

        pages = tbTutorService.selectPage(pages, ew);
        for(TbTutor temp: pages.getRecords()){
            temp.setHeadImg(RequestUitl.getDomainAndPort(request)+temp.getHeadImg());
            temp.setHeadImgExtra(RequestUitl.getDomainAndPort(request)+temp.getHeadImgExtra());
        }
        pageInfo.setRows(pages.getRecords());
        pageInfo.setTotal(pages.getTotal());
        return renderSuccess(pageInfo);
    }
}

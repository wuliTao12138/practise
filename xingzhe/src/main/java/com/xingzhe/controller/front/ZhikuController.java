package com.xingzhe.controller.front;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.xingzhe.commons.base.BaseController;
import com.xingzhe.commons.result.PageInfo;
import com.xingzhe.commons.utils.ABCListUtils;
import com.xingzhe.commons.utils.RequestUitl;
import com.xingzhe.model.TbArticle;
import com.xingzhe.model.TbInvitation;
import com.xingzhe.model.TbTutor;
import com.xingzhe.model.TbTypeDomain;
import com.xingzhe.service.ITbArticleService;
import com.xingzhe.service.ITbInvitationService;
import com.xingzhe.service.ITbTutorService;
import com.xingzhe.service.ITbTypeDomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 智库页面
 */
@RestController
@RequestMapping("api/zhiku")
public class ZhikuController extends BaseController{
    @Autowired
    private ITbTutorService tbTutorService;
    @Autowired
    private ITbTypeDomainService tbTypeDomainService;
    @Autowired
    private ITbArticleService tbArticleService;
    @Autowired
    private ITbInvitationService tbInvitationService;
    @Autowired
    private HttpServletRequest request;

    @RequestMapping("tutorOne")
    public Object tutorOne(Integer id){
        TbTutor temp = tbTutorService.selectById(id);
        temp.setHeadImg(RequestUitl.getDomainAndPort(request)+temp.getHeadImg());
        temp.setHeadImgExtra(RequestUitl.getDomainAndPort(request)+temp.getHeadImgExtra());
        return temp;
    }
    @RequestMapping("domainAll")
    public Object domainAll(TbTypeDomain tbTypeDomain, Integer page, Integer rows, String sort,String order) {
            EntityWrapper<TbTypeDomain> ew = new EntityWrapper<TbTypeDomain>(tbTypeDomain);
            List<TbTypeDomain> records= tbTypeDomainService.selectList(ew);
            return records;
    }
    @RequestMapping("inviteMaster")
    public Object inviteMaster(@Valid TbInvitation tbInvitation) {
        tbInvitation.setCreateTime(new Date());
        tbInvitation.setUpdateTime(new Date());
        tbInvitation.setDeleteFlag(0);
        boolean b = tbInvitationService.insert(tbInvitation);
        if (b) {
            return renderSuccess("邀请已发送，请等待客服联系！");
        } else {
            return renderError("邀请发送失败！");
        }
    }

}

package com.xingzhe.controller.front;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.xingzhe.commons.base.BaseController;
import com.xingzhe.commons.result.PageInfo;
import com.xingzhe.commons.utils.RequestUitl;
import com.xingzhe.dto.ArticleDTO;
import com.xingzhe.model.TbArticle;
import com.xingzhe.model.TbOpenClass;
import com.xingzhe.service.ITbArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("api/article")
public class ArticleController extends BaseController{
    @Autowired
    private ITbArticleService tbArticleService;
    @Autowired
    private HttpServletRequest request;
    @RequestMapping("list")
    public Object list(TbArticle tbArticle, Integer page, Integer rows, String sort, String order) {
        if(page==null)page=1;
        if(rows==null)rows=3;
        if(sort==null)sort="show_time";
        if(order==null)order="desc";
        PageInfo pageInfo = new PageInfo(page, rows, sort, order);
        EntityWrapper<TbArticle> ew = new EntityWrapper<TbArticle>(tbArticle);
        ew.eq("status","0").and().eq("delete_flag","0");
        ew.orderBy("sort_value",false);
        Page<TbArticle> pages = getPage(pageInfo);
        pages = tbArticleService.selectPage(pages, ew);
        for(TbArticle temp: pages.getRecords()){
            temp.setTitleImg(RequestUitl.getDomainAndPort(request)+temp.getTitleImg());
        }
        pageInfo.setRows(pages.getRecords());
        pageInfo.setTotal(pages.getTotal());
        return renderSuccess(pageInfo);
    }
    @RequestMapping("get")
    public Object get(Integer id){
        ArticleDTO articleDTO = tbArticleService.selectDTOById(id);
        return  renderSuccess(articleDTO);
    }
}

package com.xingzhe.mapper;

import com.xingzhe.model.TbOpenClass;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-07-03
 */
public interface TbOpenClassMapper extends BaseMapper<TbOpenClass> {

}
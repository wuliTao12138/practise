package com.xingzhe.mapper;

import com.xingzhe.model.TbTypeSource;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 来源类型 Mapper 接口
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
public interface TbTypeSourceMapper extends BaseMapper<TbTypeSource> {

}
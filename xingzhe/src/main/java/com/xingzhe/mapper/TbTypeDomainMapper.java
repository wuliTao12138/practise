package com.xingzhe.mapper;

import com.xingzhe.model.TbTypeDomain;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 领域类型 Mapper 接口
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
public interface TbTypeDomainMapper extends BaseMapper<TbTypeDomain> {

}
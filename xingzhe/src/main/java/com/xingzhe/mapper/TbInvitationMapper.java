package com.xingzhe.mapper;

import com.xingzhe.model.TbInvitation;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 邀请函 Mapper 接口
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
public interface TbInvitationMapper extends BaseMapper<TbInvitation> {

}
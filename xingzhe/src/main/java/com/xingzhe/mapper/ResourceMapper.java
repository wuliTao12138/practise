package com.xingzhe.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.xingzhe.model.Resource;

/**
 *
 * Resource 表数据库控制层接口
 *
 */
public interface ResourceMapper extends BaseMapper<Resource> {

}
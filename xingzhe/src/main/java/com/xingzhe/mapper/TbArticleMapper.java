package com.xingzhe.mapper;

import com.xingzhe.model.TbArticle;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-07-06
 */
public interface TbArticleMapper extends BaseMapper<TbArticle> {

}
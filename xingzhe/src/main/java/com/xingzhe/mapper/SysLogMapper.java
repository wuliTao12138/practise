package com.xingzhe.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.xingzhe.model.SysLog;

/**
 *
 * SysLog 表数据库控制层接口
 *
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}
package com.xingzhe.mapper;

import com.xingzhe.model.TbTypeArticle;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 信息类型 Mapper 接口
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
public interface TbTypeArticleMapper extends BaseMapper<TbTypeArticle> {

}
package com.xingzhe.dto;

import lombok.Data;

import java.util.Date;

/**
 * Created by zhult on 2017/8/18.
 */
public class ArticleDTO {
    private Long id;
    /**
     * 标题
     */
    private String title;
    /**
     * 显示时间
     */
    private Date showTime;
    /**
     * 标题图片
     */
    private String titleImg;
    /**
     * 来源类型id
     */
    private Integer sourceType;
    private String sourceTypeName;
    /**
     * 简介
     */
    private String intro;
    /**
     * 信息内容
     */
    private String content;
    /**
     * 信息类型id
     */
    private Integer articleType;
    /**
     * 排序值，值越大排越前
     */
    private Integer sortValue;
    private Date createTime;
    private Date updateTime;
    /**
     * 0-正常，1-删除
     */
    private Integer deleteFlag;
    /**
     * 0-正常，1-停用
     */
    private Integer status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getShowTime() {
        return showTime;
    }

    public void setShowTime(Date showTime) {
        this.showTime = showTime;
    }

    public String getTitleImg() {
        return titleImg;
    }

    public void setTitleImg(String titleImg) {
        this.titleImg = titleImg;
    }

    public Integer getSourceType() {
        return sourceType;
    }

    public void setSourceType(Integer sourceType) {
        this.sourceType = sourceType;
    }

    public String getSourceTypeName() {
        return sourceTypeName;
    }

    public void setSourceTypeName(String sourceTypeName) {
        this.sourceTypeName = sourceTypeName;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getArticleType() {
        return articleType;
    }

    public void setArticleType(Integer articleType) {
        this.articleType = articleType;
    }

    public Integer getSortValue() {
        return sortValue;
    }

    public void setSortValue(Integer sortValue) {
        this.sortValue = sortValue;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Integer deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}

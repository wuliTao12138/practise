package com.xingzhe.service;

import com.xingzhe.model.TbSlideshow;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 轮播图 服务类
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
public interface ITbSlideshowService extends IService<TbSlideshow> {
	
}

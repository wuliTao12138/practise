package com.xingzhe.service;

import com.xingzhe.model.TbTutor;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 导师 服务类
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
public interface ITbTutorService extends IService<TbTutor> {
	
}

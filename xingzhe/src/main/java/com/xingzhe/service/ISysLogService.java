package com.xingzhe.service;

import com.baomidou.mybatisplus.service.IService;
import com.xingzhe.commons.result.PageInfo;
import com.xingzhe.model.SysLog;

/**
 *
 * SysLog 表数据服务层接口
 *
 */
public interface ISysLogService extends IService<SysLog> {

    void selectDataGrid(PageInfo pageInfo);

}
package com.xingzhe.service;

import com.xingzhe.model.TbInvitation;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 邀请函 服务类
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
public interface ITbInvitationService extends IService<TbInvitation> {
	
}

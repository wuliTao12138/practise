package com.xingzhe.service;

import com.xingzhe.model.TbTypeDomain;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 领域类型 服务类
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
public interface ITbTypeDomainService extends IService<TbTypeDomain> {
	
}

package com.xingzhe.service;

import com.xingzhe.dto.ArticleDTO;
import com.xingzhe.model.TbArticle;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
public interface ITbArticleService extends IService<TbArticle> {

    ArticleDTO selectDTOById(Integer id);
}

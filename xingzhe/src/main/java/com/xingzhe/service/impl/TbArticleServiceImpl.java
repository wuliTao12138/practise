package com.xingzhe.service.impl;

import com.xingzhe.dto.ArticleDTO;
import com.xingzhe.mapper.TbTypeArticleMapper;
import com.xingzhe.mapper.TbTypeSourceMapper;
import com.xingzhe.model.TbArticle;
import com.xingzhe.mapper.TbArticleMapper;
import com.xingzhe.model.TbTypeArticle;
import com.xingzhe.model.TbTypeSource;
import com.xingzhe.service.ITbArticleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
@Service
public class TbArticleServiceImpl extends ServiceImpl<TbArticleMapper, TbArticle> implements ITbArticleService {

    @Autowired
    TbTypeSourceMapper tbTypeSourceMapper;
    @Override
    public ArticleDTO selectDTOById(Integer id) {
        TbArticle tbArticle = selectById(id);
        ArticleDTO articleDTO = new ArticleDTO();
        BeanUtils.copyProperties(tbArticle,articleDTO);
        if(tbArticle.getArticleType()!=null){
            TbTypeSource tbTypeSource = tbTypeSourceMapper.selectById(tbArticle.getSourceType());
            if(tbTypeSource!=null){
                articleDTO.setSourceTypeName(tbTypeSource.getName());
            }
        }
        return articleDTO;
    }
}

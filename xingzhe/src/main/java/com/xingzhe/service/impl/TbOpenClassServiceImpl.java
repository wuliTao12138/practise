package com.xingzhe.service.impl;

import com.xingzhe.model.TbOpenClass;
import com.xingzhe.mapper.TbOpenClassMapper;
import com.xingzhe.service.ITbOpenClassService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-07-03
 */
@Service
public class TbOpenClassServiceImpl extends ServiceImpl<TbOpenClassMapper, TbOpenClass> implements ITbOpenClassService {
	
}

package com.xingzhe.service.impl;

import com.xingzhe.model.TbTypeSource;
import com.xingzhe.mapper.TbTypeSourceMapper;
import com.xingzhe.service.ITbTypeSourceService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 来源类型 服务实现类
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
@Service
public class TbTypeSourceServiceImpl extends ServiceImpl<TbTypeSourceMapper, TbTypeSource> implements ITbTypeSourceService {
	
}

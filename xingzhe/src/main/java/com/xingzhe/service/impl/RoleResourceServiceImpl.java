package com.xingzhe.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xingzhe.mapper.RoleResourceMapper;
import com.xingzhe.model.RoleResource;
import com.xingzhe.service.IRoleResourceService;

/**
 *
 * RoleResource 表数据服务层接口实现类
 *
 */
@Service
public class RoleResourceServiceImpl extends ServiceImpl<RoleResourceMapper, RoleResource> implements IRoleResourceService {


}
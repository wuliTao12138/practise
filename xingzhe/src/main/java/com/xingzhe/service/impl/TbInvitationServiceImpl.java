package com.xingzhe.service.impl;

import com.xingzhe.model.TbInvitation;
import com.xingzhe.mapper.TbInvitationMapper;
import com.xingzhe.service.ITbInvitationService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 邀请函 服务实现类
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
@Service
public class TbInvitationServiceImpl extends ServiceImpl<TbInvitationMapper, TbInvitation> implements ITbInvitationService {
	
}

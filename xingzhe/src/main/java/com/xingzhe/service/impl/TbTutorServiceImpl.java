package com.xingzhe.service.impl;

import com.xingzhe.model.TbTutor;
import com.xingzhe.mapper.TbTutorMapper;
import com.xingzhe.service.ITbTutorService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 导师 服务实现类
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
@Service
public class TbTutorServiceImpl extends ServiceImpl<TbTutorMapper, TbTutor> implements ITbTutorService {
	
}

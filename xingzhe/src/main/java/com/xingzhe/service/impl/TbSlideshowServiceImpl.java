package com.xingzhe.service.impl;

import com.xingzhe.model.TbSlideshow;
import com.xingzhe.mapper.TbSlideshowMapper;
import com.xingzhe.service.ITbSlideshowService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 轮播图 服务实现类
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
@Service
public class TbSlideshowServiceImpl extends ServiceImpl<TbSlideshowMapper, TbSlideshow> implements ITbSlideshowService {
	
}

package com.xingzhe.service.impl;

import com.xingzhe.model.TbTypeDomain;
import com.xingzhe.mapper.TbTypeDomainMapper;
import com.xingzhe.service.ITbTypeDomainService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 领域类型 服务实现类
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
@Service
public class TbTypeDomainServiceImpl extends ServiceImpl<TbTypeDomainMapper, TbTypeDomain> implements ITbTypeDomainService {
	
}

package com.xingzhe.service.impl;

import com.xingzhe.model.TbTypeArticle;
import com.xingzhe.mapper.TbTypeArticleMapper;
import com.xingzhe.service.ITbTypeArticleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 信息类型 服务实现类
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
@Service
public class TbTypeArticleServiceImpl extends ServiceImpl<TbTypeArticleMapper, TbTypeArticle> implements ITbTypeArticleService {
	
}

package com.xingzhe.service;

import com.baomidou.mybatisplus.service.IService;
import com.xingzhe.model.RoleResource;

/**
 *
 * RoleResource 表数据服务层接口
 *
 */
public interface IRoleResourceService extends IService<RoleResource> {


}
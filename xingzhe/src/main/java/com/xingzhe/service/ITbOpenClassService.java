package com.xingzhe.service;

import com.xingzhe.model.TbOpenClass;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-07-03
 */
public interface ITbOpenClassService extends IService<TbOpenClass> {
	
}

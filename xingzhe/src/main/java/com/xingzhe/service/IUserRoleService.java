package com.xingzhe.service;

import com.baomidou.mybatisplus.service.IService;
import com.xingzhe.model.UserRole;

/**
 *
 * UserRole 表数据服务层接口
 *
 */
public interface IUserRoleService extends IService<UserRole> {


}
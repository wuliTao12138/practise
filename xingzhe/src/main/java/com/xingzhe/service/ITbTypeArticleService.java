package com.xingzhe.service;

import com.xingzhe.model.TbTypeArticle;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 信息类型 服务类
 * </p>
 *
 * @author zhixuan.wang
 * @since 2017-06-28
 */
public interface ITbTypeArticleService extends IService<TbTypeArticle> {
	
}

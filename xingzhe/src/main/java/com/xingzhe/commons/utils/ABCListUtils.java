package com.xingzhe.commons.utils;

import java.util.Arrays;
import java.util.List;

/**
 * 获取英文字母集合
 */
public class ABCListUtils {
    public static String[] s={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
    public static List<String> getAZ(){
        List<String> list = Arrays.asList(s);
        return  list;
    }
    public static List<String> getAE(){
        return getAZ().subList(0,5);
    }
    public static List<String> getFK(){
        return getAZ().subList(5,11);
    }
    public static List<String> getLQ(){
        return getAZ().subList(11,16);
    }
    public static List<String> getRV(){
        return getAZ().subList(16,21);
    }
    public static List<String> getWZ(){
        return getAZ().subList(21,25);
    }


}

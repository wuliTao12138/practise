package com.xingzhe.commons.utils;
import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * 解析页面
 */
public class HttpClientUtil {
	public static String http(String url, String userAgent) {
		// 创建client实例
		HttpClient client = HttpClients.createDefault();
		// 创建httpGet实例
		HttpGet httpget = new HttpGet(url);
		httpget.setHeader("User-Agent", userAgent);
		// 执行get请求
		HttpResponse response;
		String web = null;
		try {
			response = client.execute(httpget);
			// 返回获取实体
			HttpEntity entity = response.getEntity();
			// 获取网页内容，指定编码
			web = EntityUtils.toString(entity, "UTF-8");
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return web;

	}
}

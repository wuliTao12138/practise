package com.xingzhe.commons.utils;

import java.util.UUID;

public class RandomUtil {
	/*
	 * 随机4位数
	 */
	public static int random4(){
		return (int) (Math.random()*9000+1000);
	}
	/**
	 * 订单号
	 */
	public static String getOrderIdByUUId() {
        int machineId = 1;//最大支持1-9个集群机器部署
        int hashCodeV = UUID.randomUUID().toString().hashCode();
        if(hashCodeV < 0) {//有可能是负数
            hashCodeV = - hashCodeV;
        }
        // 0 代表前面补充0     
        // 4 代表长度为4     
        // d 代表参数为正数型
        return machineId + String.format("%015d", hashCodeV);
    }
    public static void main(String[] args) {
        System.out.println(getOrderIdByUUId());
    }
}

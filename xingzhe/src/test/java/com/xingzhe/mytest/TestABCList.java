package com.xingzhe.mytest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by zhult on 2017/7/2.
 */
public class TestABCList {
    public static void main(String[] args) {
        String[] s={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
        List<String> list = Arrays.asList(s);
        list=  list.subList(0,6);
        System.out.println(list);
    }
}
